<?php

use Faker\Generator as Faker;

$factory->define(App\Section::class, function (Faker $faker) {
	$title = $faker->word;
    return [
    	'model_type' => $faker->word,
        'name' => $title,
        'slug' => $title,
        'description' =>$faker->sentence
    ];
});
