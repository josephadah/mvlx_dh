<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    $title = $faker->sentence;
    
    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'section_id' => function () {
        	return factory('App\Section')->create()->id;
        },
        'title' => $title,
        'slug' => str_slug($title),
        'body'  => $faker->paragraph,
        'locked' => false,
    ];
});
