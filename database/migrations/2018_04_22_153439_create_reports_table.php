<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('link');
            $table->boolean('blocked')->default(false);
            $table->string('description')->nullable();
            $table->string('report1')->nullable();
            $table->string('report2')->nullable();
            $table->string('report3')->nullable();
            $table->string('report4')->nullable();
            $table->string('report5')->nullable();
            $table->string('report6')->nullable();
            $table->string('report7')->nullable();
            $table->string('report8')->nullable();
            $table->string('report9')->nullable();
            $table->string('report10')->nullable();
            $table->string('report11')->nullable();
            $table->string('report12')->nullable();
            $table->string('report13')->nullable();
            $table->string('report14')->nullable();
            $table->string('report15')->nullable();
            $table->string('report16')->nullable();
            $table->string('report17')->nullable();
            $table->string('report18')->nullable();
            $table->string('report19')->nullable();
            $table->string('report20')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
