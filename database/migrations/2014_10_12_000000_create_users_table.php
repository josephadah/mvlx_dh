<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique()->index();
            $table->string('avatar_path')->nullable();
            $table->string('thumbnail_path')->nullable();
            $table->unsignedInteger('reputation')->default(0);
            $table->unsignedInteger('reward')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('confirmed')->default(false); 
            $table->string('confirmation_token', 25)->nullable()->unique();
            $table->string('sex', 10)->nullable();
            $table->unsignedInteger('phone')->nullable();
            $table->string('dob')->nullable();
            $table->string('title', 50)->nullable();
            $table->string('location', 50)->nullable();
            $table->text('bio', 500)->nullable();
            $table->boolean('banned')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
