<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('section_id');
            $table->string('title');
            $table->string('slug');
            $table->text('body', 5000);
            $table->string('image1_path')->nullable();
            $table->string('image2_path')->nullable();
            $table->unsignedInteger('best_answer_id')->nullable();
            $table->boolean('pinned')->default(false);
            $table->boolean('featured_question')->default(false);
            $table->boolean('featured_home')->default(false);
            $table->boolean('locked')->default(false);
            $table->boolean('blocked')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
