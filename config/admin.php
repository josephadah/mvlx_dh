<?php

return [ 
	'superadministrators' => [
        // Add the email addresses of users who should be superadministrators here.
        env('SUPER_ADMIN1')
    ],

    'administrators' => [
        // Add the email addresses of users who should be administrators here.
        env('ADMIN1'),
        env('ADMIN2'), 
        env('ADMIN3'),
        env('ADMIN4'), 
        env('ADMIN5'), 
        env('ADMIN6')
    ],

    'pagination' => [
        'repliesPerPage' => 25, 
        'threadsPerPage' => 30, 
        'threadsPerSection' => 30
    ]
];
