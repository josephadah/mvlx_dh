<?php

namespace App;

use App\Commonables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Comment extends Model
{
    use Likable, Commonables;
    
	protected $fillable = [
        'news_id', 'user_id', 'body', 'blocked',
        'image1_path', 'image2_path'
    ];

    protected $casts = [
        'blocked' => 'boolean'
    ];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['owner'];

    protected $appends = [
        'isLiked', 'isDisliked',
        'image1', 'image2'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($comment) {
            Reputation::award($comment->owner, Reputation::COMMENT_WAS_CREATED);
        });

        static::deleting(function ($comment) {
            Reputation::reduce($comment->owner, Reputation::COMMENT_WAS_CREATED);
        });
    }

    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function news()
    {
        return $this->belongsTo('App\News');
    }

    public function path() 
    {
        $perPage = config('admin.pagination.repliesPerPage');

        $commentPosition = $this->news->comments()->pluck('id')->search($this->id) + 1;

        $page = ceil($commentPosition / $perPage);

        return $this->news->showPath()."?page={$page}#comment-{$this->id}";
    }

    public function wasJustPublished()
    {
        return $this->created_at->gt(Carbon::now()->subSeconds(10));
    }

    public function mentionedUsers()
    {
        preg_match_all('/@([\w\_]+)/', $this->body, $matches);

        return $matches[1];
    }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This comment has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }

    public function block()
    {
        $this->update(['blocked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);
    }
}
