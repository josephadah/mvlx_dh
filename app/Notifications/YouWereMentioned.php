<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class YouWereMentioned extends Notification
{
    use Queueable;

    protected $reply;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reply)
    {
        $this->reply = $reply;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->reply->owner->username . ' Mentioned you in ' . $this->getModelTitle(), 
            'link' => $this->reply->path() 
        ];
    }

    protected function getModelTitle()
    {
        if($this->reply->thread) {
            return $this->reply->thread->title;
        } elseif ($this->reply->question) {
            return $this->reply->question->title;
        } elseif ($this->reply->news) {
            return $this->reply->news->title;
        } elseif ($this->reply->status) {
            if($this->reply->status->creator->id == auth()->id()) {
                return 'your status';
            } else {
                return $this->reply->status->creator->username . ' status.';
            }
        }
    }
}
