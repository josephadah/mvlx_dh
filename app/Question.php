<?php

namespace App;

use App\Commonables;
use App\Events\QuestionReceivedNewAnswer;
use App\Likable;
use App\Notifications\QuestionWasUpdated;
use App\RecordsActivity;
use App\RecordsVisits;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use Likable, RecordsActivity, RecordsVisits, Commonables;

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['creator'];

    protected $fillable = [
        'user_id', 'section_id', 'title', 
        'slug', 'body', 'locked', 'pinned', 
        'blocked', 'featured_question', 'featured_home',
        'image1_path', 'image2_path', 'best_answer_id', 
        'sponsored_home', 'thumbnail_path'
    ];

    protected $appends = [
        'isLiked', 'isDisliked', 'isSubscribedTo', 
        'image1', 'image2', 'thumbnail'
    ];

    protected $casts = [
        'locked' => 'boolean',
        'blocked' => 'boolean',
        'pinned' => 'boolean',
        'featured_question' => 'boolean',
        'featured_home' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($builder) {
            $builder->withCount('answers')
                ->withCount('likes')
                ->withCount('dislikes');
        });
        
        static::creating(function ($question) {
            Reputation::award($question->creator, Reputation::QUESTION_WAS_CREATED);
        });

        static::deleting(function ($question) {
            $question->answers->each->delete();

            Reputation::reduce($question->creator, Reputation::QUESTION_WAS_CREATED);
        });
    }
	
    public function path()
    {
    	return "/questions/{$this->id}";
    }

    public function showPath()
    {
        return "/questions/{$this->id}/{$this->slug}";
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class)->withCount('likes')->withCount('dislikes');
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function bestAnswer()
    {
        return $this->belongsTo(Answer::class, 'best_answer_id');
    }

    public function addAnswer($answer)
    {
        $answer = $this->answers()->create($answer);

        // event to handle lot of things like sending
        // notification to thread subscribers and mentioned users
        event(new QuestionReceivedNewAnswer($answer));

        // notify question owner if others leave an answer
        if($answer->user_id != $this->user_id) {
            $this->creator->notify(new QuestionWasUpdated($this, $answer));
        }

        return $answer;
    }

    public function lock()
    {
        $this->update(['locked' => true]);
    }

    public function unlock()
    {
        $this->update(['locked' => false]);
    }

    public function block()
    {
        $this->update(['blocked' => true]);

        $this->update(['locked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);

        $this->update(['locked' => false]);
    }

    public function featureQuestion()
    {
        $this->update(['featured_question' => true]);
    }

    public function unfeatureQuestion()
    {
        $this->update(['featured_question' => false]);
    }

    public function featureHome()
    {
        $this->update(['featured_home' => true]);
    }

    public function unfeatureHome()
    {
        $this->update(['featured_home' => false]);
    }

    public function sponsorHome()
    {
        $this->update(['sponsored_home' => true]);
    }

    public function unsponsorHome()
    {
        $this->update(['sponsored_home' => false]);
    }

    public function notifySubscribers($answer) 
    {
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->user->id != $answer->user_id) {
                $subscription->user->notify(new QuestionWasUpdated($this, $answer));
            }
        }
    }

    public function scopeFilter($query, $filter)
    {
        return $filter->apply($query);
    }

    // subscriptions methods
    public function subscriptions()
    {
        return $this->morphMany(Subscription::class, 'subscribed');
    }

    public function subscribe()
    {
        $attributes = ['user_id' => auth()->id()];

        if (! $this->subscriptions()->where($attributes)->exists()) {
            $this->subscriptions()->create($attributes);
        }
    }

    public function unsubscribe()
    {
        $this->subscriptions()->where(['user_id' => auth()->id()])->get()->each->delete();
    }

    public function isSubscribedTo()
    {
        return $this->subscriptions()->where('user_id', auth()->id())->exists();
    }

    public function getIsSubscribedToAttribute()
    {
        return $this->isSubscribedTo();
    }


    public function hasUpdateFor()
    {
        $key = auth()->user()->visitedContentsCacheKey($this);

        if($this->answers()->exists()) {
            $lastAnswer = $this->answers()->latest()->first();
            if($lastAnswer->user_id != auth()->id()) {
                return $lastAnswer->updated_at > cache($key);
            }
        }
    }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This Question has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }

}
