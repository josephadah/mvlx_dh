<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['user_id'];

    public function subscribed()
    {
    	return $this->morphTo()->orderByDesc('updated_at');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
