<?php

namespace App;

use App\News;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = ['name'];

    public function news()
    {
        return $this->hasMany(News::class);
    }
}
