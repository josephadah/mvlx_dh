<?php

namespace App;

use App\RecordsActivity;
use Illuminate\Database\Eloquent\Model;

class Dislike extends Model
{
	// use RecordsActivity;

    protected $fillable = ['user_id'];

    public function disliked() 
    {
    	return $this->morphTo();
    }
}
