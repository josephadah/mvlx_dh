<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ManageUsersController extends Controller
{
	public function index()
	{
		$users = User::latest()->paginate(20);

		$totalUsers = User::count();

		$confirmedUsers = User::where('confirmed', true)->count();

		return view('manage.users', compact('users', 'totalUsers', 'confirmedUsers'));
	}

	public function search() 
	{	
		$query = request('user');

		$users = User::where('username', 'LIKE', "%$query%")
            	->paginate(20);
		
		return view('manage.users_search', compact('users'));
	}

}
