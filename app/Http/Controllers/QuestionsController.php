<?php

namespace App\Http\Controllers;

use App\Filters\QuestionFilters;
use App\Question;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class QuestionsController extends Controller
{
    public function home()
    {
        $recentQuestions = Question::latest()->take(20)->get();

        $trendingQuestions = Question::orderBy('answers_count', 'desc')->take(20)->get();

        $featuredQuestions = Question::where('featured_question', true)->latest()->take(20)->get();

        return view('questions.home', compact(
            'recentQuestions', 'trendingQuestions', 'featuredQuestions'
        ));
    }

    public function section(Section $section)
    {
        $questions = $section->questions()->orderBy('pinned', 'DESC')
                    ->latest()->simplePaginate(config('admin.pagination.threadsPerSection'));

        return view('questions.section', compact('questions', 'section'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(QuestionFilters $filters)
    {
        $filter_type = $this->getFilterType();

        $questions = Question::latest();

        $questions = $questions->filter($filters);

        if(request()->wantsJson()) {
            return $questions->get();
        }

        $questions = $questions->paginate(config('admin.pagination.threadsPerPage'));

        return view('questions.index', compact('questions', 'filter_type'));
    }

    public function getFilterType()
    {
        if(request('by')) { return $filter_type = request('by'); }

        return $filter_type = 'Recent';
    }

    public function trending() 
    {
        $trendingQuestionsToday = Question::where('created_at', '>=', Carbon::now()->subDay())
                                            ->orderBy('answers_count', 'desc')->take(15)->get();

        $trendingQuestionsThisWeek = Question::where('created_at', '>=', Carbon::now()->subWeek())
                                            ->orderBy('answers_count', 'desc')->take(15)->get();

        $trendingQuestionsThisMonth = Question::where('created_at', '>=', Carbon::now()->subMonth())
                                            ->orderBy('answers_count', 'desc')->take(15)->get();

        return view('questions.trending', compact(
            'trendingQuestionsToday', 'trendingQuestionsThisWeek', 'trendingQuestionsThisMonth'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::where('model_type', 'question')->get();

        return view('questions.create', compact('sections'));
    }

    // create a question report
    public function createReport(Question $question)
    {
        return view('manage.reports.create', ['content' => $question]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title'     => 'required | max:100 | spamfree',
            'section'   =>  'required|exists:sections,id',
            'body'      => 'required | max:30000 | spamfree',
            'terms'     => 'required'
        ]);

        $this->validateImages();

        $images_path = $this->uploadImage();
        
        $question = Question::create([
            'user_id'   => auth()->id(),
            'section_id' => request('section'),
            'title'     => request('title'),
            'slug'      => str_slug(request('title')),
            'body'      => request('body'), 
            'image1_path' => $images_path[0], 
            'image2_path' => $images_path[1], 
            'thumbnail_path' => $images_path[2]
        ]);

        return redirect($question->showPath())
            ->with('flash', 'Your Question has been published successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question, $slug)
    {
        $question = $question->load('section');

        if(auth()->check()) {
            if($question->isSubscribedTo()) {
                auth()->user()->readContent($question);
            }
        }

        $question->recordVisit();

        $featuredQuestions = Question::where('featured_home', true)->orderByDesc('updated_at')->take(15)->get();

        return view('questions.show', compact('question', 'featuredQuestions'));
    }

    // redirect user back to question after login to answer
    public function loginToComment(Question $question) 
    {
        return redirect($question->showPath());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question) 
    {
        return view('questions.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $this->authorize('update', $question);

         request()->validate([
            'title' => 'required | max:100 | spamfree', 
            'body' => 'required | max:30000 | spamfree'
        ]);

         $this->validateImages();

        if(request('delete_image1')) {
            $this->deleteImage1($question);
        }

        if(request('delete_image2')) {
            $this->deleteImage2($question);
        }

        $this->updateImage($question);
        
        $question->update([
            'title' => request('title'),
            'slug' => str_slug(request('title')), 
            'body' => request('body')
        ]);

        return redirect($question->showPath());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $this->authorize('update', $question);

        $this->deleteImage($question);

        $question->delete();

        return redirect(route('questions.home'));
    }
}
