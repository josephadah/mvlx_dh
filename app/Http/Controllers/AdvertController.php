<?php

namespace App\Http\Controllers;

use App\Advert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $allAdverts = Advert::all();

        return view('manage.adverts.index', compact('allAdverts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required | max:20', 
            'description' => 'required | max:255', 
            'link' => 'required | max:500', 
        ]);

        $this->validateImages();

        $uploaded_image_path = $this->uploadAdImage();

        if(request('image_path')) {
            request()->validate(['image_path' => 'max:500']);
            $image_path = request('image_path');
        } else {
            $image_path = $uploaded_image_path;
        }

        Advert::create([
            'name' => request('name'),
            'description' => request('description'),
            'link' => request('link'),
            'image_path' => $image_path,
            'uploaded_image_path' => $uploaded_image_path
        ]);

        return redirect(route('adverts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function show(Advert $advert)
    {
        return view('manage.adverts.show', compact('advert'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function edit(Advert $advert)
    {
        return view('manage.adverts.edit', compact('advert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advert $advert)
    {
        request()->validate([
            'name' => 'required | max:20', 
            'description' => 'required | max:255', 
            'link' => 'required | max:500', 
        ]);

        $this->validateImages();

        $uploaded_image_path = $this->updateAdImage($advert);

        if(request('image_path')) {
            request()->validate(['image_path' => 'max:500']);
            $image_path = request('image_path');
        } else {
            $image_path = $uploaded_image_path;
        }

        $advert->update([
            'name' => request('name'),
            'description' => request('description'),
            'link' => request('link'),
            'image_path' => $image_path,
            'uploaded_image_path' => $uploaded_image_path
        ]);

        return redirect(route('adverts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advert $advert)
    {
        $this->deleteAdImage($advert);

        $advert->delete();

        return redirect(route('adverts.index'));
    }

    // upload advert image
    public function uploadAdImage() {
        $image_path = '';

        if($file = request()->file('image1')) {
            $filename = time() . $file->hashName();
            
            $image_path = 'images/adverts/'.$filename;

            request()->file('image1')->storeAs('/images/adverts', $filename, 'public');
        }

        return $image_path;
    }

    // update advert image
    public function updateAdImage($advert) {

        $image_path = '';

        if($file = request()->file('image1')) {
            $filename = time() . $file->hashName();
            
            $image_path = 'images/adverts/'.$filename;

            if ($advert->image_path) {
                $old_image_path = ('/public/' . $advert->image_path);

                if (Storage::exists($old_image_path)) {
                    Storage::delete($old_image_path);
                };
            };

            request()->file('image1')->storeAs('/images/adverts', $filename, 'public');

            return $image_path;
        }
    }

    // delete advert image
    public function deleteAdImage($advert) {
        if ($advert->image_path) {
            $image_path = ('/public/' . $advert->image_path);

            if (Storage::exists($image_path)) {
                Storage::delete($image_path);
            };
        };
    }
}
