<?php

namespace App\Http\Controllers;

use App\Comment;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(News $news)
    {
        return $news->comments()->paginate(config('admin.pagination.repliesPerPage'));
    }

    // create a comment report
    public function createReport(Comment $comment)
    {
        return view('manage.reports.create', ['content' => $comment]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(News $news)
    {
        if ($news->locked) {
            return response('This news is locked!', 422);
        }
        
        if (Gate::denies('create', new Comment)) {
            return response([
                'errors' => ['body' => 
                ['You are posting too frequently, please try after few seconds.']]], 422);
        }

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        $images_path = $this->uploadImage();

        $comment = $news->addComment([
            'body' => request('body'), 
            'user_id' => auth()->id(),
            'image1_path' => $images_path[0],
            'image2_path' => $images_path[1]
        ]);

        if(request()->expectsJson()) {
            return $news->comments()->where('id', $comment->id)->first();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $this->authorize('update', $comment);

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        if(json_decode(request('delete_image1'))) {
            $this->deleteImage1($comment);
        }

        if(json_decode(request('delete_image2'))) {
            $this->deleteImage2($comment);
        }

        $this->updateImage($comment);

        $comment->update([
            'body' => request('body')
        ]);

        $news = News::where('id', $comment->news_id)->first();

        if(request()->expectsJson()) {
            return $news->comments()->where('id', $comment->id)->first();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('update', $comment);

        $this->deleteImage($comment);

        $comment->delete();

        if(request()->wantsJson()) {
            return response(['status' => 'Comment deleted successfully!.']);
        }

        return back();
    }
}
