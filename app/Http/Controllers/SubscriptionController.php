<?php

namespace App\Http\Controllers;

use App\News;
use App\Question;
use App\Thread;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function subscribeThread(Thread $thread)
    {
    	$thread->subscribe();
    }

    public function unsubscribeThread(Thread $thread)
    {
    	$thread->unsubscribe();
    }

    public function subscribeQuestion(Question $question)
    {
        $question->subscribe();
    }

    public function unsubscribeQuestion(Question $question)
    {
        $question->unsubscribe();
    }

    public function subscribeNews(News $news)
    {
        $news->subscribe();
    }

    public function unsubscribeNews(News $news)
    {
        $news->unsubscribe();
    }
}
