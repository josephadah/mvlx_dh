<?php

namespace App\Http\Controllers;

use App\Region;
use Illuminate\Http\Request;

class NewsRegionController extends Controller
{

    // fetch all news based on region 
    public function search()
    {
        $search = request('region');

        // $regionName = strtolower(request('region'));
        $regionName = preg_replace('/[\,\.]/', '', strtolower(request('region')));

        $news = [];

        if (Region::where('name', $regionName)->exists()) {
            $region = Region::where('name', $regionName)->first();
            $news = $region->news()->latest()->paginate(1);
        }

        return view('news.index', ['newses' => $news, 'filter_type' => request('region')]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request('region');

        return Region::where('name', 'LIKE', "%$search%")
            ->take(5)
            ->pluck('name');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
