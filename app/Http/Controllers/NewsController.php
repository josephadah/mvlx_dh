<?php

namespace App\Http\Controllers;

use App\Filters\NewsFilters;
use App\News;
use App\Region;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;

class NewsController extends Controller
{
    public function home()
    {
        $recentNews = News::latest()->take(20)->get();

        $trendingNews = News::orderBy('comments_count', 'desc')->take(20)->get();

        $featuredNews = News::where('featured_news', true)->latest()->take(20)->get();

        return view('news.home', compact(
            'recentNews', 'trendingNews', 'featuredNews'
        ));
    }

    public function section(Section $section)
    {
        $newses = $section->news()->orderBy('pinned', 'DESC')
                    ->latest()->simplePaginate(config('admin.pagination.threadsPerSection'));

        return view('news.section', compact('newses', 'section'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(NewsFilters $filters)
    {
        $filter_type = $this->getFilterType();

        $news = News::latest();

        $news = $news->filter($filters);

        if(request()->wantsJson()) {
            return $news->get();
        }

        $news = $news->paginate(config('admin.pagination.threadsPerPage'));

        return view('news.index', ['newses' => $news, 'filter_type' => $filter_type]);
    }

    public function getFilterType()
    {
        if(request('by')) { return $filter_type = request('by'); }

        return $filter_type = 'Latest';
    }

    public function trending() 
    {
        $trendingNewsToday = News::where('created_at', '>=', Carbon::now()->subDay())
                                            ->orderBy('comments_count', 'desc')->take(15)->get();

        $trendingNewsThisWeek = News::where('created_at', '>=', Carbon::now()->subWeek())
                                            ->orderBy('comments_count', 'desc')->take(15)->get();

        $trendingNewsThisMonth = News::where('created_at', '>=', Carbon::now()->subMonth())
                                            ->orderBy('comments_count', 'desc')->take(15)->get();

        return view('news.trending', compact(
            'trendingNewsToday', 'trendingNewsThisWeek', 'trendingNewsThisMonth'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::where('model_type', 'news')->get();

        return view('news.create', compact('sections'));
    }

    // create a news report
    public function createReport(News $news)
    {
        return view('manage.reports.create', ['content' => $news]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (Gate::denies('create', new News)) {
            return redirect(route('create-news'))
            ->with('flash', 'You are posting too frequently, please try after few seconds.', 'danger');
        }

        request()->validate([
            'title'     => 'required | max:100 | spamfree',
            'section'   =>  'required|exists:sections,id',
            'region'    => 'spamfree|max:20',
            'body'      => 'required | max:30000 | spamfree', 
            'terms'     => 'required'
        ]);

        $this->validateImages();

        //add region to news
        $region_id = $this->getRegion();

        $images_path = $this->uploadImage();
        
        $news = News::create([
            'user_id'   => auth()->id(),
            'section_id' => request('section'),
            'region_id' => $region_id,
            'title'     => request('title'),
            'slug'      => str_slug(request('title')),
            'body'      => request('body'), 
            'image1_path' => $images_path[0], 
            'image2_path' => $images_path[1], 
            'thumbnail_path' => $images_path[2]
        ]);

        return redirect($news->showPath())
            ->with('flash', 'Your post has been created successfully');
    }

    // function to store and get region id if given
    public function getRegion()
    {
        $region_id = null;
        if(request('region')) {
            $regionName = preg_replace('/[\,\.]/', '', strtolower(request('region')));
            if(Region::where('name', $regionName)->exists()) {
                $region_id = Region::where('name', $regionName)->first()->id;
            } else {
                $region = Region::create(['name' => $regionName]);
                $region_id = $region->id;
            }
        }
        return $region_id;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news, $slug)
    {
       $news = $news->load('section');

        if(auth()->check()) {
            if($news->isSubscribedTo()) {
                auth()->user()->readContent($news);
            }
        }

        $section_newses = News::Where('section_id', $news->section_id)->orderBy('pinned', 'DESC')
                            ->latest()->take(10)->get();

        $featuredNews = News::where('featured_home', true)->orderByDesc('updated_at')->take(15)->get();
        $sponsoredNews = News::where('sponsored_home', true)->orderByDesc('updated_at')->take(5)->get();

        return view('news.show', compact('news', 'featuredNews', 'sponsoredNews', 'section_newses')   
        ); 
    }

    // redirect user back to news after login to comment
    public function loginToComment(News $news) 
    {
        return redirect($news->showPath());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        return view('news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $this->authorize('update', $news);

        request()->validate([
            'title' => 'required | max:100 | spamfree', 
            'body' => 'required | max:30000 | spamfree'
        ]);

        $this->validateImages();

        if(request('delete_image1')) {
            $this->deleteImage1($news);
        }

        if(request('delete_image2')) {
            $this->deleteImage2($news);
        }

        $this->updateImage($news);
        
        $news->update([
            'title' => request('title'),
            'slug' => str_slug(request('title')), 
            'body' => request('body')
        ]);

        return redirect($news->showPath());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $this->authorize('update', $news);

        $this->deleteImage($news);

        $news->delete();

        return redirect(route('news.home'));
    }
}
