<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    /**
     * Fetch all relevant username.
     *
     * @return mixed
     */
    public function index()
    {
        $search = request('username');

        return User::where('username', 'LIKE', "%$search%")
            ->take(5)
            ->pluck('username');
    }

    // fetch a particular user 
    public function search()
    {
        $search = request('query');

        $users = User::where('username', 'LIKE', "%$search%")
            ->paginate(10);

        return view('users.search', compact('users'));
    }

    // fetch page to search for users
    public function create()
    {
        return view('users.create');
    }
}
