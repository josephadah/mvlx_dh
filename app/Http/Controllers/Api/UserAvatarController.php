<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use ImageOptimizer;
use Intervention\Image\Facades\Image;

class UserAvatarController extends Controller
{
    public function store()
    {
    	request()->validate([
    		'avatar' => 'required | image | mimes:jpg,jpeg,png,gif,svg | max:4100'
    	]);

        $file = request()->file('avatar');
        $filename = $file->hashName();

        $avatarPath = 'images/avatars/'.$filename;
        $avatarThumbnailPath = 'thumbnails/avatars/'.$filename;

        $avatarImage = Image::make($file->getRealPath())->fit(500, 500, null, 'center');
        $avatarThumbnail = Image::make($file->getRealPath())->fit(50, 50, null, 'center');


        if (auth()->user()->avatar_path) {
            $oldAvatarPath = ('/public/' . auth()->user()->avatar_path);
            $oldThumbnailPath = ('/public/' . auth()->user()->thumbnail_path);

            if (Storage::exists($oldAvatarPath)) {
                Storage::delete($oldAvatarPath);
            };

            if (Storage::exists($oldThumbnailPath)) {
                Storage::delete($oldThumbnailPath);
            };

        };

        Storage::put('/public/images/avatars/'.$filename, (string) $avatarImage->encode(), 'public');
        Storage::put('/public/thumbnails/avatars/'.$filename, (string) $avatarThumbnail->encode(), 'public');

        auth()->user()->update([
            'avatar_path' => $avatarPath, 
            'thumbnail_path' => $avatarThumbnailPath
        ]);

        return response(auth()->user(), 201);
    }

    public function destroy()
    {
        if (auth()->user()->avatar_path) {
            $oldAvatarPath = ('/public/' . auth()->user()->avatar_path);
            $oldThumbnailPath = ('/public/' . auth()->user()->thumbnail_path);

            if (Storage::exists($oldAvatarPath)) {
                Storage::delete($oldAvatarPath);
            };

            if (Storage::exists($oldThumbnailPath)) {
                Storage::delete($oldThumbnailPath);
            };

                auth()->user()->update([
                'avatar_path' => null, 
                'thumbnail_path' => null
            ]);
        };

        return response(auth()->user(), 201);
    }
}
