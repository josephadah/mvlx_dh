<?php

namespace App\Http\Controllers;

use App\Filters\ThreadFilters;
use App\Section;
use App\Thread;
use App\User;
use Illuminate\Support\Carbon;

class ThreadController extends Controller
{
    /**
     * Create a new ThreadsController instance.
     */
    // public function __construct()
    // {
    //     $this->middleware('auth')->except(['index', 'show', 'search']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ThreadFilters $filters)
    {
        $filter_type = $this->getFilterType();

        $threads = Thread::latest();

        $threads = $threads->filter($filters);

        if(request()->wantsJson()) {
            return $threads->get();
        }

        $threads = $threads->paginate(config('admin.pagination.threadsPerPage'));

        return view('threads.index', compact('threads', 'filter_type'));
    }

    public function getFilterType()
    {
        if(request('by')) { return $filter_type = request('by'); }

        return $filter_type = 'Recent';
    }

    public function trending() 
    {
        $trendingThreadsToday = Thread::where('created_at', '>=', Carbon::now()->subDay())
                                            ->orderBy('replies_count', 'desc')->take(15)->get();

        $trendingThreadsThisWeek = Thread::where('created_at', '>=', Carbon::now()->subWeek())
                                            ->orderBy('replies_count', 'desc')->take(15)->get();

        $trendingThreadsThisMonth = Thread::where('created_at', '>=', Carbon::now()->subMonth())
                                            ->orderBy('replies_count', 'desc')->take(15)->get();

        return view('threads.trending', compact(
            'trendingThreadsToday', 'trendingThreadsThisWeek', 'trendingThreadsThisMonth'
        ));
    }

    public function search(ThreadFilters $filters) 
    {
        $threads = Thread::latest()->filter($filter);

        return $threads;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = Section::where('model_type', 'thread')->get();

        return view('threads.create', compact('sections'));
    }

    // create a thread report
    public function createReport(Thread $thread)
    {
        return view('manage.reports.create', ['content' => $thread]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'title'     => 'required | max:100 | spamfree',
            'section'   =>  'required|exists:sections,id',
            'body'      => 'required | max:30000 | spamfree',
            'terms'     => 'required'
        ]);

        $this->validateImages();

        $images_path = $this->uploadImage();
        
        $thread = Thread::create([
            'user_id'   => auth()->id(),
            'section_id' => request('section'),
            'title'     => request('title'),
            'slug'      => str_slug(request('title')),
            'body'      => request('body'), 
            'image1_path' => $images_path[0], 
            'image2_path' => $images_path[1], 
            'thumbnail_path' => $images_path[2]
        ]);

        return redirect($thread->showPath())
            ->with('flash', 'Your thread has been created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread, $slug)
    {
        $thread = $thread->load('section');

        if(auth()->check()) {
            if($thread->isSubscribedTo()) {
                auth()->user()->readContent($thread);
            }
        }

        $section_threads = Thread::Where('section_id', $thread->section_id)->orderBy('pinned', 'DESC')
                            ->latest()->take(10)->get();
        $featuredThreads = Thread::where('featured_home', true)->orderByDesc('updated_at')->take(15)->get();
        $sponsoredThreads = Thread::where('sponsored_home', true)->orderByDesc('updated_at')->take(5)->get();

        $thread->recordVisit();

        return view('threads.show', compact('thread', 'featuredThreads', 'sponsoredThreads', 'section_threads'));
    }

    // redirect user back to thread after login to reply
    public function loginToComment(Thread $thread) 
    {
        return redirect($thread->showPath());
    }

    public function edit(Thread $thread) 
    {
    	return view('threads.edit', compact('thread'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Thread $thread)
    {
        $this->authorize('update', $thread);

        request()->validate([
            'title' => 'required | max:100 | spamfree', 
            'body' => 'required | max:30000 | spamfree'
        ]);

        $this->validateImages();

        if(request('delete_image1')) {
        	$this->deleteImage1($thread);
        }

        if(request('delete_image2')) {
        	$this->deleteImage2($thread);
        }

        $this->updateImage($thread);
        
        $thread->update([
            'title' => request('title'),
            'slug' => str_slug(request('title')), 
            'body' => request('body')
        ]);

        return redirect($thread->showPath());

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thread $thread)
    {
        $this->authorize('update', $thread);

        $this->deleteImage($thread);

        $thread->delete();

        return redirect(route('forum.home'));
    }
}
