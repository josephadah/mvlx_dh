<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Comment;
use App\News;
use App\Question;
use App\Reply;
use App\Response;
use App\Status;
use App\Thread;

class BlockedContentsController extends Controller
{
    public function blockThread(Thread $thread)
    {
    	$thread->block();
    }

    public function unblockThread(Thread $thread)
    {
    	$thread->unblock();
    }

    public function blockQuestion(Question $question)
    {
        $question->block();
    }

    public function unblockQuestion(Question $question)
    {
        $question->unblock();
    }

    public function blockNews(News $news)
    {
        $news->block();
    }

    public function unblockNews(News $news)
    {
        $news->unblock();
    }

    public function blockStatus(Status $status)
    {
        $status->block();
    }

    public function unblockStatus(Status $status)
    {
        $status->unblock();
    }

    public function blockReply(Reply $reply)
    {
    	$reply->block();
    }

    public function unblockReply(Reply $reply)
    {
    	$reply->unblock();
    }

    public function blockAnswer(Answer $answer)
    {
        $answer->block();
    }

    public function unblockAnswer(Answer $answer)
    {
        $answer->unblock();
    }

    public function blockComment(Comment $comment)
    {
        $comment->block();
    }

    public function unblockComment(Comment $comment)
    {
        $comment->unblock();
    }

    public function blockResponse(Response $response)
    {
        $response->block();
    }

    public function unblockResponse(Response $response)
    {
        $response->unblock();
    }
}
