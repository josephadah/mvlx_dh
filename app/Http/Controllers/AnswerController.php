<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Question $question)
    {
        return $question->answers()->paginate(config('admin.pagination.repliesPerPage'));
    }

    // create a answer report
    public function createReport(Answer $answer)
    {
        return view('manage.reports.create', ['content' => $answer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Question $question)
    {
        if ($question->locked) {
            return response('This question is locked!', 422);
        }
        
        if (Gate::denies('create', new Answer)) {
            return response([
                'errors' => ['body' => 
                ['You are posting too frequently, please try after few seconds.']]], 422);
        }

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        $images_path = $this->uploadImage();

        $answer = $question->addAnswer([
            'body' => request('body'), 
            'user_id' => auth()->id(),
            'image1_path' => $images_path[0],
            'image2_path' => $images_path[1]
        ]);

        if(request()->expectsJson()) {
            return $question->answers()->where('id', $answer->id)->first();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Answer $answer)
    {
        $this->authorize('update', $answer);

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        if(json_decode(request('delete_image1'))) {
            $this->deleteImage1($answer);
        }

        if(json_decode(request('delete_image2'))) {
            $this->deleteImage2($answer);
        }

        $this->updateImage($answer);

        $answer->update([
            'body' => request('body')
        ]);

        $question = Question::where('id', $answer->question_id)->first();

        if(request()->expectsJson()) {
            return $question->answers()->where('id', $answer->id)->first();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        $this->authorize('update', $answer);

        $this->deleteImage($answer);

        $answer->delete();

        if(request()->wantsJson()) {
            return response(['status' => 'Answer deleted successfully!.']);
        }

        return back();
    }
}
