<?php

namespace App\Http\Controllers;

use App\Response;
use App\Status;
use Illuminate\Support\Facades\Gate;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Status $status)
    {
        $statuses = $status->responses()->paginate(config('admin.pagination.repliesPerPage'));

        return $statuses->toArray();
    }

    // create a response report
    public function createReport(Response $response)
    {
        return view('manage.reports.create', ['content' => $response]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Status $status)
    {        
        if (Gate::denies('create', new Response)) {
            return response([
                'errors' => ['body' => 
                ['You are posting too frequently, please try after few seconds.']]], 422);
        }

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        $images_path = $this->uploadImage();

        $response = $status->addResponse([
            'body' => request('body'), 
            'user_id' => auth()->id(),
            'image1_path' => $images_path[0],
            'image2_path' => $images_path[1]
        ]);

        if(request()->expectsJson()) {
            return $status->responses()->where('id', $response->id)->first();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Response  $response
     * @return \Illuminate\Http\Response
     */
    public function update(Response $response)
    {
        $this->authorize('update', $response);

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        if(json_decode(request('delete_image1'))) {
            $this->deleteImage1($response);
        }

        if(json_decode(request('delete_image2'))) {
            $this->deleteImage2($response);
        }

        $this->updateImage($response);

        $response->update([
            'body' => request('body')
        ]);

        $status = Status::where('id', $response->status_id)->first();

        if(request()->expectsJson()) {
            return $status->responses()->where('id', $response->id)->first();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Response  $response
     * @return \Illuminate\Http\Response
     */
    public function destroy(Response $response)
    {
        $this->authorize('update', $response);

        $this->deleteImage($response);

        $response->delete();

        if(request()->wantsJson()) {
            return response(['status' => 'Comment deleted successfully!.']);
        }

        return back();
    }
}
