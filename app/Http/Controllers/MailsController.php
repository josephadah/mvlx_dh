<?php

namespace App\Http\Controllers;

use App\Mail\YoulynUpdates;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mailgun\Mailgun;

class MailsController extends Controller
{
    public function index()
    {
    	return view('manage.mails.index');
    }

    public function updateList()
    {
        if(!auth()->check() && ! auth()->user()->isSuperAdmin()) return;
        
        // Instantiate the client.
        $mgClient = new Mailgun(env('MAILGUN_SECRET'));

        $listAddress = 'subscribed_list@mg.youlyn.com';

        $subscribed_list = [];

        // FILTER IT TO ONLY COLLECT LIST OF USERS FROM LAST WEEK ONLY
        $users = User::where('confirmed', true)->get();

        foreach ($users as $user) {
            $subscribed_list[] = ['address' => $user->email, 'name' => $user->username];
        }

        $json_list = json_encode($subscribed_list);

        // Issue the call to the client.
        $result = $mgClient->post("lists/$listAddress/members.json", array(
            'members'   => "$json_list",
            'upsert'    => true
        ));

        return back()->with('flash', 'List updated successfully.');
    }

    public function store()
    {
        if(!auth()->check() && ! auth()->user()->isSuperAdmin()) return;

    	request()->validate([
            'link1_title' => 'required',
            'link2_title' => 'required',
            'link3_title' => 'required',
            'link4_title' => 'required',
            'link5_title' => 'required',
            'link6_title' => 'required',
            'link7_title' => 'required',
            'link8_title' => 'required',
            'link9_title' => 'required',
            'link10_title' => 'required',
            'link1' => 'required',
            'link2' => 'required',
            'link3' => 'required',
            'link4' => 'required',
            'link5' => 'required',
            'link6' => 'required',
            'link7' => 'required',
            'link8' => 'required',
            'link9' => 'required',
            'link10' => 'required'
        ]);

        $all_requests = request()->all();
                
        Mail::to('subscribed_list@mg.youlyn.com')
            ->send(new YoulynUpdates($all_requests));

    	return view('emails.broadcast_sent');
    }
}
