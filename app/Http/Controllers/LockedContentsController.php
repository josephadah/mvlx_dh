<?php

namespace App\Http\Controllers;

use App\News;
use App\Question;
use App\Thread;
use Illuminate\Http\Request;

class LockedContentsController extends Controller
{
    public function lockThread(Thread $thread)
    {
    	$thread->lock();
    }

    public function unlockThread(Thread $thread)
    {
    	$thread->unlock();
    }

    public function lockQuestion(Question $question)
    {
    	$question->lock();
    }

    public function unlockQuestion(Question $question)
    {
    	$question->unlock();
    }

    public function lockNews(News $news)
    {
    	$news->lock();
    }

    public function unlockNews(News $news)
    {
    	$news->unlock();
    }
}
