<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class BanUserController extends Controller
{
    public function store(User $user)
    {
    	$user->ban();
    }

    public function destroy(User $user)
    {
    	$user->unban();
    }
}
