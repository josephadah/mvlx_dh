<?php

namespace App\Http\Controllers;

use App\Activity;
use App\News;
use App\Question;
use App\Status;
use App\Thread;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the HOME PAGE.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featuredNews = News::where('featured_home', true)->orderByDesc('updated_at')->take(20)->get();

        $sponsoredNews = News::where('sponsored_home', true)->orderByDesc('updated_at')->take(5)->get();

        $recentNews = News::latest()->take(20)->get();

        // CHECK N+1 ISSUES WHEN LOADING ACTIVITIES.

        if(auth()->check()) {
            $idsOfFollowedUsersAndYours = auth()->user()->getIdsOfFollowedUsers();
            $idsOfFollowedUsersAndYours[] = auth()->id();

            $feedActivities = Activity::whereIn('user_id', $idsOfFollowedUsersAndYours)
                            ->with('subject')->latest()->paginate(20);
        }
        return view('home', compact('feedActivities', 'featuredNews', 'sponsoredNews', 'recentNews'));
    }
}
