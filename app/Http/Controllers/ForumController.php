<?php

namespace App\Http\Controllers;

use App\Section;
use App\Thread;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function home()
    {
        $recentThreads = Thread::latest()->take(20)->get();

        $trendingThreads = Thread::orderBy('replies_count', 'desc')->take(20)->get();

        $featuredThreads = Thread::where('featured_thread', true)->latest()->take(20)->get();

        return view('threads.forum', compact(
            'recentThreads', 'trendingThreads', 'featuredThreads'
        ));
    }

    public function section(Section $section)
    {
    	$threads = $section->threads()->orderBy('pinned', 'DESC')
                    ->latest()->simplePaginate(config('admin.pagination.threadsPerSection'));

    	return view('threads.section', compact('threads', 'section'));
    }
}
