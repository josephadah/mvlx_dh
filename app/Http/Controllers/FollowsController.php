<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FollowsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user)
    {
        if($user) {
            auth()->user()->follow($user->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showFollowers(User $user)
    {
        return view('profiles.followers', [
            'followers' => $user->followers()->latest()->get(),
            'profileOwner' => $user
        ]);
    }

    public function showFollowing(User $user)
    {
        return view('profiles.following', [
            'followings' => $user->follows()->latest()->get(), 
            'profileOwner' => $user
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user) {
            auth()->user()->unfollow($user->id);
        }
    }
}
