<?php

namespace App\Http\Controllers;

use App\Advert;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $ads;

    public function __construct()
    {
        // get all ads data object
        $this->ads = Advert::all();

        // share it with all views
        View::share('ads', $this->ads);
    }

    public function uploadImage() {
    	$image1_path = '';
    	$image2_path = '';
        $thumbnail_path = '';

    	if($file1 = request()->file('image1')) {
        	$filename1 = time() . $file1->hashName();
            $thumbnailName = time() . $file1->hashName();
        	
        	$image1_path = 'images/'.$filename1;
            $thumbnail_path = 'thumbnails/'.$thumbnailName;

        	$image1 = Image::make($file1->getRealPath())
        				->resize(500, null, function ($constraint) {
        					$constraint->aspectRatio();
        				});

            $thumbnail = Image::make($file1->getRealPath())
                        ->fit(100, 100, null, 'center');

            Storage::put('/public/images/'.$filename1, (string) $image1->encode(), 'public');
        	Storage::put('/public/thumbnails/'.$thumbnailName, (string) $thumbnail->encode(), 'public');
        }

        if($file2 = request()->file('image2')) {
        	$filename2 = time() . $file2->hashName();
        	
        	$image2_path = 'images/'.$filename2;

        	$image2 = Image::make($file2->getRealPath())
        				->resize(500, null, function ($constraint) {
        					$constraint->aspectRatio();
        				});

        	Storage::put('/public/images/'.$filename2, (string) $image2->encode(), 'public');
        }

        return [$image1_path, $image2_path, $thumbnail_path];
    }


    public function updateImage($model) {

        // update image1 if its in the request
        if($file1 = request()->file('image1')) {
            $filename1 = time() . $file1->hashName();
            $thumbnailName = time() . $file1->hashName();
            
            $image1_path = 'images/'.$filename1;
            $thumbnail_path = 'thumbnails/'.$thumbnailName;

            $image1 = Image::make($file1->getRealPath())
                        ->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

            $thumbnail = Image::make($file1->getRealPath())
                        ->fit(100, 100, null, 'center');

            if ($model->image1_path) {
                $old_image1_path = ('/public/' . $model->image1_path);

                if (Storage::exists($old_image1_path)) {
                    Storage::delete($old_image1_path);
                };
            };

            if ($model->thumbnail_path) {
                $old_thumbnail_path = ('/public/' . $model->thumbnail_path);

                if(Storage::exists($old_thumbnail_path)) {
                    Storage::delete($old_thumbnail_path);
                };
            };

            Storage::put('/public/images/'.$filename1, (string) $image1->encode(), 'public');
            Storage::put('/public/thumbnails/'.$thumbnailName, (string) $thumbnail->encode(), 'public');

            $model->update(['image1_path' => $image1_path]);
            $model->update(['thumbnail_path' => $thumbnail_path]);

        }

        // update image2 if its in the request
        if($file2 = request()->file('image2')) {
            $filename2 = time() . $file2->hashName();
            
            $image2_path = 'images/'.$filename2;

            $image2 = Image::make($file2->getRealPath())
                        ->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });

            if ($model->image2_path) {
                $old_image2_path = ('/public/' . $model->image2_path);

                if (Storage::exists($old_image2_path)) {
                    Storage::delete($old_image2_path);
                };
            };

            Storage::put('/public/images/'.$filename2, (string) $image2->encode(), 'public');

            $model->update(['image2_path' => $image2_path]);
        }

    }


    public function deleteImage($model) {
    	if ($model->image1_path) {
            $image1_path = ('/public/' . $model->image1_path);

            if (Storage::exists($image1_path)) {
                Storage::delete($image1_path);
            };
        };

        if ($model->thumbnail_path) {
            $thumbnail_path = ('/public/' . $model->thumbnail_path);

            if (Storage::exists($thumbnail_path)) {
                Storage::delete($thumbnail_path);
            };
        };

        if ($model->image2_path) {
            $image2_path = ('/public/' . $model->image2_path);

            if (Storage::exists($image2_path)) {
                Storage::delete($image2_path);
            };
        };
    }

    public function deleteImage1($model) {
        if ($model->image1_path) {
            $image1_path = ('/public/' . $model->image1_path);

            if (Storage::exists($image1_path)) {
                Storage::delete($image1_path);
            };

            $model->update(['image1_path' => null]);
        };

        if ($model->thumbnail_path) {
            $thumbnail_path = ('/public/' . $model->thumbnail_path);

            if (Storage::exists($thumbnail_path)) {
                Storage::delete($thumbnail_path);
            };

            $model->update(['thumbnail_path' => null]);
        };
    }

    public function deleteImage2($model) {
        if ($model->image2_path) {
            $image2_path = ('/public/' . $model->image2_path);

            if (Storage::exists($image2_path)) {
                Storage::delete($image2_path);
            };

            $model->update(['image2_path' => null]);
        };
    }

    // validate request for reply, comment, answer and responses only
    public function validateImages()
    {
        if(request()->file('image1')) {
            request()->validate([
                'image1' => 'image|mimes:jpg,jpeg,png,gif,svg|max:4100',
            ]);
        }

        if(request()->file('image2')) {
            request()->validate([
                'image2' => 'image|mimes:jpg,jpeg,png,gif,svg|max:4100'
            ]);
        }
    }
}
