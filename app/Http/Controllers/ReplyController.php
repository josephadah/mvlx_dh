<?php

namespace App\Http\Controllers;

use App\Reply;
use App\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Thread $thread)
    {
        // return $thread->replies()->paginate(20);
        return $thread->replies()->paginate(config('admin.pagination.repliesPerPage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Thread $thread)
    {
        if ($thread->locked) {
            return response('This thread is locked!', 422);
        }
        
        if (Gate::denies('create', new Reply)) {
            return response([
                'errors' => ['body' => 
                ['You are posting too frequently, please try after few seconds.']]], 422);
        }

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        $images_path = $this->uploadImage();

        $reply = $thread->addReply([
            'body' => request('body'), 
            'user_id' => auth()->id(),
            'image1_path' => $images_path[0],
            'image2_path' => $images_path[1]
        ]);

        if(request()->expectsJson()) {
            return $thread->replies()->where('id', $reply->id)->first();
        }
    }

    // create a reply report
    public function createReport(Reply $reply)
    {
        return view('manage.reports.create', ['content' => $reply]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reply $reply)
    {
        $this->authorize('update', $reply);

        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        if(json_decode(request('delete_image1'))) {
            $this->deleteImage1($reply);
        }

        if(json_decode(request('delete_image2'))) {
            $this->deleteImage2($reply);
        }

        $this->updateImage($reply);

        $reply->update([
            'body' => request('body')
        ]);

        $thread = Thread::where('id', $reply->thread_id)->first();

        if(request()->expectsJson()) {
            return $thread->replies()->where('id', $reply->id)->first();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reply $reply)
    {
        $this->authorize('update', $reply);

        $this->deleteImage($reply);

        $reply->delete();

        if(request()->wantsJson()) {
            return response(['status' => 'Reply deleted successfully!.']);
        }

        return back();
    }
}
