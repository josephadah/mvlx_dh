<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RewardController extends Controller
{
	// increment a user reward by 1
    public function store(User $user)
    {
    	$user->increment('reward', 1);

    	return back();
    }

    // decrement a user reward by 1
    public function destroy(User $user)
    {
    	$user->decrement('reward', 1);

    	return back();
    }
}
