<?php

namespace App\Http\Controllers;

use App\Status;
use App\User;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $profileOwner = $user;

        $statuses = $user->statuses()->latest()->paginate(20);

        return view('statuses.index', compact('statuses', 'profileOwner'));
    }

    // create a status report
    public function createReport(Status $status)
    {
        return view('manage.reports.create', ['content' => $status]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'body' => 'required | max:10000 | spamfree', 
        ]);
        
        $this->validateImages();

        $images_path = $this->uploadImage();

        $status = Status::create([
            'user_id' => auth()->id(),
            'body' => request('body'),
            'image1_path' => $images_path[0], 
            'image2_path' => $images_path[1]
        ]);

        return redirect('/#feed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function show(Status $status)
    {
        $status->recordVisit();

        return view('statuses.show', compact('status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function edit(Status $status)
    {
        return view('statuses.edit', compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function update(Status $status)
    {
        request()->validate([
            'body' => 'required | max:10000 | spamfree'
        ]);

        $this->validateImages();

        if(request('delete_image1')) {
            $this->deleteImage1($status);
        }

        if(request('delete_image2')) {
            $this->deleteImage2($status);
        }

        $this->updateImage($status);
        
        $status->update([
            'title' => request('title'),
            'slug' => str_slug(request('title')), 
            'body' => request('body')
        ]);

        return redirect($status->showPath());;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Status $status)
    {
        $this->authorize('update', $status);

        $this->deleteImage($status);

        $status->delete();

        return redirect(route('profile', auth()->user()->username));
    }
}
