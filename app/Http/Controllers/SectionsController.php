<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;

class SectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = Section::orderBy('model_type', 'desc')->get();

        return view('manage.sections.index', compact('sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'model_type' => 'required|max:20',
            'name' => 'required|max:50', 
            'slug' => 'required|max:30', 
            'description' => 'required|max:500'
        ]);

        $sections = Section::create([
            'model_type' => request('model_type'),
            'name' => request('name'), 
            'slug' => request('slug'),
            'description' => request('description')
        ]);

        return redirect(route('sections.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        return view('manage.sections.edit', compact('section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Section $section)
    {
        request()->validate([
            'model_type' => 'required|max:20',
            'name' => 'required|max:50', 
            'slug' => 'required|max:30', 
            'description' => 'required|max:500'
        ]);

        $section->update([
            'model_type' => request('model_type'),
            'name' => request('name'), 
            'slug' => request('slug'),
            'description' => request('description')
        ]);

        return redirect(route('sections.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        $section->delete();

        return redirect(route('sections.index'));
    }
}
