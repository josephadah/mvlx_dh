<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::paginate(10);

        return view('manage.reports.index', compact('reports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'link' => 'required|max:255'
        ]);

        if(request('description')) {
            request()->validate([
                'description' => 'max:200'
            ]);
        }

        Report::create([
            'link' => request('link'),
            'report1' => request('report1'),
            'report2' => request('report2'),
            'report3' => request('report3'),
            'report4' => request('report4'),
            'report5' => request('report5'),
            'report6' => request('report6'),
            'report7' => request('report7'),
            'report8' => request('report8'),
            'report9' => request('report9'),
            'report10' => request('report10'),
            'description' => request('description')
        ]);

        return view('manage.reports.success');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        $report->update(['blocked' => true]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        $report->delete();

        return back();
    }
}
