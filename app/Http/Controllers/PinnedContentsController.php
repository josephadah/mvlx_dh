<?php

namespace App\Http\Controllers;

use App\News;
use App\Question;
use App\Thread;
use Illuminate\Http\Request;

class PinnedContentsController extends Controller
{
    public function pinThread(Thread $thread)
    {
    	$thread->update(['pinned' => true]);
    }

    public function unpinThread(Thread $thread)
    {
    	$thread->update(['pinned' => false]);
    }

    public function pinQuestion(Question $question)
    {
    	$question->update(['pinned' => true]);
    }

    public function unpinQuestion(Question $question)
    {
    	$question->update(['pinned' => false]);
    }

    public function pinNews(News $news)
    {
        $news->update(['pinned' => true]);
    }

    public function unpinNews(News $news)
    {
        $news->update(['pinned' => false]);
    }
}
