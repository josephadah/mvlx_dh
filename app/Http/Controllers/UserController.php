<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Status;
use App\Subscription;
use App\Thread;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile(User $user)
    {
        $profileOwnerActivities = Activity::where('user_id', $user->id)
                                    ->latest()->paginate(20);

    	return view('profiles.show', [
    		'profileOwner' => $user,
    		'profileOwnerActivities' => $profileOwnerActivities 
    	]);
    }

    public function subscriptions(User $user)
    {
        $newsSubscriptions = $user->getSubscribedNews();
        $threadSubscriptions = $user->getSubscribedThreads();

    	return view('profiles.subscriptions', compact(
            'newsSubscriptions', 'threadSubscriptions'
        ));
    }

    public function threadSubscriptions(User $user)
    {
        $threads = $user->getSubscribedThreads();

        return view('threads.subscriptions', compact('threads'));
    }

    public function questionSubscriptions(User $user)
    {
        $questions = $user->getSubscribedQuestions();

        return view('questions.subscriptions', compact('questions'));
    }

    public function newsSubscriptions(User $user)
    {
        $newses = $user->getSubscribedNews();

        return view('news.subscriptions', compact('newses'));
    }

    public function storeProfile(User $user) 
    {
        request()->validate([
            'sex' => 'max:10',
            'location' => 'max:50 | spamfree',
            'title' => 'max:50 | spamfree',
            'phone' => 'max:50 | spamfree',
            'bio' => 'max:300 | spamfree'
        ]);

        if($user) {
            $user->update([
                'sex' => request('sex'), 
                'location' => request('location'), 
                'title' => request('title'),
                'phone' => request('phone'),
                'bio' => request('bio')
            ]);
        }
    }

    // request for user contact
    public function requestContact(User $user)
    {
        if($user) {
            auth()->user()->request($user->id);
        }
    }

    public function allRequest()
    {
        return view('profiles.requests', [
            'requesters' => auth()->user()->requesters()->latest()->get()
        ]);
    }

    public function approveRequest(User $user)
    {
        auth()->user()->approve($user->id);

        return back()->with('flash', 'User approved to view your contact.');
    }

    public function declineRequest(User $user)
    {
        auth()->user()->decline($user->id);

        return back()->with('flash', 'User Request has been declined');
    }

    public function resetRequestSent()
    {
        return view('auth.passwords.reset_request_sent');
    }

    public function resetSuccessful()
    {
        return view('auth.passwords.reset_successful');
    }

}
