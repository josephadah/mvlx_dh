<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;

class RegisterConfirmationController extends Controller
{
    use AuthenticatesUsers;
    
    public function index()
    {
    	$user = User::where('confirmation_token', request('token'))->first();

    	if (! $user) {
    		return redirect(route('home'))
	    			->with('flash', 'Invalid confirmation token.');
    	}

	    $user->confirm();

        $this->guard()->login($user);

    	return view('pages.account_confirmed');
    }
}
