<?php

namespace App\Http\Controllers;

use App\News;
use App\Question;
use App\Status;
use App\Thread; 
use Illuminate\Http\Request;

class FeaturedContentsController extends Controller
{
    // featuring threads 
    public function featureThread(Thread $thread) 
    {
    	$thread->featureThread();
    }

    public function unfeatureThread(Thread $thread) 
    {
    	$thread->unfeatureThread();
    }

    public function featureHomeThread(Thread $thread) 
    {
    	$thread->featureHome();
    }

    public function unfeatureHomeThread(Thread $thread) 
    {
    	$thread->unfeatureHome();
    }

    public function sponsorHomeThread(Thread $thread)
    {
        $thread->sponsorHome();
    }

    public function unsponsorHomeThread(Thread $thread) 
    {
        $thread->unsponsorHome();
    }

    // featuring questions
    public function featureQuestion(Question $question) 
    {
        $question->featureQuestion();
    }

    public function unfeatureQuestion(Question $question) 
    {
        $question->unfeatureQuestion();
    }

    public function featureHomeQuestion(Question $question) 
    {
        $question->featureHome();
    }

    public function unfeatureHomeQuestion(Question $question) 
    {
        $question->unfeatureHome();
    }

    public function sponsorHomeQuestion(Question $question)
    {
        $question->sponsorHome();
    }

    public function unsponsorHomeQuestion(Question $question) 
    {
        $question->unsponsorHome();
    }

    // featuring news
    public function featureNews(News $news) 
    {
        $news->featureNews();
    }

    public function unfeatureNews(News $news) 
    {
        $news->unfeatureNews();
    }

    public function featureHomeNews(News $news) 
    {
        $news->featureHome();
    }

    public function unfeatureHomeNews(News $news) 
    {
        $news->unfeatureHome();
    }

    public function sponsorHomeNews(News $news)
    {
        $news->sponsorHome();
    }

    public function unsponsorHomeNews(News $news) 
    {
        $news->unsponsorHome();
    }

    // featuring status
    public function featureHomeStatus(Status $status) 
    {
        $status->featureHome();
    }

    public function unfeatureHomeStatus(Status $status) 
    {
        $status->unfeatureHome();
    }
}
