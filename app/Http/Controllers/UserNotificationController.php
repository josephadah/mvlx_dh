<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserNotificationController extends Controller
{
	public function allNotifications()
	{
		$allNotifications = auth()->user()->unreadNotifications()->paginate(30);

		return view('profiles.all_notifications', compact('allNotifications'));
	}

	public function index()
	{
		$recentNotifications = auth()->user()->unreadNotifications()->take(10)->get()->toArray();
		$totalNotifications = auth()->user()->unreadNotifications()->count();

		return response([$recentNotifications, $totalNotifications], 201);
	}

    public function destroy($notification)
    {
    	auth()->user()->notifications()->findOrFail($notification)->delete();
    }

    public function deleteAll()
    {
    	auth()->user()->notifications()->delete();

    	return back()->with('flash', 'All Notifications Deleted.');
    }
}
