<?php

namespace App\Http\Controllers;

use App\Mail\PleaseConfirmYourEmail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{	
	// about us page
	public function about()
	{
		return view('pages.about');
	}

	// terms page
	public function terms()
	{
		return view('pages.terms');
	}

	// privacy page
	public function privacy()
	{
		return view('pages.privacy');
	}

	// contact page 
	public function contact()
	{
		return view('pages.contact');
	}

	// email not confirmed
	public function notConfirmed()
	{
		return view('pages.not_confirmed', compact('userEmail'));
	}

	// user ban page
    public function userBan()
    {
    	return view('pages.ban');
    }

    // registration success page
    public function registrationSuccess()
    {
    	return view('pages.registration_success');
    }


    // resend confirmation email
    public function resendConfirmationEmail()
    {
    	$user = User::where('username', auth()->user()->username)->first();

        Mail::to($user->email)->send(new PleaseConfirmYourEmail($user));

        return view('pages.resend_confirmation_email_sent');
    }

    // change user email after registration
    public function changeEmail()
    {
        return view('pages.change_email');
    }

    // store changed email after registration
    public function storeChangeEmail(User $user)
    {
        request()->validate(['email' => 'required|email|unique:users|max:100']);

        $user->update(['email' => request('email')]);

        $this->resendConfirmationEmail();

        $userNewEmail = $user->fresh()->email;

        return view('pages.change_email_successful', compact('userNewEmail'));
    }
}
