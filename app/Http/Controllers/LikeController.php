<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Comment;
use App\News;
use App\Question;
use App\Reply;
use App\Response;
use App\Status;
use App\Thread;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    //liking thread
    public function likeThread(Thread $thread)
    {
        if($thread->isLiked()) {
            $thread->unlike();
        } else {
            if($thread->isDisliked()) {
                $thread->undislike();
            }  
            $thread->like();
        }
    }

    public function unlikeThread(Thread $thread)
    {
        $thread->unlike();
    }

    public function dislikeThread(Thread $thread)
    {
        if($thread->isDisliked()) {
            $thread->undislike();
        } else {
            if($thread->isLiked()) {
                $thread->unlike();
            }  
            $thread->dislike();
        }
    }

    public function undislikeThread(Thread $thread)
    {
        $thread->undislike();
    }

    // liking question
    public function likeQuestion(Question $question)
    {
        if($question->isLiked()) {
            $question->unlike();
        } else {
            if($question->isDisliked()) {
                $question->undislike();
            }  
            $question->like();
        }
    }

    public function unlikeQuestion(Question $question)
    {
        $question->unlike();
    }

    public function dislikeQuestion(Question $question)
    {
        if($question->isDisliked()) {
            $question->undislike();
        } else {
            if($question->isLiked()) {
                $question->unlike();
            }  
            $question->dislike();
        }
    }

    public function undislikeQuestion(Question $question)
    {
        $question->undislike();
    }


    //liking news
    public function likeNews(News $news)
    {
        if($news->isLiked()) {
            $news->unlike();
        } else {
            if($news->isDisliked()) {
                $news->undislike();
            }  
            $news->like();
        }
    }

    public function unlikeNews(News $news)
    {
        $news->unlike();
    }

    public function dislikeNews(News $news)
    {
        if($news->isDisliked()) {
            $news->undislike();
        } else {
            if($news->isLiked()) {
                $news->unlike();
            }  
            $news->dislike();
        }
    }

    public function undislikeNews(News $news)
    {
        $news->undislike();
    }


    // liking status
    public function likeStatus(Status $status)
    {
        if($status->isLiked()) {
            $status->unlike();
        } else {
            if($status->isDisliked()) {
                $status->undislike();
            }  
            $status->like();
        }
    }

    public function unlikeStatus(Status $status)
    {
        $status->unlike();
    }

    public function dislikeStatus(Status $status)
    {
        if($status->isDisliked()) {
            $status->undislike();
        } else {
            if($status->isLiked()) {
                $status->unlike();
            }  
            $status->dislike();
        }
    }

    public function undislikeStatus(Status $status)
    {
        $status->undislike();
    }


    // liking reply
    public function likeReply(Reply $reply)
    {
        if($reply->isLiked()) {
            $reply->unlike();
        } else {
            if($reply->isDisliked()) {
                $reply->undislike();
            }  
            $reply->like();
        }
    }

    public function unlikeReply(Reply $reply)
    {
        $reply->undislike();
    }

    public function dislikeReply(Reply $reply)
    {
        if($reply->isDisliked()) {
            $reply->undislike();
        } else {
            if($reply->isLiked()) {
                $reply->unlike();
            }  
            $reply->dislike();
        }
    }

    public function undislikeReply(Reply $reply)
    {
        $reply->undislike();
    }


    // liking answer
    public function likeAnswer(Answer $answer)
    {
        if($answer->isLiked()) {
            $answer->unlike();
        } else {
            if($answer->isDisliked()) {
                $answer->undislike();
            }  
            $answer->like();
        }
    }

    public function unlikeAnswer(Answer $answer)
    {
        $answer->undislike();
    }

    public function dislikeAnswer(Answer $answer)
    {
        if($answer->isDisliked()) {
            $answer->undislike();
        } else {
            if($answer->isLiked()) {
                $answer->unlike();
            }  
            $answer->dislike();
        }
    }

    public function undislikeAnswer(Answer $answer)
    {
        $answer->undislike();
    }


    // liking comment 
    public function likeComment(Comment $comment)
    {
        if($comment->isLiked()) {
            $comment->unlike();
        } else {
            if($comment->isDisliked()) {
                $comment->undislike();
            }  
            $comment->like();
        }
    }

    public function unlikeComment(Comment $comment)
    {
        $comment->undislike();
    }

    public function dislikeComment(Comment $comment)
    {
        if($comment->isDisliked()) {
            $comment->undislike();
        } else {
            if($comment->isLiked()) {
                $comment->unlike();
            }  
            $comment->dislike();
        }
    }

    public function undislikeComment(Comment $comment)
    {
        $comment->undislike();
    }


    // liking response
    public function likeResponse(Response $response)
    {
        if($response->isLiked()) {
            $response->unlike();
        } else {
            if($response->isDisliked()) {
                $response->undislike();
            }  
            $response->like();
        }
    }

    public function unlikeResponse(Response $response)
    {
        $response->unlike();
    }

    public function dislikeResponse(Response $response)
    {
        if($response->isDisliked()) {
            $response->undislike();
        } else {
            if($response->isLiked()) {
                $response->unlike();
            }  
            $response->dislike();
        }
    }

    public function undislikeResponse(Response $response)
    {
        $response->undislike();
    }


}
