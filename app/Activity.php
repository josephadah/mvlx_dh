<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
    	'user_id', 'subject_id', 'subject_type', 'type'
    ];

    protected $with = ['subject'];

    public function subject()
    {
    	return $this->morphTo();
    }

    public static function feed($user)
    {
    	return $user->activities()
    			->take(10)->latest()->get();
    }
}
