<?php

namespace App;

/**
* reputation class
*/
class Reputation
{
	const THREAD_WAS_CREATED = 5;
	const QUESTION_WAS_CREATED = 5;
	const NEWS_WAS_CREATED = 5;
	const REPLY_WAS_CREATED = 1;
	const ANSWER_WAS_CREATED = 1;
	const COMMENT_WAS_CREATED = 1;
	
	public static function award($user, $points)
	{
		$user->increment('reputation', $points);
	}

	public static function reduce($user, $points)
	{
		$user->decrement('reputation', $points);
	}
}