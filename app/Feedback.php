<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $guarded = [];

    protected $with = ['sender'];

    public function sender()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
