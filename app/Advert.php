<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    protected $fillable = [
    	'name', 'description', 'link',
    	'image_path', 'uploaded_image_path'
    ]; 
}
