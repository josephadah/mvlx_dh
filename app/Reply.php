<?php

namespace App;

use App\Likable;
use App\Commonables;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Reply extends Model
{
    use Likable, Commonables;
    
	protected $fillable = [
        'thread_id', 'user_id', 'body', 'blocked', 
        'image1_path', 'image2_path'
    ];

    protected $casts = [
        'blocked' => 'boolean'
    ];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['owner'];

    protected $appends = [
        'isLiked', 'isDisliked', 
        'image1', 'image2'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($reply) {
            Reputation::award($reply->owner, Reputation::REPLY_WAS_CREATED);
        });

        static::deleting(function ($reply) {
            Reputation::reduce($reply->owner, Reputation::REPLY_WAS_CREATED);
        });
    }

    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function thread()
    {
        return $this->belongsTo('App\Thread');
    }

    public function path() 
    {
        $perPage = config('admin.pagination.repliesPerPage');

        $replyPosition = $this->thread->replies()->pluck('id')->search($this->id) + 1;

        $page = ceil($replyPosition / $perPage);

        return $this->thread->showPath()."?page={$page}#reply-{$this->id}";
    }

    public function mentionedUsers()
    {
        preg_match_all('/@([\w\_]+)/', $this->body, $matches);

        return $matches[1];
    }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This reply has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }

    public function block()
    {
        $this->update(['blocked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);
    }
}
