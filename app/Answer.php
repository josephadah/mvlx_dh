<?php

namespace App;

use App\Commonables;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use Likable, Commonables;
    
	protected $fillable = [
        'question_id', 'user_id', 'body', 'blocked', 
        'image1_path', 'image2_path'
    ];

    protected $casts = [
        'blocked' => 'boolean'
    ];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['owner'];

    protected $appends = [
        'isLiked', 'isDisliked', 
        'image1', 'image2', 'isBest'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($answer) {
            Reputation::award($answer->owner, Reputation::ANSWER_WAS_CREATED);
        });

        static::deleting(function ($answer) {
            if($answer->isBest()) {
                $answer->question->update(['best_answer_id' => null]);
            };

            Reputation::reduce($answer->owner, Reputation::ANSWER_WAS_CREATED);
        });
    }

    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function path() 
    {

        $perPage = config('admin.pagination.repliesPerPage');

        $answerPosition = $this->question->answers()->pluck('id')->search($this->id) + 1;

        $page = ceil($answerPosition / $perPage);

        return $this->question->showPath()."?page={$page}#answer-{$this->id}";
    }

    public function getIsBestAttribute()
    {
        return $this->isBest();
    }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This answer has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }

    public function block()
    {
        $this->update(['blocked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);
    }

    public function isBest()
    {
        return $this->question->best_answer_id == $this->id;
    }
}
