<?php

namespace App;

use App\Activity;
use App\Answer;
use App\Comment;
use App\News;
use App\Question;
use App\Reply;
use App\Response;
use App\Status;
use App\Thread;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'avatar_path', 
        'confirmed', 'confirmation_token', 'sex', 'location', 
        'title', 'bio', 'thumbnail_path', 'dob', 'phone', 
        'banned' 
    ];

    protected $casts = [
        'confirmed' => 'boolean',
        'banned' => 'boolean'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'isAdmin', 'isSuperAdmin', 
        'avatar', 'thumbnail', 'requestsCount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email', 'confirmation_token'
    ];

    /**
     * Get the route key name for Laravel.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'username';
    }

    public function statuses()
    {
        return $this->hasMany(Status::class);
    }

    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function lastNews()
    {
        return $this->hasOne(News::class)->latest();
    }

    public function lastReply()
    {
        return $this->hasOne(Reply::class)->latest();
    }

    public function lastResponse()
    {
        return $this->hasOne(Response::class)->latest();
    }

    public function lastAnswer()
    {
        return $this->hasOne(Answer::class)->latest();
    }

    public function lastComment()
    {
        return $this->hasOne(Comment::class)->latest();
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function subscribed()
    {
        return $this->hasMany('App\Subscription', 'user_id');
    }

    /**
     * Determine if the user is an administrator.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return in_array(
            strtolower($this->email),
            array_map('strtolower', config('admin.administrators'))
        );
    }


    /**
     * Determine if the user is an administrator.
     *
     * @return bool
     */
    public function isBanned()
    {
        return $this->banned;
    }

    /**
     * Determine if the user is an administrator.
     *
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->isAdmin();
    }

    /**
     * Determine if the user is a superadministrator.
     *
     * @return bool
     */
    public function isSuperAdmin()
    {
        return in_array(
            strtolower($this->email),
            array_map('strtolower', config('admin.superadministrators'))
        );
    }

    /**
     * Determine if the user is an administrator.
     *
     * @return bool
     */
    public function getIsSuperAdminAttribute()
    {
        return $this->isSuperAdmin();
    }

    public function confirm()
    {
        $this->confirmed = true;

        $this->confirmation_token = null;

        $this->save();
    }

    public function getAllSubscribedContents()
    {
        return $this->subscribed()->with('subscribed')->paginate(20);
    }

    public function getSubscribedThreads()
    {
        $subscribedThreadIds = $this->subscribed()
                        ->where('subscribed_type', 'App\Thread')
                        ->get(['subscribed_id'])
                        ->toArray();

        return Thread::find($subscribedThreadIds)->sortByDesc(function($builder) {
            if($builder->replies()->exists()) {
            $lastReply = $builder->replies()->latest()->first();
            return $lastReply->created_at;
            }
        })->take(20);
    }

    public function getSubscribedQuestions()
    {
        $subscribedQuestionIds = $this->subscribed()
                        ->where('subscribed_type', 'App\Question')
                        ->get(['subscribed_id'])
                        ->toArray();

        return Question::find($subscribedQuestionIds)->sortByDesc(function($builder) {
            if($builder->answers()->exists()) {
            $lastAnswer = $builder->answers()->latest()->first();
            return $lastAnswer->created_at;
            }
        })->take(20);
    }

    public function getSubscribedNews()
    {
        $subscribedNewsIds = $this->subscribed()
                        ->where('subscribed_type', 'App\News')
                        ->get(['subscribed_id'])
                        ->toArray();

        return News::find($subscribedNewsIds)->sortByDesc(function($builder) {
            if($builder->comments()->exists()) {
            $lastComment = $builder->comments()->latest()->first();
            return $lastComment->created_at;
            }
        })->take(20);
    }

    public function visitedContentsCacheKey($model) 
    {
    	return sprintf("users.%s.visitsthread.%s", $this->id, $model->id);
    }

    public function readContent($model)
    {
    	cache()->forever($this->visitedContentsCacheKey($model), \Carbon\Carbon::now());
    }

    public function avatar()
    {
        if (! $this->avatar_path) {
            return asset("storage/images/avatars/default.png");
        }
        return asset('storage/' . $this->avatar_path);
    }

    public function thumbnail()
    {
        if (! $this->thumbnail_path) {
            return asset("storage/images/avatars/default.png");
        }
        return asset('storage/' . $this->thumbnail_path);
    }

    // getter for avatar
    public function getAvatarAttribute()
    {
        return $this->avatar();
    }

    // getter for thumbnail
    public function getThumbnailAttribute()
    {
        return $this->thumbnail();
    }

    // ban user
    public function ban()
    {
        return $this->update(['banned' => true]);
    }

    // unban user
    public function unban()
    {
        return $this->update(['banned' => false]);
    }

    // return list of requests sent by the current user
    public function requests()
    {
        return $this->belongsToMany(self::class, 'requests', 'requester_id', 'requested_id')
                    ->withTimestamps();
    }

    // return list of users that sent contact request to this user
    public function requesters()
    {
        return $this->belongsToMany(self::class, 'requests', 'requested_id', 'requester_id')
                    ->withTimestamps();
    }

    public function getRequestCacheKey($userId)
    {
        return $this->id . 'request' . $userId;
    }

    // check if the auth user has sent this user a request
    public function hasRequested($userId)
    {
        return Cache::get($this->getRequestCacheKey($userId), false);
    }

    public function request($userId)
    {
        $this->requests()->attach($userId);

        Cache::rememberForever($this->getRequestCacheKey($userId), function() {
            return true;
        });
    }

    public function getRequestsCountAttribute()
    {
        return $this->requesters()->count();
    }

    // lists of permitted users 
    public function permissions()
    {
        return $this->belongsToMany(self::class, 'permissions', 'user_id', 'permitted_id');
    }

    public function hasPermitted($userId)
    {
        return $this->permissions()->where('permitted_id', $userId)->exists();
    }

    // approve a user request
    public function approve($userId)
    {
        $this->permissions()->attach($userId);
        $this->requesters()->detach($userId);
    }

     // decline a user request
    public function decline($userId)
    {
        $this->requesters()->detach($userId);
    }

    // return list users that the current user follows
    public function follows()
    {
        return $this->belongsToMany(self::class, 'follows', 'follower_id', 'followed_id')
                    ->withTimestamps();
    }

    // return list of users that follows the current user
    public function followers()
    {
        return $this->belongsToMany(self::class, 'follows', 'followed_id', 'follower_id')
                                    ->withTimestamps();
    }

    public function follow($userId)
    {
        return $this->follows()->attach($userId);
    }

    public function unfollow($userId)
    {
        return $this->follows()->detach($userId);
    }

    public function isFollowing($userId)
    {
        $otherUserFollowersId = [];
        $otherUser = $this->findOrFail($userId);
        if ($otherUser) {
            $otherUserFollowersId = $otherUser->followers()->pluck('follower_id')->toArray();
        }
        return in_array($this->id, $otherUserFollowersId);
    }

    public function getIdsOfFollowedUsers()
    {
        return $this->follows()->pluck('followed_id')->toArray();
    }

    public function earnReward($points=1)
    {
        $this->increment('reward', $points);
    }

    public function looseReward($points=1)
    {
        $this->decrement('reward', $points);
    }

    public function getTitleAttribute($title)
    {
        return \Purify::clean(ucfirst($title));
    }

    public function getLocationAttribute($location)
    {
        return \Purify::clean(ucfirst($location));
    }

    public function getBioAttribute($bio)
    {
        return \Purify::clean(ucfirst($bio));
    }

}
