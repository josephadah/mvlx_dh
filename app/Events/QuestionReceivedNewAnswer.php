<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class QuestionReceivedNewAnswer
{
    use Dispatchable, SerializesModels;

    public $answer;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($answer)
    {
        $this->answer = $answer;
    }
}
