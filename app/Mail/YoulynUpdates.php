<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class YoulynUpdates extends Mailable
{
    use Queueable, SerializesModels;

    public $all_requests;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($all_requests)
    {
        $this->all_requests = $all_requests;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.weekly_broadcast');
    }
}
