<?php

namespace App;

use App\Comment;
use App\Commonables;
use App\Events\NewsReceivedNewComment;
use App\Notifications\NewsWasUpdated;
use App\Region;
use App\Section;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use Likable, RecordsActivity, Commonables;

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['creator'];

    protected $fillable = [
        'user_id', 'section_id', 'title', 
        'slug', 'body', 'locked', 'pinned', 
        'blocked', 'featured_news', 'featured_home', 
        'image1_path', 'image2_path', 'region_id',
        'sponsored_home', 'thumbnail_path'
    ];

    protected $appends = [
        'isLiked', 'isDisliked', 'isSubscribedTo', 
        'image1', 'image2', 'thumbnail'
    ];

    protected $casts = [
        'locked' => 'boolean',
        'blocked' => 'boolean',
        'pinned' => 'boolean',
        'featured_news' => 'boolean',
        'featured_home' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($builder) {
            $builder->withCount('comments')
                ->withCount('likes')
                ->withCount('dislikes');
        });
        
        static::creating(function ($news) {
            Reputation::award($news->creator, Reputation::NEWS_WAS_CREATED);
        });

        static::deleting(function ($news) {
            $news->comments->each->delete();

            Reputation::reduce($news->creator, Reputation::NEWS_WAS_CREATED);
        });
    }
	
    public function path()
    {
    	return "/news/{$this->id}";
    }

    public function showPath()
    {
        return "/news/{$this->id}/{$this->slug}";
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->withCount('likes')->withCount('dislikes');
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function addComment($comment)
    {
        $comment = $this->comments()->create($comment);

        // event to handle lot of things like sending
        // notification to thread subscribers and mentioned users
        event(new NewsReceivedNewComment($comment));

        // notify news owner if others leave an comment
        if($comment->user_id != $this->user_id) {
            $this->creator->notify(new NewsWasUpdated($this, $comment));
        }

        return $comment;
    }

    public function lock()
    {
        $this->update(['locked' => true]);
    }

    public function unlock()
    {
        $this->update(['locked' => false]);
    }

    public function block()
    {
        $this->update(['blocked' => true]);

        $this->update(['locked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);

        $this->update(['locked' => false]);
    }

    public function featureNews()
    {
        $this->update(['featured_news' => true]);
    }

    public function unfeatureNews()
    {
        $this->update(['featured_news' => false]);
    }

    public function featureHome()
    {
        $this->update(['featured_home' => true]);
    }

    public function unfeatureHome()
    {
        $this->update(['featured_home' => false]);
    }

    public function sponsorHome()
    {
        $this->update(['sponsored_home' => true]);
    }

    public function unsponsorHome()
    {
        $this->update(['sponsored_home' => false]);
    }

    public function notifySubscribers($comment) 
    {
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->user->id != $comment->user_id) {
                $subscription->user->notify(new NewsWasUpdated($this, $comment));
            }
        }
    }

    public function scopeFilter($query, $filter)
    {
        return $filter->apply($query);
    }

    // subscriptions methods
    public function subscriptions()
    {
        return $this->morphMany(Subscription::class, 'subscribed');
    }

    public function subscribe()
    {
        $attributes = ['user_id' => auth()->id()];

        if (! $this->subscriptions()->where($attributes)->exists()) {
            $this->subscriptions()->create($attributes);
        }
    }

    public function unsubscribe()
    {
        $this->subscriptions()->where(['user_id' => auth()->id()])->get()->each->delete();
    }

    public function isSubscribedTo()
    {
        return $this->subscriptions()->where('user_id', auth()->id())->exists();
    }

    public function getIsSubscribedToAttribute()
    {
        return $this->isSubscribedTo();
    }


    public function hasUpdateFor()
    {
        $key = auth()->user()->visitedContentsCacheKey($this);

        if($this->comments()->exists()) {
            $lastComment = $this->comments()->latest()->first();
            if($lastComment->user_id != auth()->id()) {
                return $lastComment->updated_at > cache($key);
            }
        }
    }

    // public function getRegionNameAttribute()
    // {
    //     if($this->region_id) {
    //         return $this->region()->first()->name;
    //     } else {
    //         return false;
    //     }
    // }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This News has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }
}
