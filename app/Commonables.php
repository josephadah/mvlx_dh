<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

trait Commonables {

    // getter for title
    public function getTitleAttribute($title)
    {
        return \Purify::clean(ucfirst($title));
    }

    // getter for image 1
	public function getImage1Attribute()
    {
        if (! $this->image1_path) {
            return false;
        }
        return asset('storage/' . $this->image1_path);
    }

    // getter for image 2
    public function getImage2Attribute()
    {
        if (! $this->image2_path) {
            return false;
        }
        return asset('storage/' . $this->image2_path);
    }

    // getter for thumbnail
    public function getThumbnailAttribute()
    {
        if (! $this->thumbnail_path) {
            return false;
        }
        return asset('storage/' . $this->thumbnail_path);
    }

    // check if the model was just published within some seconds
    public function wasJustPublished()
    {
        return $this->created_at->gt(Carbon::now()->subSeconds(10));
    }

    // get all the memtioned users
    public function mentionedUsers()
    {
        preg_match_all('/\s@([\w\_]+)/', $this->body, $matches);

        return $matches[1];
    }

    public function setBodyAttribute($body)
    {
        $this->attributes['body'] = preg_replace('/\s(@([\w\_]+))/', ' <a href="/$2">$1</a>', $body);
    }

    public function getVisitsAttribute()
    {
        return $this->visits();
    }
}