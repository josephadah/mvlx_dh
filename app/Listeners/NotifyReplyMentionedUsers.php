<?php

namespace App\Listeners;

use App\Events\ThreadReceivedNewReply;
use App\Notifications\YouWereMentioned;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyReplyMentionedUsers
{

    /**
     * Handle the event.
     *
     * @param  ThreadReceivedNewReply  $event
     * @return void
     */
    public function handle(ThreadReceivedNewReply $event)
    {
        $mentionedUsers =  User::whereIn('username', $event->reply->mentionedUsers())->get();

        foreach ($mentionedUsers as $user) {
            $user->notify(new YouWereMentioned($event->reply));
        }
    }
}
