<?php

namespace App\Listeners;

use App\Events\QuestionReceivedNewAnswer;
use App\Notifications\YouWereMentioned;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyAnswerMentionedUsers
{
    /**
     * Handle the event.
     *
     * @param  QuestionReceivedNewAnswer  $event
     * @return void
     */
    public function handle(QuestionReceivedNewAnswer $event)
    {
        $mentionedUsers =  User::whereIn('username', $event->answer->mentionedUsers())->get();

        foreach ($mentionedUsers as $user) {
            $user->notify(new YouWereMentioned($event->answer));
        }
    }
}
