<?php

namespace App\Listeners;

use App\Events\NewsReceivedNewComment;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyNewsSubscribers
{
    /**
     * Handle the event.
     *
     * @param  NewsReceivedNewComment  $event
     * @return void
     */
    public function handle(NewsReceivedNewComment $event)
    {
        $news = $event->comment->news;

        $news->notifySubscribers($event->comment);
    }
}
