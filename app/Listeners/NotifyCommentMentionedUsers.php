<?php

namespace App\Listeners;

use App\Events\NewsReceivedNewComment;
use App\Notifications\YouWereMentioned;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyCommentMentionedUsers
{
    /**
     * Handle the event.
     *
     * @param  NewsReceivedNewComment  $event
     * @return void
     */
    public function handle(NewsReceivedNewComment $event)
    {
        $mentionedUsers =  User::whereIn('username', $event->comment->mentionedUsers())->get();

        foreach ($mentionedUsers as $user) {
            $user->notify(new YouWereMentioned($event->comment));
        }
    }
}
