<?php

namespace App\Listeners;

use App\Events\QuestionReceivedNewAnswer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyQuestionSubscribers
{
    /**
     * Handle the event.
     *
     * @param  QuestionReceivedNewAnswer  $event
     * @return void
     */
    public function handle(QuestionReceivedNewAnswer $event)
    {
        $question = $event->answer->question;

        $question->notifySubscribers($event->answer);
    }
}
