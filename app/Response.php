<?php

namespace App;

use App\Commonables;
use App\Likable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Response extends Model
{
        use Likable, Commonables;
    
	protected $fillable = [
        'status_id', 'user_id', 'body', 'blocked',
        'image1_path', 'image2_path'
    ];

    protected $casts = [
        'blocked' => 'boolean'
    ];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['owner'];

    protected $appends = [
        'isLiked', 'isDisliked',
        'image1', 'image2'
    ];

    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function path() 
    {
        $perPage = config('admin.pagination.repliesPerPage');

        $responsePosition = $this->status->responses()->pluck('id')->search($this->id) + 1;

        $page = ceil($responsePosition / $perPage);

        return $this->status->showPath()."?page={$page}#response-{$this->id}";
    }

    public function wasJustPublished()
    {
        return $this->created_at->gt(Carbon::now()->subSeconds(10));
    }

    public function mentionedUsers()
    {
        preg_match_all('/@([\w\_]+)/', $this->body, $matches);

        return $matches[1];
    }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This comment has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }

    public function block()
    {
        $this->update(['blocked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);
    }
}
