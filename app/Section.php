<?php

namespace App;

use App\News;
use App\Question;
use App\Thread;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = [
        'model_type', 'name', 'slug', 
        'description'
    ];

    /**
     * Get the route key name for Laravel.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * A channel consists of threads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

}
