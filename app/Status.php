<?php

namespace App;

use App\Commonables;
use App\Notifications\StatusWasUpdated;
use App\Notifications\YouWereMentioned;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
	use Likable, RecordsActivity, RecordsVisits, Commonables;
	
    protected $fillable = [
        'user_id', 'body', 'blocked', 'featured_home', 
        'image1_path', 'image2_path'
    ];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['creator'];

    protected $appends = [
        'isLiked', 'isDisliked', 'image1', 'image2'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($builder) {
            $builder->withCount('responses')
                ->withCount('likes')
                ->withCount('dislikes');
        });

        static::deleting(function ($status) {
            $status->responses->each->delete();
        });
    }

    public function showPath()
    {
    	return '/statuses/'.$this->id;
    }

    public function creator()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function responses()
    {
    	return $this->hasMany(Response::class)->withCount('likes')->withCount('dislikes');
    }

    public function addResponse($response)
    {
        $response = $this->responses()->create($response);

        // notify status owner if others leave a response
        if($response->user_id != $this->user_id) {
            $this->creator->notify(new StatusWasUpdated($this, $response));
        }

        $this->notifyMentionedUsers($response);

        return $response;
    }

    public function notifyMentionedUsers($response)
    {
        $mentionedUsers =  User::whereIn('username', $response->mentionedUsers())->get();

        foreach ($mentionedUsers as $user) {
            $user->notify(new YouWereMentioned($response));
        }
    }

    public function block()
    {
        $this->update(['blocked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);
    }

    public function featureHome()
    {
        $this->update(['featured_home' => true]);
    }

    public function unfeatureHome()
    {
        $this->update(['featured_home' => false]);
    }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This status has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }
}
