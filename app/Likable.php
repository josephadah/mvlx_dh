<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

trait Likable {
    protected static function bootLikable()
    {
        static::deleting(function ($model) {
            $model->likes->each->delete();
        });

        static::deleting(function ($model) {
            $model->dislikes->each->delete();
        });
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'liked');
    }

    public function like()
    {
        $attributes = ['user_id' => auth()->id()];

        if (!$this->likes()->where($attributes)->exists()) {
            return $this->likes()->create($attributes);
        }
    }

    public function unlike()
    {
        $attributes = ['user_id' => auth()->id()];

        $this->likes()->where($attributes)->get()->each->delete();
    }

    public function dislikes()
    {
        return $this->morphMany(Dislike::class, 'disliked');
    }

    public function dislike()
    {
        $attributes = ['user_id' => auth()->id()];

        if (!$this->dislikes()->where($attributes)->exists()) {
            return $this->dislikes()->create($attributes);
        }
    }

    public function undislike() 
    {
        $attributes = ['user_id' => auth()->id()];

        $this->dislikes()->where($attributes)->get()->each->delete();
    }

    public function isLiked()
    {
        return !!$this->likes->where('user_id', auth()->id())->count();
    }

    public function isDisliked()
    {
        return !!$this->dislikes->where('user_id', auth()->id())->count();
    }

    // public function getLikesCountAttribute()
    // {
    //     return count($this->likes()->get());
    // }

    // public function getDislikesCountAttribute()
    // {
    //     return count($this->dislikes()->get());
    // }

    public function getIsLikedAttribute()
    {
        return $this->isLiked();
    }

    public function getIsDislikedAttribute()
    {
        return $this->isDisliked();
    }

    public function getModelNameAttribute()
    {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }
	
}