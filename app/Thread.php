<?php

namespace App;

use App\Commonables;
use App\Dislike;
use App\Events\ThreadReceivedNewReply;
use App\Likable;
use App\Like;
use App\Notifications\ThreadWasUpdated;
use App\RecordsVisits;
use App\Reply;
use App\Section;
use App\Subscription;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use Likable, RecordsActivity, RecordsVisits, Commonables;

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['creator'];

    protected $fillable = [
        'user_id', 'section_id', 'title', 
        'slug', 'body', 'locked', 'pinned', 
        'blocked', 'featured_thread', 'featured_home', 
        'image1_path', 'image2_path', 
        'sponsored_home', 'thumbnail_path'
    ];

    protected $appends = [
       'isLiked', 'isDisliked', 'isSubscribedTo',
        'image1', 'image2', 'thumbnail'
    ];

    protected $casts = [
        'locked' => 'boolean',
        'blocked' => 'boolean',
        'pinned' => 'boolean',
        'featured_thread' => 'boolean',
        'featured_home' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($builder) {
            $builder->withCount('replies')
                ->withCount('likes')
                ->withCount('dislikes');
        });
        
        static::creating(function ($thread) {
            Reputation::award($thread->creator, Reputation::THREAD_WAS_CREATED);
        });

        static::deleting(function ($thread) {
            $thread->replies->each->delete();

            Reputation::reduce($thread->creator, Reputation::THREAD_WAS_CREATED);
        });
    }
	
    public function path()
    {
    	return "/threads/{$this->id}";
    }

    public function showPath()
    {
        return "/threads/{$this->id}/{$this->slug}";
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class)->withCount('likes')->withCount('dislikes');
    }

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        // event to handle lot of things like sending
        // notification to thread subscribers and mentioned users
        event(new ThreadReceivedNewReply($reply));

        // notify thread owner if others leave a reply
        if($reply->user_id != $this->user_id) {
            $this->creator->notify(new ThreadWasUpdated($this, $reply));
        }

        return $reply;
    }

    public function lock()
    {
        $this->update(['locked' => true]);
    }

    public function unlock()
    {
        $this->update(['locked' => false]);
    }

    public function block()
    {
        $this->update(['blocked' => true]);

        $this->update(['locked' => true]);
    }

    public function unblock()
    {
        $this->update(['blocked' => false]);

        $this->update(['locked' => false]);
    }

    public function featureThread()
    {
        $this->update(['featured_thread' => true]);
    }

    public function unfeatureThread()
    {
        $this->update(['featured_thread' => false]);
    }

    public function featureHome()
    {
        $this->update(['featured_home' => true]);
    }

    public function unfeatureHome()
    {
        $this->update(['featured_home' => false]);
    }

    public function sponsorHome()
    {
        $this->update(['sponsored_home' => true]);
    }

    public function unsponsorHome()
    {
        $this->update(['sponsored_home' => false]);
    }

    public function notifySubscribers($reply) 
    {
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->user->id != $reply->user_id) {
                $subscription->user->notify(new ThreadWasUpdated($this, $reply));
            }
        }
    }

    public function scopeFilter($query, $filter)
    {
        return $filter->apply($query);
    }

    // subscriptions methods
    public function subscriptions()
    {
        return $this->morphMany(Subscription::class, 'subscribed');
    }

    public function subscribe()
    {
        $attributes = ['user_id' => auth()->id()];

        if (! $this->subscriptions()->where($attributes)->exists()) {
            $this->subscriptions()->create($attributes);
        }
    }

    public function unsubscribe()
    {
        $this->subscriptions()->where(['user_id' => auth()->id()])->get()->each->delete();
    }

    public function isSubscribedTo()
    {
        return $this->subscriptions()->where('user_id', auth()->id())->exists();
    }

    public function getIsSubscribedToAttribute()
    {
        return $this->isSubscribedTo();
    }


    public function hasUpdateFor()
    {
        $key = auth()->user()->visitedContentsCacheKey($this);

        if($this->replies()->exists()) {
            $lastReply = $this->replies()->latest()->first();
            if($lastReply->user_id != auth()->id()) {
                return $lastReply->updated_at > cache($key);
            }
        }
    }

    public function getBodyAttribute($body)
    {
        if($this->blocked) {
            return '<p style="color:red;">This thread has been blocked.</p>';
        }
        return \Purify::clean(ucfirst($body));
    }

}
