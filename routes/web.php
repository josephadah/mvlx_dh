<?php

use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();
Route::get('/reset-request-sent', 'UserController@resetRequestSent')->name('reset-request-sent');
Route::get('/reset-successful', 'UserController@resetSuccessful')
		->middleware('auth')->name('reset-successful');
Route::get('/register/confirm', 'Auth\RegisterConfirmationController@index')
		->name('register.confirm');

Route::get('/forum', 'ForumController@home')->name('forum.home');
Route::get('/forum/{section}', 'ForumController@section')->name('forum.section');
Route::get('/questions-home', 'QuestionsController@home')->name('questions.home');
Route::get('/questions-section/{section}', 'QuestionsController@section')->name('questions.section');
Route::get('/news-home', 'NewsController@home')->name('news.home');
Route::get('/section/{section}', 'NewsController@section')->name('section');

Route::get('/promo', 'PromoController@index')->name('promo.index');

//news routes
Route::get('/news/create', 'NewsController@create')
		->middleware('auth', 'must-be-confirmed', 'user-not-banned')->name('news.create');
Route::post('/news', 'NewsController@store')
		->middleware('auth', 'must-be-confirmed', 'user-not-banned')->name('news.store');
Route::get('/news', 'NewsController@index')->name('news.index');
Route::get('/news/trending', 'NewsController@trending')->name('news.trending');
Route::patch('/news/{news}', 'NewsController@update')
		->middleware('auth')->name('news.update');
Route::delete('/news/{news}', 'NewsController@destroy')->middleware('auth')->name('news.destroy');
Route::get('/news/{news}/edit', 'NewsController@edit')->middleware('auth')->name('news.edit');
Route::get('/news/{news}/comments', 'CommentController@index')->name('comments.index');
Route::post('/news/{news}/comments', 'CommentController@store')
		->middleware('auth', 'must-be-confirmed', 'user-not-banned')->name('comments.store');
Route::post('/update-comments/{comment}', 'CommentController@update')
		->middleware('auth')->name('comments.update');
Route::delete('/comments/{comment}', 'CommentController@destroy')
		->middleware('auth')->name('comments.destroy');

Route::get('/news-report/{news}', 'NewsController@createReport')
		->middleware('auth')->name('news.report');
Route::get('/comments-report/{comment}', 'CommentController@createReport')
		->middleware('auth')->name('comments.report');

Route::get('/news/{news}/login', 'NewsController@loginToComment')
		->middleware('auth', 'must-be-confirmed', 'user-not-banned')->name('news.login');
Route::get('/news/{news}/{slug}', 'NewsController@show')->name('news.show');

// statuses routes
Route::get('/statuses/{status}', 'StatusController@show')->name('statuses.show');
Route::post('/statuses', 'StatusController@store')
		->middleware('auth', 'must-be-confirmed', 'user-not-banned')->name('statuses.store');
Route::patch('/statuses/{status}', 'StatusController@update')
		->middleware('auth')->name('statuses.update');
Route::delete('/statuses/{status}', 'StatusController@destroy')
		->middleware('auth')->name('statuses.destroy');
Route::get('/statuses/{status}/edit', 'StatusController@edit')
		->middleware('auth')->name('statuses.edit');
Route::get('/statuses/{status}/responses', 'ResponseController@index')->name('responses.index');
Route::post('/statuses/{status}/responses', 'ResponseController@store')
		->middleware('auth', 'user-not-banned')->name('responses.store');
Route::post('/update-responses/{response}', 'ResponseController@update')
		->middleware('auth')->name('responses.update');
Route::delete('/responses/{response}', 'ResponseController@destroy')
		->middleware('auth')->name('responses.destroy');

Route::get('/statuses-report/{status}', 'StatusController@createReport')
		->middleware('auth')->name('statuses.report');
Route::get('/responses-report/{response}', 'ResponseController@createReport')
		->middleware('auth')->name('responses.report');


Route::middleware('auth')->group(function() {
	//likes routes
	Route::post('/threads/{thread}/likes', 'LikeController@likeThread');
	Route::delete('/threads/{thread}/likes', 'LikeController@unlikeThread');
	Route::post('/threads/{thread}/dislikes', 'LikeController@dislikeThread');
	Route::delete('/threads/{thread}/dislikes', 'LikeController@undislikeThread');

	Route::post('/questions/{question}/likes', 'LikeController@likeQuestion');
	Route::delete('/questions/{question}/likes', 'LikeController@unlikeQuestion');
	Route::post('/questions/{question}/dislikes', 'LikeController@dislikeQuestion');
	Route::delete('/questions/{question}/dislikes', 'LikeController@undislikeQuestion');

	Route::post('/news/{news}/likes', 'LikeController@likeNews');
	Route::delete('/news/{news}/likes', 'LikeController@unlikeNews');
	Route::post('/news/{news}/dislikes', 'LikeController@dislikeNews');
	Route::delete('/news/{news}/dislikes', 'LikeController@undislikeNews');

	Route::post('/statuses/{status}/likes', 'LikeController@likeStatus');
	Route::delete('/statuses/{status}/likes', 'LikeController@unlikeStatus');
	Route::post('/statuses/{status}/dislikes', 'LikeController@dislikeStatus');
	Route::delete('/statuses/{status}/dislikes', 'LikeController@undislikeStatus');

	Route::post('/replies/{reply}/likes', 'LikeController@likeReply');
	Route::delete('/replies/{reply}/likes', 'LikeController@unlikeReply');
	Route::post('/replies/{reply}/dislikes', 'LikeController@dislikeReply');
	Route::delete('/replies/{reply}/dislikes', 'LikeController@undislikeReply');

	Route::post('/answers/{answer}/likes', 'LikeController@likeAnswer');
	Route::delete('/answers/{answer}/likes', 'LikeController@unlikeAnswer');
	Route::post('/answers/{answer}/dislikes', 'LikeController@dislikeAnswer');
	Route::delete('/answers/{answer}/dislikes', 'LikeController@undislikeAnswer');

	Route::post('/comments/{comment}/likes', 'LikeController@likeComment');
	Route::delete('/comments/{comment}/likes', 'LikeController@unlikeComment');
	Route::post('/comments/{comment}/dislikes', 'LikeController@dislikeComment');
	Route::delete('/comments/{comment}/dislikes', 'LikeController@undislikeComment');

	Route::post('/responses/{response}/likes', 'LikeController@likeResponse');
	Route::delete('/responses/{response}/likes', 'LikeController@unlikeResponse');
	Route::post('/responses/{response}/dislikes', 'LikeController@dislikeResponse');
	Route::delete('/responses/{response}/dislikes', 'LikeController@undislikeResponse');

	// subscription routes
	Route::post('/threads/{thread}/subscriptions', 'SubscriptionController@subscribeThread');
	Route::delete('/threads/{thread}/subscriptions', 'SubscriptionController@unsubscribeThread');

	Route::post('/questions/{question}/subscriptions', 'SubscriptionController@subscribeQuestion');
	Route::delete('/questions/{question}/subscriptions', 'SubscriptionController@unsubscribeQuestion');

	Route::post('/news/{news}/subscriptions', 'SubscriptionController@subscribeNews');
	Route::delete('/news/{news}/subscriptions', 'SubscriptionController@unsubscribeNews');

	// notification route
	Route::get('/notifications', 'UserNotificationController@allNotifications');
	Route::get('/delete-all-notifications', 'UserNotificationController@deleteAll')
			->name('delete.all.notifications');
	Route::delete('/notifications/{notification}', 'UserNotificationController@destroy');

	// appeal routes
	Route::get('/appeal', 'AppealController@index')->name('appeals.index');
	Route::get('/appeal/create', 'AppealController@create')->name('appeals.create');
	Route::post('/appeal', 'AppealController@store')->name('appeals.store');
	Route::delete('/appeal/{appeal}', 'AppealController@destroy')->name('appeals.destroy');

});


// report routes
Route::post('/report', 'ReportController@store')->middleware('auth')->name('reports.store');
Route::get('/report', 'ReportController@index')
	->middleware('auth', 'admin')->name('reports.index');
Route::delete('/report/{report}', 'ReportController@destroy')
	->middleware('auth', 'admin')->name('reports.destroy');
Route::post('report-blocked/{report}', 'ReportController@update')
	->middleware('auth', 'admin')->name('reports.blocked');

// management routes
Route::prefix('manage')
	->middleware('auth', 'admin')
	->group(function() {
	Route::get('/', 'ManageController@index')->name('manage.index');
	Route::get('/users', 'ManageUsersController@index')->name('manage.users');
	Route::get('/users/search', 'ManageUsersController@search')->name('manage.users.search');
	Route::get('/dashboard', 'ManageController@dashboard')->name('manage.dashboard');
	//sections routes
	Route::get('/sections', 'SectionsController@index')->name('sections.index');
	Route::post('/sections', 'SectionsController@store')->name('sections.store');
	Route::get('/sections/{section}/edit', 'SectionsController@edit')->name('sections.edit');
	Route::patch('/sections/{section}', 'SectionsController@update')->name('sections.update');
	Route::delete('/sections/{section}', 'SectionsController@destroy')->name('sections.destroy');
});
	
Route::middleware('auth', 'admin')
	->group(function() {
	//lock contents routes
	Route::post('/locked-threads/{thread}', 'LockedContentsController@lockThread')
			->name('locked-threads.store');
	Route::delete('/locked-threads/{thread}', 'LockedContentsController@unlockThread')
			->name('locked-threads.destroy');

	Route::post('/locked-questions/{question}', 'LockedContentsController@lockQuestion')
			->name('locked-questions.store');
	Route::delete('/locked-questions/{question}', 'LockedContentsController@unlockQuestion')
			->name('locked-questions.destroy');

	Route::post('/locked-news/{news}', 'LockedContentsController@lockNews')
			->name('locked-news.store');
	Route::delete('/locked-news/{news}', 'LockedContentsController@unlockNews')
			->name('locked-news.destroy');

	// block contents routes
	Route::post('/blocked-threads/{thread}', 'BlockedContentsController@blockThread')
			->name('blocked-threads.store');
	Route::delete('/blocked-threads/{thread}', 'BlockedContentsController@unblockThread')
			->name('blocked-threads.destroy');

	Route::post('/blocked-questions/{question}', 'BlockedContentsController@blockQuestion')
			->name('blocked-questions.store');
	Route::delete('/blocked-questions/{question}', 'BlockedContentsController@unblockQuestion')
			->name('blocked-questions.destroy');

	Route::post('/blocked-news/{news}', 'BlockedContentsController@blockNews')
			->name('blocked-news.store');
	Route::delete('/blocked-news/{news}', 'BlockedContentsController@unblockNews')
			->name('blocked-news.destroy');

	Route::post('/blocked-statuses/{status}', 'BlockedContentsController@blockStatus')
			->name('blocked-statuses.store');
	Route::delete('/blocked-statuses/{status}', 'BlockedContentsController@unblockStatus')
			->name('blocked-statuses.destroy');

	Route::post('/blocked-replies/{reply}', 'BlockedContentsController@blockReply')
			->name('blocked-replies.store');
	Route::delete('/blocked-replies/{reply}', 'BlockedContentsController@unblockReply')
			->name('blocked-replies.destroy');

	Route::post('/blocked-answers/{answer}', 'BlockedContentsController@blockAnswer')
			->name('blocked-answers.store');
	Route::delete('/blocked-answers/{answer}', 'BlockedContentsController@unblockAnswer')
			->name('blocked-answers.destroy');

	Route::post('/blocked-comments/{comment}', 'BlockedContentsController@blockComment')
			->name('blocked-comments.store');
	Route::delete('/blocked-comments/{comment}', 'BlockedContentsController@unblockComment')
			->name('blocked-comments.destroy');

	Route::post('/blocked-responses/{response}', 'BlockedContentsController@blockResponse')
			->name('blocked-responses.store');
	Route::delete('/blocked-responses/{response}', 'BlockedContentsController@unblockResponse')
			->name('blocked-responses.destroy');

	// pin contents routes
	Route::post('pinned-threads/{thread}', 'PinnedContentsController@pinThread')
			->name('pinned-threads.store');
	Route::delete('pinned-threads/{thread}', 'PinnedContentsController@unpinThread')
			->name('pinned-threads.destroy');

	Route::post('pinned-questions/{question}', 'PinnedContentsController@pinQuestion')
			->name('pinned-questions.store');
	Route::delete('pinned-questions/{question}', 'PinnedContentsController@unpinQuestion')
			->name('pinned-questions.destroy');

	Route::post('pinned-news/{news}', 'PinnedContentsController@pinNews')
			->name('pinned-news.store');
	Route::delete('pinned-news/{news}', 'PinnedContentsController@unpinNews')
			->name('pinned-news.destroy');

	Route::post('ban-user/{user}', 'BanUserController@store');
	Route::delete('ban-user/{user}', 'BanUserController@destroy');

	// feature contents in the contents home page
	Route::post('/featured-threads/{thread}', 'FeaturedContentsController@featureThread');
	Route::delete('/featured-threads/{thread}', 'FeaturedContentsController@unfeatureThread');
	Route::post('/featured-questions/{question}', 'FeaturedContentsController@featureQuestion');
	Route::delete('/featured-questions/{question}', 'FeaturedContentsController@unfeatureQuestion');
	Route::post('/featured-news/{news}', 'FeaturedContentsController@featureNews');
	Route::delete('/featured-news/{news}', 'FeaturedContentsController@unfeatureNews');

	// feature contents in the site home page
	Route::post('/featured-home-threads/{thread}', 'FeaturedContentsController@featureHomeThread');
	Route::delete('/featured-home-threads/{thread}', 'FeaturedContentsController@unfeatureHomeThread');
	Route::post('/featured-home-questions/{question}', 'FeaturedContentsController@featureHomeQuestion');
	Route::delete('/featured-home-questions/{question}', 'FeaturedContentsController@unfeatureHomeQuestion');
	Route::post('/featured-home-news/{news}', 'FeaturedContentsController@featureHomeNews');
	Route::delete('/featured-home-news/{news}', 'FeaturedContentsController@unfeatureHomeNews');
	Route::post('/featured-home-statuses/{status}', 'FeaturedContentsController@featureHomeStatus');
	Route::delete('/featured-home-statuses/{status}', 'FeaturedContentsController@unfeatureHomeStatus');
});

// superadmin routes
Route::middleware('auth', 'superadmin')
	->group(function() {
	// sponsor posts on the site home page
	Route::post('/sponsored-home-news/{news}', 'FeaturedContentsController@sponsorHomeNews');
	Route::delete('/sponsored-home-news/{news}', 'FeaturedContentsController@unsponsorHomeNews');
	Route::post('/sponsored-home-threads/{thread}', 'FeaturedContentsController@sponsorHomeThread');
	Route::delete('/sponsored-home-threads/{thread}', 'FeaturedContentsController@unsponsorHomeThread');
	Route::post('/sponsored-home-questions/{question}', 'FeaturedContentsController@sponsorHomeQuestion');
	Route::delete('/sponsored-home-questions/{question}', 'FeaturedContentsController@unsponsorHomeQuestion');
	
	// adverts routes
	Route::get('/adverts', 'AdvertController@index')->name('adverts.index');
	Route::post('/adverts', 'AdvertController@store')->name('adverts.store');
	Route::get('/adverts/{advert}', 'AdvertController@show')->name('adverts.show');
	Route::get('/adverts/{advert}/edit', 'AdvertController@edit')->name('adverts.edit');
	Route::patch('/adverts/{advert}', 'AdvertController@update')->name('adverts.update');
	Route::delete('/adverts/{advert}', 'AdvertController@destroy')->name('adverts.destroy');

	// reward routes
	Route::post('/award-coin/{user}', 'RewardController@store')->name('award-coin');
	Route::delete('/award-coin/{user}', 'RewardController@destroy')->name('reduce-coin');

	// mails routes
	Route::get('mails', 'MailsController@index')->name('mails.index');
	Route::post('mails', 'MailsController@store')->name('mails.store');
	Route::get('mails-update-list', 'MailsController@updateList')->name('mails.update.list');
});

// apis
Route::get('/api/users', 'Api\UsersController@index');
Route::get('/api/regions', 'NewsRegionController@index');
Route::post('/api/users/{user}/avatar', 'Api\UserAvatarController@store')
		->middleware('auth');
Route::delete('/api/users/{user}/avatar', 'Api\UserAvatarController@destroy')
		->middleware('auth');

// search routes
Route::get('/users', 'Api\UsersController@search')->name('search.user');
Route::get('/search-users', 'Api\UsersController@create')->name('search.page');
Route::get('/search-news', 'NewsRegionController@search')->name('search.news');

// user feedbacks
Route::get('/users-feedback', 'FeedbackController@index')->name('feedbacks.index');
Route::get('/users-feedback/create', 'FeedbackController@create')->name('feedbacks.create');
Route::post('/users-feedback', 'FeedbackController@store')->name('feedbacks.store');
Route::delete('/users-feedback/{feedback}', 'FeedbackController@destroy')->name('feedbacks.destroy');

// pages routes
Route::get('/about-us', 'PagesController@about')->name('about-us');
Route::get('/contact-us', 'PagesController@contact')->name('contact-us');
Route::get('/terms', 'PagesController@terms')->name('terms');
Route::get('/privacy', 'PagesController@privacy')->name('privacy');
Route::get('/user-ban', 'PagesController@userBan')->middleware('auth')->name('user.ban');
Route::get('/email-not-confirmed', 'PagesController@notConfirmed')
		->middleware('auth')->name('not-confirmed');
Route::get('/resend-email', 'PagesController@resendConfirmationEmail')
		->middleware('auth')->name('resend.email');
Route::get('/registration-success', 'PagesController@registrationSuccess')
		->middleware('auth');
Route::get('/change-email', 'PagesController@changeEmail')->middleware('auth')->name('change-email');
Route::post('/change-email/{user}', 'PagesController@storeChangeEmail')
		->middleware('auth')->name('store-change-email');

// user routes
Route::post('/follow-user/{user}', 'FollowsController@store')->name('follow.user')
		->middleware('auth');
Route::delete('/follow-user/{user}', 'FollowsController@destroy')->name('unfollow.user')
		->middleware('auth');
Route::get('/followers/{user}', 'FollowsController@showFollowers')->name('followers.show');
Route::get('/followings/{user}', 'FollowsController@showFollowing')->name('followings.show');

Route::post('/request-contact/{user}', 'UserController@requestContact')->middleware('auth');
Route::get('/contact-requests', 'UserController@allRequest')->middleware('auth')->name('contact-requests');
Route::post('/approve-request/{user}', 'UserController@approveRequest')
		->middleware('auth')->name('approve-request');
Route::post('/decline-request/{user}', 'UserController@declineRequest')
		->middleware('auth')->name('decline-request');

Route::post('/profile/{user}', 'UserController@storeProfile')->name('profile.store')->middleware('auth');
Route::get('/{user}', 'UserController@profile')->name('profile');
Route::get('/{user}/subscriptions', 'UserController@subscriptions')->name('user.subscriptions')
		->middleware('auth');
Route::get('/{user}/thread-subscriptions', 'UserController@threadSubscriptions')
		->name('user.threadsubscriptions')->middleware('auth');
Route::get('/{user}/question-subscriptions', 'UserController@questionSubscriptions')
		->name('user.questionsubscriptions')->middleware('auth');
Route::get('/{user}/news-subscriptions', 'UserController@newsSubscriptions')
		->name('user.newssubscriptions')->middleware('auth');
Route::get('/{user}/notifications', 'UserNotificationController@index')
		->middleware('auth');
Route::get('/{user}/statuses', 'StatusController@index')->name('user.statuses');