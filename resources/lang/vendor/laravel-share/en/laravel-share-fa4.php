<?php

return [
    'facebook' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-facebook-official is-size-4 has-text-primary" style="margin:2px; padding:2px; background-color:#fff;"></span></a></li>',
    'twitter' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-twitter is-size-4 has-text-primary" style="margin:2px; padding:2px; background-color:#fff;"></span></a></li>',
    'gplus' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-google-plus is-size-4 has-text-danger" style="margin:2px; padding:2px; background-color:#fff;"></span></a></li>',
    'linkedin' => '<li><a href=":url" class="social-button :class" id=":id"><span class="fa fa-linkedin is-size-4 has-text-primary" style="margin:2px; padding:2px; background-color:#fff;"></span></a></li>',
];
 