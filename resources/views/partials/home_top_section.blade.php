<p class="m-b-10">
	<a href="{{ route('section', 'general') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline is-underline">General</a>
	<a href="{{ route('section', 'politics') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Politics</a>
	<a href="{{ route('section', 'business') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Business</a>
	<a href="{{ route('section', 'education') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Education</a>
	<a href="{{ route('section', 'sports') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Sports</a>
	<a href="{{ route('section', 'health') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Health</a>
	<a href="{{ route('section', 'technology') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Technology</a>
	<a href="{{ route('section', 'job-career') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Job/Career</a>
	<a href="{{ route('section', 'family-relationship') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Family/Relationship</a>
	<a href="{{ route('section', 'nysc') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">NYSC</a>
	<a href="{{ route('section', 'religion') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Religion</a>
	<a href="{{ route('section', 'entertainment') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Entertainment</a>
	<a href="{{ route('section', 'africa') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Africa</a>
	<a href="{{ route('section', 'international') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">International</a>
</p>

<a href="{{ route('news.create') }}" class="button is-small is-link is-outlined has-text-weight-semibold">
    Create New Post
</a>