<div class="media has-border-bottom">
    <figure class="media-left">
        <p class="image is-48x48">
            @auth
                <a href="{{ route('profile', auth()->user()->username) }}">
                    <img class="profile-image-thumbnail-32x32 has-border-circle" 
                            src="{{ auth()->user()->thumbnail }}" 
                            alt="">
                </a>
            @else
                <img class="profile-image-thumbnail-32x32 has-border-circle" 
                            src="{{ asset('storage/images/avatars/default.png') }}"
                            alt="">
            @endauth
        </p>
    </figure>
    <div class="media-content is-size-7">
        <p class="m-l-10 is-size-7">
            <strong>
                Welcome! {{ auth()->check() ? auth()->user()->username : 'Guest.' }}
            </strong>
        </p>
        <p>You have: 
            @auth
                <span class="m-r-20">
                    <strong>{{ auth()->user()->reputation }} xp, </strong>
                </span>
                <span>
                    <strong>
                    {{ $rewardCount = auth()->user()->reward }} {{ str_plural('Ucoin', $rewardCount) }}
                    </strong>
                </span>
            @else
                <strong class="m-r-20">0 xp, </strong>
                <strong>0 Ucoin</strong>
            @endauth
        </p>
        <p>
            @auth
                <span class="m-r-20">
                    <a href="{{ route('followers.show', auth()->user()->username) }}">
                        <strong>Followers:</strong>
                    </a> <strong>{{ auth()->user()->followers()->count() }}</strong>
                </span>
                <span>
                    <a href="{{ route('followings.show', auth()->user()->username) }}"><strong>Following:</strong></a> <strong>{{ auth()->user()->follows()->count() }}
                    </strong>
                </span>
                <p>
                    <a class="is-size-7 is-underline m-r-10" href="{{ route('user.statuses', auth()->user()->username) }}">My Statuses
                    </a>
                    <a class="is-size-7 is-underline m-l-10" href="{{ route('user.subscriptions', auth()->user()->username) }}">
                        My Subscriptions
                    </a>
                </p>
                
                @if($requestsCount = auth()->user()->requestsCount)
                    <p class="has-text-centered is-size-7">
                        <a href="{{ route('contact-requests') }}" class="is-darkgreen">
                            Contact Requests
                        </a>
                        <span class="notifications-count is-size-7">{{ $requestsCount }}</span>
                    </p>
                @endif

            @else
                <span class="m-r-20"><strong>0</strong> Followers,</span>
                <span><strong>0</strong> Followings</span>
            @endauth
        </p>
    </div>
</div>
<div>
    <p class="m-b-10">
        <a href="{{ route('section', 'news-general') }}" class="has-text-weight-semibold is-size-7 is-underline">General</a>
        <a href="{{ route('section', 'news-politics') }}" class="has-text-weight-semibold is-size-7 is-underline">Politics</a>
        <a href="{{ route('section', 'news-business') }}" class="has-text-weight-semibold is-size-7 is-underline">Business</a>
        <a href="{{ route('section', 'news-education') }}" class="has-text-weight-semibold is-size-7 is-underline">Education</a>
        <a href="{{ route('section', 'news-sports') }}" class="has-text-weight-semibold is-size-7 is-underline">Sports</a>
        <a href="{{ route('section', 'news-health') }}" class="has-text-weight-semibold is-size-7 is-underline">Health</a>
        <a href="{{ route('section', 'news-technology') }}" class="has-text-weight-semibold is-size-7 is-underline">Technology</a>
        <a href="{{ route('section', 'news-job-career') }}" class="has-text-weight-semibold is-size-7 is-underline">Job/Career</a>
        <a href="{{ route('section', 'news-family-relationship') }}" class="has-text-weight-semibold is-size-7 is-underline">Family/Relationship</a>
        <a href="{{ route('section', 'news-nysc') }}" class="has-text-weight-semibold is-size-7 is-underline">NYSC</a>
        <a href="{{ route('section', 'news-religion') }}" class="has-text-weight-semibold is-size-7 is-underline">Religion</a>
        <a href="{{ route('section', 'news-entertainment') }}" class="has-text-weight-semibold is-size-7 is-underline">Entertainment</a>
        <a href="{{ route('section', 'thread-africa') }}" class="has-text-weight-semibold is-size-7 is-underline">Africa</a>
        <a href="{{ route('section', 'thread-international') }}" class="has-text-weight-semibold is-size-7 is-underline">International</a>
    </p>

    <a href="{{ route('news.create') }}" class="button is-small is-link is-outlined has-text-weight-semibold">
        Create New Post
    </a>
</div>