<div class="box is-marginless p-5 has-text-centered">
	<p class="is-hidden-mobile">
		<a href="{{ route('section', 'general') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline is-underline">General</a>
		<a href="{{ route('section', 'politics') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Politics</a>
		<a href="{{ route('section', 'business') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Business</a>
		<a href="{{ route('section', 'education') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Education</a>
		<a href="{{ route('section', 'sports') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Sports</a>
		<a href="{{ route('section', 'health') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Health</a>
		<a href="{{ route('section', 'technology') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Technology</a>
		<a href="{{ route('section', 'job-career') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Job/Career</a>
		<a href="{{ route('section', 'family-relationship') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Family/Relationship</a>
		<a href="{{ route('section', 'nysc') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">NYSC</a>
		<a href="{{ route('section', 'religion') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Religion</a>
		<a href="{{ route('section', 'entertainment') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Entertainment</a>
		<a href="{{ route('section', 'africa') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Africa</a>
		<a href="{{ route('section', 'international') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">International</a>
	</p>

	{{-- show only on mobile --}}
	<p class="is-hidden-tablet">
		<a href="{{ route('section', 'news-general') }}" class="has-text-weight-semibold is-size-7 is-underline">General</a>
        <a href="{{ route('section', 'news-politics') }}" class="has-text-weight-semibold is-size-7 is-underline">Politics</a>
        <a href="{{ route('section', 'news-business') }}" class="has-text-weight-semibold is-size-7 is-underline">Business</a>
        <a href="{{ route('section', 'news-education') }}" class="has-text-weight-semibold is-size-7 is-underline">Education</a>
        <a href="{{ route('section', 'news-sports') }}" class="has-text-weight-semibold is-size-7 is-underline">Sports</a>
        <a href="{{ route('section', 'news-health') }}" class="has-text-weight-semibold is-size-7 is-underline">Health</a>
        <a href="{{ route('section', 'news-technology') }}" class="has-text-weight-semibold is-size-7 is-underline">Technology</a>
        <a href="{{ route('section', 'news-job-career') }}" class="has-text-weight-semibold is-size-7 is-underline">Job/Career</a>
        <a href="{{ route('section', 'news-family-relationship') }}" class="has-text-weight-semibold is-size-7 is-underline">Family/Relationship</a>
        <a href="{{ route('section', 'news-nysc') }}" class="has-text-weight-semibold is-size-7 is-underline">NYSC</a>
        <a href="{{ route('section', 'news-religion') }}" class="has-text-weight-semibold is-size-7 is-underline">Religion</a>
        <a href="{{ route('section', 'news-entertainment') }}" class="has-text-weight-semibold is-size-7 is-underline">Entertainment</a>
        <a href="{{ route('section', 'thread-africa') }}" class="has-text-weight-semibold is-size-7 is-underline">Africa</a>
        <a href="{{ route('section', 'thread-international') }}" class="has-text-weight-semibold is-size-7 is-underline">International</a>
	</p>

	<div class="columns is-centered">
		<div class="column is-12 m-10">
			<form method="GET" action="{{ route('search.news') }}">
			    @csrf
			    <div class="field has-addons is-expanded">
			        <div class="control is-expanded">
			            <input type="text" name="region" placeholder="Search News by Region" 
			            class="input" id="searchRegion">
			        </div>
			        <div class="control">
			            <button type="submit" class="button is-link">
			                <i class="fa fa-search"></i>
			            </button>
			        </div>
			    </div>
			</form>
		</div>
	</div>
</div>

<div class="has-text-centered m-t-10 m-b-10">
	<p><strong>What's happening around you?</strong></p>
	<a href="{{ route('news.create') }}" class="button is-link is-size-6-mobile">
		Create New Post
	</a>
	<p class="is-size-6 has-text-grey m-t-10">
		You earn XP points or Ucoins when you Create New News !
	</p>
</div>