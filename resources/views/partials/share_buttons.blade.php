<span class="dropdown is-up is-clickable">
  <span class="dropdown-trigger">
  	<button class="button is-small is-outlined is-link">
      <span class="is-size-7">Share</span>
      <span class="icon is-small">
        <i class="fa fa-share-alt" aria-hidden="true"></i>
      </span>
	</button>
  </span>
  <span class="dropdown-menu p-0 m-0" id="dropdown-menu7" role="menu">
    <span class="dropdown-content">
      <span class="dropdown-item p-b-0 m-b-0">
	        {!! Share::currentPage($title)->facebook()->twitter()->googlePlus()->linkedin() !!}
      </span>
    </span>
  </span>
</span>