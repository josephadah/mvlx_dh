<div class="box is-marginless p-10">
	<p class="title is-6 is-underline is-marginless">Section</p>
	<p>
		<a href="{{ route('questions.section', 'question-general') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">General</a>
		<a href="{{ route('questions.section', 'question-science') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Science</a>
		<a href="{{ route('questions.section', 'question-art-literature') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Art/Literature</a>
		<a href="{{ route('questions.section', 'question-programming') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Programming</a>
		<a href="{{ route('questions.section', 'question-business') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Business</a>
		<a href="{{ route('questions.section', 'question-education') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Education</a>
		<a href="{{ route('questions.section', 'question-health') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Health</a>
		<a href="{{ route('questions.section', 'question-technology') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Technology</a>
		<a href="{{ route('questions.section', 'question-job-career') }}" class="tag has-text-weight-semibold is-size-7 m-t-5">Job/Career</a>
	</p>
</div>

<div class="has-text-centered m-t-10 m-b-10">
	<p><strong>Have a Question to Ask?</strong></p>
	<a href="{{ route('questions.create') }}" class="button is-link is-size-6-mobile">
		Ask Question Now
	</a>
	<p class="is-size-6 has-text-grey m-t-10">
		You earn XP points or Ucoins when you Create New Question !
	</p>
</div>