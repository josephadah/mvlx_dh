<span class="dropdown is-up is-clickable">
	<span class="dropdown-trigger">
	      <span class="button is-small is-outlined is-danger">Delete</span>
	</span>
	<span class="dropdown-menu p-5 m-0 is-white-background has-border" id="dropdown-menu7" role="menu">
	    <span class="dropdown-content">
	      <span class="dropdown-item p-b-0 m-b-0">
		        <span>Are you sure?</span><br>
		        <span>
		        	<button type="submit" class="button is-danger is-small is-outlined">Yes</button>
		        	<a class="button is-small m-l-5 is-clickable">No</a>
		        </span>
	      </span>
	    </span>
	</span>
</span>