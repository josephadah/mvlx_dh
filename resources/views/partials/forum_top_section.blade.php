<div class="box is-marginless p-5 has-text-centered">
	<p class="title is-6 is-underline is-marginless">Thread Sections</p>
	<p class="is-hidden-mobile">
		<a href="{{ route('forum.section', 'thread-general') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">General</a>
		<a href="{{ route('forum.section', 'thread-politics') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Politics</a>
		<a href="{{ route('forum.section', 'thread-business') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Business</a>
		<a href="{{ route('forum.section', 'thread-education') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Education</a>
		<a href="{{ route('forum.section', 'thread-sports') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Sports</a>
		<a href="{{ route('forum.section', 'thread-health') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Health</a>
		<a href="{{ route('forum.section', 'thread-technology') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Technology</a>
		<a href="{{ route('forum.section', 'thread-job-career') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Job/Career</a>
		<a href="{{ route('forum.section', 'thread-nysc') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">NYSC</a>
		<a href="{{ route('forum.section', 'thread-family-relationship') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Family/Relationship</a>
		<a href="{{ route('forum.section', 'thread-religion') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Religion</a>
		<a href="{{ route('forum.section', 'thread-entertainment') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Entertainment</a>
		<a href="{{ route('forum.section', 'thread-africa') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">Africa</a>
		<a href="{{ route('forum.section', 'thread-international') }}" class="tag has-text-weight-semibold is-size-7 m-t-5 is-underline">International</a>
	</p>

	{{-- show only on mobile --}}
	<p class="is-hidden-tablet">
		<a href="{{ route('forum.section', 'thread-general') }}" class="has-text-weight-semibold is-size-7 is-underline">General</a>
		<a href="{{ route('forum.section', 'thread-politics') }}" class="has-text-weight-semibold is-size-7 is-underline">Politics</a>
		<a href="{{ route('forum.section', 'thread-business') }}" class="has-text-weight-semibold is-size-7 is-underline">Business</a>
		<a href="{{ route('forum.section', 'thread-education') }}" class="has-text-weight-semibold is-size-7 is-underline">Education</a>
		<a href="{{ route('forum.section', 'thread-sports') }}" class="has-text-weight-semibold is-size-7 is-underline">Sports</a>
		<a href="{{ route('forum.section', 'thread-health') }}" class="has-text-weight-semibold is-size-7 is-underline">Health</a>
		<a href="{{ route('forum.section', 'thread-technology') }}" class="has-text-weight-semibold is-size-7 is-underline">Technology</a>
		<a href="{{ route('forum.section', 'thread-job-career') }}" class="has-text-weight-semibold is-size-7 is-underline">Job/Career</a>
		<a href="{{ route('forum.section', 'thread-nysc') }}" class="has-text-weight-semibold is-size-7 is-underline">NYSC</a>
		<a href="{{ route('forum.section', 'thread-family-relationship') }}" class="has-text-weight-semibold is-size-7 is-underline">Family/Relationship</a>
		<a href="{{ route('forum.section', 'thread-religion') }}" class="has-text-weight-semibold is-size-7 is-underline">Religion</a>
		<a href="{{ route('forum.section', 'thread-entertainment') }}" class="has-text-weight-semibold is-size-7 is-underline">Entertainment</a>
		<a href="{{ route('forum.section', 'thread-africa') }}" class="has-text-weight-semibold is-size-7 is-underline">Africa</a>
		<a href="{{ route('forum.section', 'thread-international') }}" class="has-text-weight-semibold is-size-7 is-underline">International</a>
	</p>
</div>

<div class="has-text-centered m-t-10 m-b-10">
	<p><strong>Have a Topic to Discuss?</strong></p>
	<a href="{{ route('threads.create') }}" class="button is-link is-size-6-mobile">
		Create New Thread Now
	</a>
	<p class="is-size-6 has-text-grey m-t-10">
		You earn XP points or Ucoins when you Create New Thread !
	</p>
</div>