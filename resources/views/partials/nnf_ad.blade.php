@foreach($ads as $ad)
	@if($ad->name == 'nnf')
		<div class="m-10">
		    <p>
		    	<a href="{{ $ad->link }}">
		    		<p class="image">
		    			<img src="{{ asset('storage/' . $ad->image_path) }}" alt="">
		    		</p>
		    	</a>
		    </p>
		</div>
	@endif
@endforeach