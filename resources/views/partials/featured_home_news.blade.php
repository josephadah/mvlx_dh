{{-- DISPLAY FEATURED NEWS --}}
<div class="box m-t-10">
    <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Featured Posts</h3>
    @forelse($featuredNews as $featuredNew)
        @if($featuredNew->id != $news->id)
            <a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen has-border-bottom" href="{{ $featuredNew->showPath() }}">
                @if($featuredNew->thumbnail)
                    <span class="m-5">
                        <figure class="image is-96x96">
                            <img class="img-responsive" src="{{ $featuredNew->thumbnail }}" alt="">
                        </figure>
                    </span>
                @endif
                <span class="p-t-10 p-b-10">
                    @if(!$featuredNew->thumbnail)
                        <strong> >> </strong>
                    @endif
                        {{ $featuredNew->title }} <br/>
                </span>
            </a>
        @endif
    @empty
        <p>No featured posts</p>
    @endforelse

    {{-- DISPLAY SPONSORED NEWS --}}
    @if(!$sponsoredNews->isEmpty())
        <div>
            <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Sponsored</h3>
            @foreach($sponsoredNews as $sponsoredNew)
                @if($sponsoredNew->id != $news->id)
                    <a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen" href="{{ $sponsoredNew->showPath() }}">
                        @if($sponsoredNew->thumbnail)
                            <span class="m-5">
                                <figure class="image is-96x96">
                                    <img class="img-responsive" src="{{ $sponsoredNew->thumbnail }}" alt="">
                                </figure>
                            </span>
                        @endif
                        <span class="p-t-10 p-b-10">
                            @if(!$sponsoredNew->thumbnail)
                                <strong> >> </strong>
                            @endif
                                {{ $sponsoredNew->title }} <br/>
                        </span>
                    </a>
                @endif
            @endforeach
        </div>
    @endif

</div>