@foreach($ads as $ad)
	@if($ad->name == 'bottom_center')
		<div class="m-5">
		    <p>
		    	<a href="{{ $ad->link }}">
		    		<p class="image">
		    			<img src="{{ asset('storage/' . $ad->image_path) }}" alt="">
		    		</p>
		    	</a>
		    </p>
		</div>
	@endif
@endforeach