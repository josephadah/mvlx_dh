{{-- DISPLAY FEATURED QUESTIONS --}}
<div class="box">
    <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Featured Questions</h3>
    @forelse($featuredQuestions as $featuredQuestion)
        @if($featuredQuestion->id != $question->id)
            <a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen" href="{{ $featuredQuestion->showPath() }}">
                <span class="m-5">
                    <figure class="image is-96x96">
                        <img class="img-responsive" src="{{ $featuredQuestion->thumbnail }}" alt="">
                    </figure>
                </span>
                <span class="m-5">
                        {{ $featuredQuestion->title }}
                </span>
            </a>
        @endif
    @empty
        <p>No featured questions</p>
    @endforelse

</div>