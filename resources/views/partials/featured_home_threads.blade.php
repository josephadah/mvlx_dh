{{-- DISPLAY FEATURED THREADS --}}
<div class="box">
    <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Featured Threads</h3>
    @forelse($featuredThreads as $featuredThread)
        @if($featuredThread->id != $thread->id)
            <a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen" href="{{ $featuredThread->showPath() }}">
                <span class="m-5">
                    <figure class="image is-96x96">
                        <img class="img-responsive" src="{{ $featuredThread->thumbnail }}" alt="">
                    </figure>
                </span>
                <span class="m-5">
                        {{ $featuredThread->title }}
                </span>
            </a>
        @endif
    @empty
        <p>No featured threads</p>
    @endforelse

    {{-- DISPLAY SPONSORED THREADS --}}
    @if(!$sponsoredThreads->isEmpty())
        <div>
            <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Sponsored</h3>
            @foreach($sponsoredThreads as $sponsoredThread)
                @if($sponsoredThread->id != $thread->id)
                    <a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen" href="{{ $sponsoredThread->showPath() }}">
                        <span class="m-5">
                            <figure class="image is-96x96">
                                <img class="img-responsive" src="{{ $sponsoredThread->thumbnail }}" alt="">
                            </figure>
                        </span>
                        <span class="m-5">
                                {{ $sponsoredThread->title }}
                        </span>
                    </a>
                @endif
            @endforeach
        </div>
    @endif
</div>