@if ($paginator->hasPages())
    <nav class="pagination is-centered is-size-7">
        @if ($paginator->onFirstPage())
            <a class="pagination-previous is-size-7" disabled><< Previous page</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="pagination-previous is-size-7"><< Previous page</a>
        @endif

        @if ($paginator->hasMorePages())
            <a class="pagination-next is-size-7" href="{{ $paginator->nextPageUrl() }}" rel="next">Next page >></a>
        @else
            <a class="pagination-next is-size-7" disabled>Next page >></a>
        @endif
    </nav>
@endif