@extends('layouts.app')

@section('title', 'NaijaNewsForum | ' . $profileOwner->username . ' profile')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria</h3>
        </div>
    </div>
@endsection

@section('content')
@php
    if(auth()->check()) {
        $isFollowing = auth()->user()->isFollowing($profileOwner->id);
        $authenticatedUser = auth()->user();
    } else {
        $isFollowing = false;
        $authenticatedUser = '';
    } 
@endphp
<profile :profile-owner="{{ json_encode($profileOwner) }}" 
            :isfollowing="{{ json_encode($isFollowing) }}"
            :auth-user="{{ json_encode($authenticatedUser) }}"
            :has-requested="{{ json_encode(auth()->check() ? auth()->user()->hasRequested($profileOwner->id) : false) }}"
            :has-permission="{{ json_encode($profileOwner->hasPermitted(auth()->id())) }}"
            inline-template>
    <div>
        
            @include('profiles.top_details')

        <div class="box">

            <div v-if="signedIn && isOwner" v-cloak>
                @include('profiles.statusForm')
            </div>
            
            <p class="is-size-5 has-text-centered is-underline">Recent Activities</p>
            
            @forelse($profileOwnerActivities as $activity)
                @if(view()->exists("activities.{$activity->type}"))
                    @include("activities.{$activity->type}")
                @endif
            @empty
                <p class="has-text-centered">{{ $profileOwner->username }} has no activities</p>
            @endforelse

            {{ $profileOwnerActivities->links('vendor.pagination.bulma-simple') }}

        </div>
    </div>
</profile>
    
@endsection
