@extends('layouts.app')

@section('title', 'NaijaNewsForum | Your Subscriptions')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria</h3>
        </div>
    </div>
@endsection

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-5 m-l-5 has-border-bottom-2 m-b-10 has-text-centered m-t-10">Latest Posts Subscriptions</h1>
			@forelse($newsSubscriptions as $content)
				@include('profiles.subscription_content')
			@empty
				<p class="has-text-centered m-10">No post subscription</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection