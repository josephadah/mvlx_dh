@extends('layouts.app')

@section('title', 'NaijaNewsForum | Your Followers')

@section('content')

	<div class="box">
		<p class="is-size-4 has-text-centered has-border-bottom-2 m-b-10">{{ $profileOwner->username }} Followers</p>
		<ul>
			@forelse($followers as $follower)
				<li><a class="is-underline" href="{{ route('profile', $follower->username) }}">{{ $follower->username }}</a></li>
			@empty
				<p class="has-text-centered">0 followers</p>
			@endforelse
		</ul>
	</div>

	@include('partials.bottom_center_ad')

@endsection