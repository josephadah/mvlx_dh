@extends('layouts.app')

@section('title', 'NaijaNewsForum | Contact Requests')

@section('content')

	<div class="box">
		<p class="is-size-4 has-text-centered has-border-bottom-2 m-b-10">Contact Requests</p>
		<p class="m-10">Hi {{ auth()->user()->username }}, the following users seek your permission to view your contact information. You can either approve or decline their request.</p>
		<p class="is-size-7 m-10 has-border-bottom"><span class="has-text-danger">NB: </span>You can't approve a user again after declining their request neither can they send you a request again.</p>			
				
				@forelse($requesters as $requester)
						<div class="level is-mobile">
							<div class="level-left">
								<span class="image is-64x64">
									<img src="{{ $requester->thumbnail }}">
								</span>
								<span class="m-l-10 m-r-10">{{ $requester->username }}</span>
							</div>
							<div class="level-right">
								<form method="POST" action="{{ route('approve-request', $requester->username) }}">
									@csrf
									<button type="submit" class="button is-small is-link">Approve</button>
								</form>
								<form class="m-l-10" method="POST" action="{{ route('decline-request', $requester->username) }}">
									@csrf
									<button type="submit" class="button is-small is-danger">Decline</button>
								</form>
							</div>
						</div>
				@empty
					<p class="has-text-centered has-border-top">You don't have any contact request now.</p>
				@endforelse

	</div>

	@include('partials.bottom_center_ad')

@endsection