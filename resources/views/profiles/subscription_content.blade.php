	{{-- Main thread title and body --}}
<div class="has-border m-b-10"> 
	@if($content->hasUpdateFor())
		<h1 class="is-size-5 has-text-justified p-5 m-l-10">
			<a href="{{ $content->showPath() }}">
				<strong>{{ $content->title }}</strong><i class="is-upated-note m-l-5">updated</i>
			</a>
		</h1>
	@else
		<h1 class="is-size-5 has-text-justified p-5 m-l-10">
			<a href="{{ $content->showPath() }}">{{ $content->title }}</a>
		</h1>
	@endif
</div>