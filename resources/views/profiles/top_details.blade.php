<div class="box m-0 p-10">
    <div class="columns has-text-centered">
        <div class="column is-3 has-text-centered">
            <avatar-upload :user="{{ $profileOwner }}"></avatar-upload>
        </div>
        <div class="column is-9">
            <div class="has-text-centered" v-if="!editprofile">
                <h2 class="is-size-4">{{ $profileOwner->username }} Profile</h2>

                <p><strong>I am: </strong> <span v-text="title"></span></p>

                <div class="has-text-left">
                    <p><span class="m-r-30"><strong>Location:</strong><span v-text="location"></span></span>
                        <span><strong>Sex:</strong><span v-text="sex"></span></span></p>
                    <p><span class="m-r-30"><strong>Experience:</strong> <strong>{{ $profileOwner->reputation }}xp
                        </strong></span>
                        <span><strong>Reward:</strong> <strong>{{ $rewardCount = $profileOwner->reward }} {{ str_plural('Ucoin', $rewardCount) }}</strong>
                        </span>
                    </p>
                    <p><span class="m-r-30">
                            <a href="{{ route('followers.show', $profileOwner->username) }}">
                                <strong>Followers:</strong>
                            </a> <strong>{{ $profileOwner->followers()->count() }}</strong>
                        </span> 
                        <span><a href="{{ route('followings.show', $profileOwner->username) }}"><strong>Following:</strong></a> <strong>{{ $profileOwner->follows()->count() }}
                            </strong>
                        </span>
                    </p>
                    @auth
                        @if($profileOwner->hasPermitted(auth()->id()) || ($profileOwner->id == auth()->id()) || auth()->user()->isAdmin)
                            <p><strong>Contact: </strong>{{ $profileOwner->email }}, <span v-text="phone"></span></p>
                        @endif
                    @endauth
                    <p><strong>Bio/About me: </strong><span v-text="bio"></span></p>
                </div>
            </div>

            @include('profiles.edit-form')

            <button @click="editprofile=true" v-if="isOwner && ! editprofile" v-cloak>
                <i class="fa fa-pencil-square-o"></i> Edit Profile
            </button>
        </div>
    </div>
    
    <div class="has-text-centered">
        <span v-if="signedIn && ! isOwner" v-cloak>
            <button class="button is-small is-link" :class="isLoadingClass" @click="follow" v-if="! following">
                Follow {{ $profileOwner->username }}
            </button>
            <button class="button is-small is-danger" :class="isLoadingClass" @click="unfollow" v-else>
                Unfollow {{ $profileOwner->username }}
            </button>

            <button class="button is-small is-link" :class="isRequestingClass" 
                @click="requestContact" v-if=" ! hasRequested && ! hasPermission">
                Request contact
            </button>
            <button class="button is-small is-outlined" v-if="hasRequested && ! hasPermission">
                Contact request sent <i class="fa fa-check m-l-5"></i>
            </button>
        </span>

        @auth
            <span class="has-border-bottom" v-if="isOwner" v-cloak>
                @if($requestsCount = auth()->user()->requestsCount)
                    <a href="{{ route('contact-requests') }}" class="is-darkgreen">
                        Contact Requests
                    </a>
                    <span class="notifications-count">{{ $requestsCount }}</span>
                @endif
            </span>
        @endauth

        <span v-if="isAdmin && ! isOwner" v-cloak>
            <button class="button is-small is-danger is-outlined" @click="ban" v-if="! banned">
                Ban User
            </button>
            <button class="button is-small is-danger is-outlined" @click="unban" v-else>
                Unban User
            </button>
        </span>
    </div>
    
</div>

<div class="has-border has-text-centered is-size-7">
    <a class="button is-small" href="/news?by={{ $profileOwner->username }}">
        My News ({{ $profileOwner->news()->count() }})
    </a>
    <a class="button is-small" href="{{ route('user.statuses', $profileOwner->username) }}">
        My Statuses
    </a>
    <a class="button is-small" href="{{ route('user.subscriptions', $profileOwner->username) }}" v-if="isOwner" v-cloak>
        My Subscriptions
    </a>
</div>