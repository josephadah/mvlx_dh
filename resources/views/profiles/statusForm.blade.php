<form class="has-text-left" method="POST" action="{{ route('statuses.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="field">
        <div class="control">
            <div id="create-textarea">
                <textarea
                    class="m-t-5 textarea {{ $errors->has('body') ? ' is-danger' : '' }}" 
                    id="status_body" name="body" rows="1" 
                    placeholder="Share Status with your followers" @focus="showStatusHidden=true" required>
                    {{ old('body') }}
                </textarea>
            </div>
        </div>
    </div>

    <div class="field">
        @if ($errors->has('body'))
            <p class="help is-danger">
                {{ $errors->first('body') }}
            </p>
        @endif
    </div>



    <div v-if="showStatusHidden" v-cloak>
        <p class="button is-small is-link is-outlined m-b-5" 
            @click="showStatusAddImage=true">Add Image
        </p>

        <div class="field is-size-7" v-if="showStatusAddImage" v-cloak>
            <p class="is-size-7">
                <span class="is-red">NB: </span>Only jpg, jpeg, png, gif, svg images with max-size:4MB are allowed.
            </p>
            <div class="control is-size-7 m-b-5">
                <label for="image1">Select Image 1.</label>
                <input type="file" name="image1" accept="image/*">
            </div>
            <div class="control is-size-7">
                <label for="image2">Select Image 2.</label>
                <input type="file" name="image2" accept="image/*">
            </div>

            @if ($errors->has('image1'))
                <p class="help is-danger">
                    {{ $errors->first('image1') }}
                </p>
            @endif

            @if ($errors->has('image2'))
                <p class="help is-danger">
                    {{ $errors->first('image2') }}
                </p>
            @endif
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link is-small">
                    Post
                </button>
            </div>
        </div>

    </div>
</form>