@extends('layouts.app')

@section('title', 'NaijaNewsForum | All Notifications')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria</h3>
        </div>
    </div>
@endsection

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-5 m-l-5 has-border-bottom-2 m-10 has-text-centered m-t-10">Recent Unread Notifications</h1>
			@forelse($allNotifications as $notification)
				<a href="{{ $notification->data['link'] }}" 
					@click="markAsRead({{ json_encode($notification->id) }})">
					<p class="has-border p-5 m-5">
						{{ str_limit($notification->data['message'], 70) }}
					</p>
				</a>
			@empty
				<p class="has-text-centered">No recent unread notification</p>
			@endforelse

				{{ $allNotifications->links('vendor.pagination.bulma-simple') }}

			@if(count($allNotifications))
				<p class="has-text-centered"><a href="{{ route('delete.all.notifications') }}" class="button is-small is-danger">Delete all Notifications</a></p>
			@endif
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection