<form class="has-text-left" v-if="editprofile" v-cloak>
    <div class="field">
        <label for="title" class="label">I am: </label>
        <div class="control">
            <input type="text" 
                class="input {{ $errors->has('title') ? ' is-danger' : '' }}" 
                id="title" name="title" 
                placeholder="Enter your title" maxlength="50" 
                value="{{ old('title') }}" v-model="form.title">
            @if ($errors->has('title'))
                <p class="help is-danger">
                    <strong>{{ $errors->first('title') }}</strong>
                </p>
            @endif
        </div>
    </div>

    <div class="field">
        <label for="location" class="label">Location</label>
        <div class="control">
            <input type="text" 
                class="input {{ $errors->has('location') ? ' is-danger' : '' }}" 
                id="location" name="location" 
                placeholder="Enter your location" maxlength="50" 
                value="{{ old('location') }}" v-model="form.location">
            @if ($errors->has('location'))
                <p class="help is-danger">
                    <strong>{{ $errors->first('location') }}</strong>
                </p>
            @endif
        </div>
    </div>

    <div class="field">
        <label for="sex" class="label">Sex: </label>
        <div class="control">
            <div class="select">
                <select name="sex" v-model="form.sex">
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="None">None</option>
                </select>
            </div>
        </div>
    </div>

    <div class="field">
        <label for="phone" class="label">Phone: </label>
        <p class="has-text-danger is-size-7">Only those you have permitted can see your contact details</p>
        <div class="control">
            <input type="tel" 
                class="input {{ $errors->has('phone') ? ' is-danger' : '' }}" 
                id="phone" name="phone" 
                placeholder="Enter your phone number" maxlength="30" 
                value="{{ old('phone') }}" v-model="form.phone">
            @if ($errors->has('phone'))
                <p class="help is-danger">
                    <strong>{{ $errors->first('phone') }}</strong>
                </p>
            @endif
        </div>
    </div>

    <div class="field">
        <label for="bio" class="label">Your Bio/About</label>
        <div class="control">
            <div>
                <textarea
                    class="textarea {{ $errors->has('bio') ? ' is-danger' : '' }}" 
                    id="bio" name="bio" rows="1"
                     maxlength="300" 
                    v-model="form.bio">
                    {{ old('bio') }}
                </textarea>
            </div>
                
            <div v-if="errors" v-for="error in errors">
                <div v-for="err in error">
                    <p class="help is-danger" v-text="err"></p>
                </div>
            </div>
        </div>
    </div>

    <button type="button" :class="submitingClasses" @click="update">
        Update
    </button>
    <button type="button" class="button is-small is-danger m-t-5" @click="editprofile=false">
        Cancel
    </button>
</form>