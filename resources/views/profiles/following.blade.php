@extends('layouts.app')

@section('title', 'NaijaNewsForum | Your Followings')

@section('content')

	<div class="box">
		<p class="is-size-4 has-text-centered has-border-bottom-2 m-b-10">{{ $profileOwner->username }} is Following</p>
		<ul>
			@forelse($followings as $following)
				<li><a class="is-underline" href="{{ route('profile', $following->username) }}">{{ $following->username }}</a></li>
			@empty
				<p class="has-text-centered">0 members</p>
			@endforelse
		</ul>
	</div>

	@include('partials.bottom_center_ad')

@endsection