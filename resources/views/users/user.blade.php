<div class="has-border m-b-20">
	<div class="media has-border-top-bottom p-5">
		<figure class="media-left">
			<p class="image is-64x64">
				<a href="{{ route('profile', $user->username) }}">
					<img src="{{ $user->thumbnail }}" alt="">
				</a>
			</p>
		</figure>
		<div class="media-content">
			<div class="level is-mobile is-marginless">
				<div class="level-left">
					<span class="is-size-5">
						<strong>
							<a href="{{ route('profile', $user->username) }}">
								{{ $user->username }}
							</a> <span>({{ $user->reputation }} xp)</span>
						</strong><br>
					</span>
				</div>
			</div>
			<p>
				<span class="is-size-7">
					<strong>
						{{ str_limit($user->title, 50) }}
					</strong>
				</span>
			</p>
		</div>
	</div> {{-- end of media --}}
</div> {{-- end of main user title and body --}}