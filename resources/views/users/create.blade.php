@extends('layouts.app')

@section('title', 'NaijaNewsForum | Search Users')

@section('content')

	<div class="box">
		<p class="is-size-4 has-text-centered is-underline">Search for User</p>

		<div class="has-text-centered">
			@include('layouts.search_form')
		</div>
		
	</div>

@endsection