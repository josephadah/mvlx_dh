@extends('layouts.app')

@section('title', 'NaijaNewsForum | Search Users Result')

@section('content')

	<div class="box">
		<p class="is-size-4 has-text-centered is-underline">Search Result</p>

		@forelse($users as $user)

			@include('users.user')

		@empty
			<p class="has-text-centered">No result found.</p>
		@endforelse

		{{ $users->links('vendor.pagination.bulma-simple') }}
	</div>

@endsection