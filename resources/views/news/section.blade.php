@extends('layouts.app')

@section('title', 'NaijaNewsForum | ' . $section->name)

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.news_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<p class="has-text-centered has-border-bottom"><strong><a href="{{ route('home') }}">Home</a> >> {{ $section->name }}</strong></p>
			<p class="has-text-centered has-border-bottom">{{ $section->description }}</p>
		</div>

		{{-- Load 3 or 5 featured first then the recent follows --}}
		<div class="box p-10">
			@foreach($newses as $news)
				@include('news.section_news')
			@endforeach

			<div class="m-10">
				{{ $newses->links('vendor.pagination.bulma-simple') }}
			</div>
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection