@extends('layouts.app')

@section('title', 'NaijaNewsForum | Create Post')

@section('styles')
	<link href="{{ asset('css/vendor/trumbowyg.min.css') }}" rel="stylesheet">
	{{-- <link href="{{ asset('vendor/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.css') }}" rel="stylesheet"> --}}
@endsection

@section('content')
{{-- <create-thread inline-template> --}}
	<div class="box">
		
		<form method="POST" action="{{ route('news.store') }}" enctype="multipart/form-data">
	        @csrf

	        <p class="is-size-4 has-text-centered is-underline">Create New Post</p>
	        <p class="is-size-7 has-text-centered m-10">
	        	Please ensure you have read the Terms and Condition for Posting on this platform to avoid been banned. <br>
				<a class="is-underline" href="{{ route('terms') }}">Terms and Conditions</a>
	        </p>
	        
	        <div class="field">
	            <label for="title" class="label">Title: </label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('title') ? ' is-danger' : '' }}" 
	                    id="title" name="title" maxlength="100"
	                    placeholder="Enter Title here..."
	                    value="{{ old('title') }}" required autofocus>

	                @if ($errors->has('title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="section" class="label">Category: </label>
	            <div class="control">
					<div class="select {{ $errors->has('section') ? ' is-danger' : '' }}">
						<select name="section" required>
								<option value="" selected>Select appropriate category</option>
							@foreach($sections as $section)
								<option value="{{ $section->id }}" {{ old('section') == $section->id ? 'selected' : '' }}>
									{{ $section->name }}
								</option>
							@endforeach
						</select>
					</div>
	                @if ($errors->has('section'))
	                    <p class="help is-danger">
	                        {{ $errors->first('section') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="region" class="label m-0">Add Region/Location: <small class="is-size-7">(Optional)</small></label>
	            <div class="control">
	            	<span class="is-size-7">Please select/add location</span>
	            	<input type="text" id="searchRegion"
	                    class="input {{ $errors->has('region') ? ' is-danger' : '' }}" 
	                    id="region" name="region" maxlength="20" 
	                    placeholder="Enter or select location"
	                    value="{{ old('region') }}">
					
	                @if ($errors->has('region'))
	                    <p class="help is-danger">
	                        {{ $errors->first('region') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="body" class="label">Body of Post:</label>
	            <div class="control">
	            	<span class="is-size-7">Please include sources/references and images as appropriate.</span>
					<div id="create-textarea">
		                <textarea
		                    class="textarea {{ $errors->has('body') ? ' is-danger' : '' }}" 
		                    id="body" name="body" rows="1" 
		                    placeholder="Enter Detail of the news here..." required>
		                    {{ old('body') }}
		                </textarea>
		            </div>
	                    
	                @if ($errors->has('body'))
	                    <p class="help is-danger">
	                        {{ $errors->first('body') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
				<p class="is-underline"><strong>Add Image</strong></p>
				<p class="is-size-7">
					<span class="is-red">NB: </span>Only jpg, jpeg, png, gif, svg images with max-size:4MB are allowed.
				</p>
				<div class="control">
					<label for="image1">Select Image 1.</label>
					<input type="file" name="image1" accept="image/*">
				</div>
				<div class="control">
					<label for="image2">Select Image 2.</label>
					<input type="file" name="image2" accept="image/*">
				</div>

				@if ($errors->has('image1'))
                    <p class="help is-danger">
                        {{ $errors->first('image1') }}
                    </p>
                @endif

                @if ($errors->has('image2'))
                    <p class="help is-danger">
                        {{ $errors->first('image2') }}
                    </p>
                @endif
			</div>

			<div class="field">
                <div class="control has-text-centered">
                    <label for="terms" class="is-size-7">
                        <input type="checkbox" class="checkbox" name="terms" required>
                            I accept that I will be solely responsible for this post and I shall be banned if this post violate the rules and regulations of this forum.
                    </label>
                </div>

                @if ($errors->has('terms'))
                    <p class="help is-danger">
                        {{ $errors->first('terms') }}
                    </p>
                @endif
            </div>

	        <div class="field">
	            <div class="control">
	                <button type="submit" class="button is-link">
	                    Create Post
	                </button>
	            </div>
	        </div>

	    </form>

	</div>
{{-- </create-thread> --}}
@endsection

@section('scripts')
	<script src="{{ asset('js/vendor/trumbowyg.min.js') }}"></script>
	
	{{-- Import Trumbowyg colors JS at the end of.. --}}
	{{-- <script src="{{ asset('vendor/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js') }}"></script> --}}

	<script>
		$('textarea').trumbowyg({
			autogrow: true,
			autogrowOnEnter: true,
			btns: [
				['strong', 'em', 'del'],
				['superscript', 'subscript'],
				// ['formatting'],
				// ['foreColor'],
				['link'],
				['insertImage'],
				['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
				// ['horizontalRule']
			]
		});
	</script>
@endsection