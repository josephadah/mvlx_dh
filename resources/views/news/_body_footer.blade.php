<div class="level is-marginless has-border-top-bottom p-5 is-size-7">
	<div class="level-left">
		<span class="p-l-20" v-text="likesCount">
			{{-- {{ $news->likes_count }} {{ str_plural('Like', $news->likes_count) }} --}}
		</span>
		<span class="p-l-20" v-text="dislikesCount"></span>
		<span class="p-l-20">
			<span v-text="commentsCount"></span>
		</span>
	</div>	

	@include('partials.share_buttons', ['title' => $news->title])
</div>

@if(Auth()->check())
	<div class="level is-marginless p-5 is-size-6 has-border-bottom">
		<div class="level-left">
			<like :model="{{ $news }}" @liked="likeCounter"
				:model-route="'/news/'"
				:like-type="'Like'"
				:dislike-type="'Dislike'">
			</like>
			<span v-if="isConfirmed" v-cloak>
				<a href="#newcomment-textarea" class="m-l-10 m-b-5 button is-small is-outlined is-link"><i class="fa fa-reply is-size-6 m-r-5"></i>Comment</a>
			</span>
		</div>
		<div class="level-right m-0">
			<subscribe-button 
				:data="{{ json_encode($news->isSubscribedTo) }}" 
				:modelpath="{{ json_encode($news->path()) }}"
				v-if="! authorize('owns', news)">
			</subscribe-button>

			<a href="{{ route('news.report', $news) }}" class="button is-small is-danger is-outlined m-l-5">Report</a>
		</div>
	</div>

	@can('update', $news)
		<div class="level is-marginless p-5 is-size-6">
			<div class="level-left m-0 p-b-5">
				<admin-actions routename="news/" 
					:model="{{ $news }}" 
					:featured="{{ json_encode($news->featured_news) }}">
				</admin-actions>
			</div>
			<div class="level-right m-0">
				<form method="POST" action="{{ route('news.destroy', $news) }}" class="m-l-10">
					@csrf
					{{ method_field('DELETE') }}
					<a class="button is-small is-link is-outlined" href="{{ route('news.edit', $news) }}">Edit</a>
					
					@include('partials.confirm_delete')
					
				</form>
			</div>
		</div>
	@endcan
@endif