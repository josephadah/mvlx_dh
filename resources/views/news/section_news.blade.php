	{{-- Main news title and body --}}
<div class="has-border m-b-20"> 
	<h2 class="is-size-5 p-5 m-l-10 is-darkgreen has-text-centered">
		<a class="is-darkgreen" href="{{ $news->showPath() }}">{{ $news->title }}</a>
	</h2>

	<div class="media has-border-top-bottom p-5">
		<figure class="media-left">
			<p class="image is-32x32">
				<a href="{{ route('profile', $news->creator) }}">
					<img src="{{ $news->creator->thumbnail }}" alt="">
				</a>
			</p>
		</figure>
		<div class="media-content">
			<div class="level is-mobile is-marginless">
				<div class="level-left">
					<span class="is-size-7">
						<strong>
							<a href="{{ route('profile', $news->creator) }}">
								{{ $news->creator->username }}
							</a> <span>({{ $news->creator->reputation }} xp)</span>
						</strong><br>
					</span>
				</div>
				<div class="level-right">
					<span class="is-size-7">
						<strong>{{ $news->created_at->diffForHumans() }}</strong>
					</span>
				</div>
			</div>
			<p>
				<span class="is-size-7">
					<strong>
						{{ str_limit($news->creator->title, 50) }}
					</strong>
				</span>
			</p>
		</div>
	</div> {{-- end of media --}}

	<div class="has-border-top">
		<span class="is-size-7">
			<span class="p-l-10">
				{{ $likesCount = $news->likes_count }} {{ str_plural('Like', $likesCount) }}
			</span>
			<span class="p-l-10">
				{{ $likesCount = $news->dislikes_count }} {{ str_plural('Dislike', $likesCount) }}
			</span>
			<span class="p-l-10">
				{{ $news->comments_count }} {{ str_plural('comment', $news->comments_count) }}
			</span>
		</span>
	</div>
</div> {{-- end of main news title and body --}}