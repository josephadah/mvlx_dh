@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Posts Home')

{{-- @section('sub_nav')
	@include('news.sub_nav')
@endsection --}}

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.news_top_section')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Featured Posts</h1>
			@forelse($featuredNews as $featuredNew)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $featuredNew->showPath() }}">
						{{ $featuredNew->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No featured posts</p>
			@endforelse
		</div>

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Trending Posts</h1>
			@forelse($trendingNews as $trendingNew)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $trendingNew->showPath() }}">
						{{ $trendingNew->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No trending posts</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Recent Posts</h1>
			@forelse($recentNews as $recentNew)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $recentNew->showPath() }}">
						{{ $recentNew->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No recent post</p>
			@endforelse
		</div>


	</div>
@endsection