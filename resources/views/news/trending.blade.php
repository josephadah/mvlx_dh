@extends('layouts.app')

@section('title', 'NaijaNewsForum | Trending Posts')

{{-- @section('sub_nav')
	@include('news.sub_nav')
@endsection --}}

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.news_top_section')

		<div class="box p-10">
			<h1 class="is-size-5 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Posts in Last 24 hours</h1>
			@forelse($trendingNewsToday as $news)
				@include('news.section_news')
			@empty
				<p>No Trending Posts in Last 24 hours</p>
			@endforelse
		</div>

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-5 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Posts in Last 7 days</h1>
			@forelse($trendingNewsThisWeek as $news)
				@include('news.section_news')
			@empty
				<p>No Trending Post in Last 7 days</p>
			@endforelse
		</div>

		<div class="box p-10">
			<h1 class="is-size-5 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Posts within a Month</h1>
			@forelse($trendingNewsThisMonth as $news)
				@include('news.section_news')
			@empty
				<p>No Trending Post within a month</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection