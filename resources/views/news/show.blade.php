@extends('layouts.app')

@section('meta_image')
	<meta property="og:url" content="{{ $news->showPath() }}">
    <meta property="og:image" content="{{ $news->image1 ? $news->image1 : $news->image2 }}">
    <meta property="og:description" content="{{ str_limit(strip_tags($news->body), 300) }}">
    <meta name="twitter:title" content="{{ $news->title }}">
    <meta name="twitter:description" content="{{ str_limit(strip_tags($news->body), 300) }}">
	<meta name="twitter:image" content="{{ $news->image1 ? $news->image1 : $news->image2 }}">
@endsection

@section('title', $news->title)

@section('styles')
	<link rel="stylesheet" href="../css/vendor/jquery.atwho.min.css">
@endsection

@section('content')
<news :news="{{ json_encode($news) }}"
 			inline-template>
	<div class="box is-marginless p-10">
		
		

		@include('news.body')

		{{-- comments start here --}}
		<comments :newspath="{{ json_encode($news->path()) }}" :news="{{ $news }}" 
				@added="comments++" @removed="comments--"></comments>

		@guest
			<p class="has-text-centered has-text-weight-semibold">
				<a href="{{ route('news.login', $news->id) }}">Sign In</a> to add your Comment
			</p>
		@endguest

		@include('partials.top_center_ad')

		@include('partials.featured_home_news')

		<div class="box">
			<h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Related Posts in <a href="{{ route('section', $news->section->slug) }}">{{ $news->section->name }}</a> Section</h3>
			@foreach($section_newses as $section_news)
				@if($section_news->id != $news->id)
					<a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen m-b-10" href="{{ $section_news->showPath() }}"><strong class="m-r-5"> > </strong>  {{ $section_news->title }}</a>
				@endif
			@endforeach
		</div>

		@include('partials.news_top_section')

		@include('partials.bottom_center_ad')

	</div>
</news>
@endsection