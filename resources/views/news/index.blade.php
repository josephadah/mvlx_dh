@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Posts')

{{-- @section('sub_nav')
	@include('news.sub_nav')
@endsection --}}

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.news_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="has-text-centered is-size-4 m-l-5 has-border-bottom-2 m-b-10">{{ $filter_type }} Posts</h1>
			@forelse($newses as $news)
				@include('news.section_news')
			@empty
				<p class="has-text-centered">No Result for {{ $filter_type }} Posts found</p>
			@endforelse

			{{ $newses->links('vendor.pagination.bulma-simple') }}
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection