@extends('layouts.app')

@section('title', 'NaijaNewsForum | Subscriptions')

{{-- @section('sub_nav')
	@include('news.sub_nav')
@endsection --}}

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.news_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Posts you Subscribed to</h1>
			@forelse($newses as $news)
				@include('news.subscription_news')
			@empty
				<p class="has-text-centered m-10">No post</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection