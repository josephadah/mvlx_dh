@extends('layouts.app')

@section('title', 'NaijaNewsForum | Forum')

{{-- @section('sub_nav')
	@include('threads.sub_nav')
@endsection --}}

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.forum_top_section')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Threads in Last 24 hours</h1>
			@forelse($trendingThreadsToday as $thread)
				@include('threads.section_thread')
			@empty
				<p>No Trending Threads in Last 24 hours</p>
			@endforelse
		</div>

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Threads in Last 7 days</h1>
			@forelse($trendingThreadsThisWeek as $thread)
				@include('threads.section_thread')
			@empty
				<p>No Trending Threads in Last 7 days</p>
			@endforelse
		</div>

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Threads within a Month</h1>
			@forelse($trendingThreadsThisMonth as $thread)
				@include('threads.section_thread')
			@empty
				<p>No Trending Thread within a month</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection