<reply :reply="{{ $reply }}" inline-template v-cloak>
	<div class="m-10 has-border">
		<div class="media has-border-top-bottom p-5">
			<figure class="media-left">
				<p class="image is-32x32">
					<a href="#">
						<img src="http://localhost:8000/images/placeholders/128x128.png" alt="avatar">
					</a>
				</p>
			</figure>
			<div class="media-content">
				<div class="level is-marginless">
					<div class="level-left">
						<span class="is-size-7">
							<strong>
								<a href="{{ route('profile', $reply->owner->username) }}">
									{{ $reply->owner->username }}
								</a> - 324xp
							</strong><br>
						</span>
					</div>

					@can('update', $reply)
						<div class="level-right" v-if="! editing">
							<button class="button is-small" @click="editing = true">Edit</button>
							<button class="button is-small is-danger" @click="destroy">Delete</button>
						</div>
					@endcan
					
				</div>
				<div class="level is-marginless">
					<div class="level-left">
						<span class="is-size-7">
							<strong>(A Web developer, an Entreprenuer and A Christian)</strong>
						</span>
					</div>
					<div class="level-right">
						<span class="is-size-7">
							<strong>{{ $reply->created_at->diffForHumans() }}</a></strong>
						</span>
					</div>
				</div>
			</div>
		</div> {{-- end of media --}}

		{{-- form to edit reply --}}
		<div class="p-10" v-if="editing">
			<textarea class="textarea" rows=1 v-model="body"></textarea>
			<button class="button is-small is-primary" @click="update">Update</button>
			<button class="button is-small is-link" @click="editing = false">Cancel</button>
		</div>
		{{-- show body of reply --}}
		<div class="p-10" v-else>
			<p class="is-size-6" v-text="body"></p>
		</div>

		<div class="level is-marginless has-border-top-bottom p-5 is-size-7">
			<div class="level-left">
				<span class="p-l-10">22 Likes</span>
				<span class="p-l-20">3 Dislikes</span>
			</div>
			@if(Auth()->check())
				<div class="level-right">
					<like :reply="{{ $reply }}"></like>
					<a href="#" class="m-l-20">Quote</a>
					<a href="#" class="m-l-20">Share</a>
					<a href="#" class="m-l-20">Report</a>
				</div>
			@endif
		</div>
	</div>
</reply>