@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Forum Home')

{{-- @section('sub_nav')
	@include('threads.sub_nav')
@endsection --}}

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.forum_top_section')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Featured Threads</h1>
			@forelse($featuredThreads as $featuredThread)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $featuredThread->showPath() }}">
						{{ $featuredThread->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No featured threads</p>
			@endforelse
		</div>

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Trending Threads</h1>
			@forelse($trendingThreads as $trendingThread)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $trendingThread->showPath() }}">
						{{ $trendingThread->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No trending threadings</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Recent Threads</h1>
			@forelse($recentThreads as $recentThread)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $recentThread->showPath() }}">
						{{ $recentThread->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No recent threads</p>
			@endforelse
		</div>


	</div>
@endsection