	{{-- Main thread title and body --}}
<div class="has-border m-b-20"> 
	<h2 class="is-size-5 p-5 m-l-10 is-darkgreen has-text-centered">
		<a class="is-darkgreen" href="{{ $thread->showPath() }}">{{ $thread->title }}</a>
	</h2>

	<div class="media has-border-top-bottom p-5">
		<figure class="media-left">
			<p class="image is-32x32">
				<a href="{{ route('profile', $thread->creator) }}">
					<img src="{{ $thread->creator->thumbnail }}" alt="">
				</a>
			</p>
		</figure>
		<div class="media-content">
			<div class="level is-mobile is-marginless">
				<div class="level-left">
					<span class="is-size-7">
						<strong>
							<a href="{{ route('profile', $thread->creator) }}">
								{{ $thread->creator->username }}
							</a> <span>({{ $thread->creator->reputation }} xp)</span>
						</strong><br>
					</span>
				</div>
				<div class="level-right">
					<span class="is-size-7">
						<strong>{{ $thread->created_at->diffForHumans() }}</strong>
					</span>
				</div>
			</div>
			<p>
				<span class="is-size-7">
					<strong>
						{{ str_limit($thread->creator->title, 50) }}
					</strong>
				</span>
			</p>
		</div>
	</div> {{-- end of media --}}

	<div class="has-border-top">
		<span class="is-size-7">
			<span class="p-l-10">
				{{ $likesCount = $thread->likes_count }} {{ str_plural('Like', $likesCount) }}
			</span>
			<span class="p-l-10">
				{{ $thread->replies_count }} {{ str_plural('reply', $thread->replies_count) }}
			</span>
		</span>
	</div>
</div> {{-- end of main thread title and body --}}