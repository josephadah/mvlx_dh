<div class="level is-marginless has-border-top-bottom p-5 is-size-7">
	<div class="level-left">
		<span class="p-l-20" v-text="likesCount">
			{{-- {{ $thread->likes_count }} {{ str_plural('Like', $thread->likes_count) }} --}}
		</span>
		<span class="p-l-20" v-text="dislikesCount"></span>
		<span class="p-l-20">
			<span v-text="repliesCount"></span>
		</span>
	</div>

	@include('partials.share_buttons', ['title' => $thread->title])	
</div>

@if(Auth()->check())
	<div class="level is-marginless p-5 is-size-6 has-border-bottom">
		<div class="level-left">
			<like :model="{{ $thread }}" @liked="likeCounter"
				:model-route="'/threads/'"
				:like-type="'Like'"
				:dislike-type="'Dislike'">
			</like>
			<span v-if="isConfirmed" v-cloak>
				<a href="#newreply-textarea" class="m-l-10 m-b-5 button is-small is-outlined is-link"><i class="fa fa-reply is-size-6 m-r-5"></i>Reply</a>
			</span>
		</div>
		<div class="level-right m-0">
			<subscribe-button 
				:data="{{ json_encode($thread->isSubscribedTo) }}" 
				:modelpath="{{ json_encode($thread->path()) }}"
				v-if="! authorize('owns', thread)">
			</subscribe-button>

			<a href="{{ route('threads.report', $thread) }}" class="button is-small is-danger is-outlined m-l-5">Report</a>
		</div>
	</div>

	@can('update', $thread)
		<div class="level is-marginless p-5 is-size-6">
			<div class="level-left m-0 p-b-5">
				<admin-actions routename="threads/" 
					:model="{{ $thread }}" 
					:featured="{{ json_encode($thread->featured_thread) }}">
				</admin-actions>
			</div>
			<div class="level-right m-0">
				<form method="POST" action="{{ route('threads.destroy', $thread) }}" class="m-l-10">
					@csrf
					{{ method_field('DELETE') }}
					<a class="button is-small is-link is-outlined" href="{{ route('threads.edit', $thread) }}">Edit</a>

					@include('partials.confirm_delete')

				</form>
			</div>
		</div>
	@endcan
@endif