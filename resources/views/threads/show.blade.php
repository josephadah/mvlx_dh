@extends('layouts.app')

@section('meta_image')
    <meta property="og:url" content="{{ $thread->showPath() }}">
    <meta property="og:image" content="{{ $thread->image1 ? $thread->image1 : $thread->image2 }}">
    <meta property="og:description" content="{{ str_limit(strip_tags($thread->body), 300) }}">
    <meta name="twitter:title" content="{{ $thread->title }}">
    <meta name="twitter:description" content="{{ str_limit(strip_tags($thread->body), 300) }}">
	<meta name="twitter:image" content="{{ $thread->image1 ? $thread->image1 : $thread->image2 }}">
@endsection

@section('title', $thread->title)

@section('styles')
	<link rel="stylesheet" href="../css/vendor/jquery.atwho.min.css">
@endsection

@section('content')
<thread :thread="{{ json_encode($thread) }}"
 			inline-template>
	<div class="box is-marginless p-10">		

		@include('threads.body')

		{{-- Replies start here --}}
		<replies :threadpath="{{ json_encode($thread->path()) }}" :thread="{{ $thread }}" 
				@added="replies++" @removed="replies--"></replies>

		@guest
			<p class="has-text-centered has-text-weight-semibold">
				<a href="{{ route('threads.login', $thread->id) }}">Sign In</a> to add your reply
			</p>
		@endguest

		@include('partials.top_center_ad')

		@include('partials.featured_home_threads')

		<div class="box">
			<h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Related Threads <strong>In: <a href="{{ '/forum/' . $thread->section->slug }}">{{ $thread->section->name }}</a></strong> Section</h3>
			@foreach($section_threads as $section_thread)
				@if($section_thread->id != $thread->id)
					<a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen m-b-10" href="{{ $section_thread->showPath() }}"><strong class="m-r-5"> > </strong>  {{ $section_thread->title }}</a>
				@endif
			@endforeach
		</div>

		@include('partials.forum_top_section')
	
		@include('partials.bottom_center_ad')

	</div>
</thread>
@endsection