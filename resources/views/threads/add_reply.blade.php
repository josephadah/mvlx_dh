<div class="m-10">
	<form method="POST" action="{{ route('replies.store', $thread->id) }}">
		@csrf

        <div class="field">
            <div class="control">
                <textarea
                    class="textarea {{ $errors->has('body') ? ' is-danger' : '' }}" 
                    id="body" name="body" rows="1" 
                    placeholder="Add a reply" required>
                    {{ old('body') }}
                </textarea>
                    
                @if ($errors->has('body'))
                    <p class="help is-danger">
                        <strong>{{ $errors->first('body') }}</strong>
                    </p>
                @endif
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link is-small">
                    Reply
                </button>
            </div>
        </div>

	</form>
</div>