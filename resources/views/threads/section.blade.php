@extends('layouts.app')

@section('title', 'NaijaNewsForum | Forum >> ' . $section->name)

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.forum_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<p class="has-text-centered has-border-bottom"><strong>Forum >> {{ $section->name }}</strong></p>
			<p class="has-text-centered has-border-bottom">{{ $section->description }}</p>
		</div>

		{{-- Load 3 or 5 featured first then the recent follows --}}
		<div class="box p-10">
			@foreach($threads as $thread)
				@include('threads.section_thread')
			@endforeach

			<div class="m-10">
				{{ $threads->links('vendor.pagination.bulma-simple') }}
			</div>
			
		</div>

		@include('partials.bottom_center_ad')


	</div>
@endsection