	{{-- SHOWING MAIN THREAD--}}
<div class="has-border"> 
	{{-- main thread heading --}}
	@include('threads._body_heading')

	<div class="p-10">
		<p class="has-text-centered" v-if="image1">
			<figure class="image">
				<img :src="image1" alt="">
			</figure>
		</p>

		<p class="is-size-6 m-b-15 m-t-15" v-html="body"></p>
		
		<p class="has-text-centered" v-if="image2">
			<figure class="image">
				<img :src="image2" alt="">
			</figure>
		</p>
	</div>
	{{-- main thread footer --}}
	@include('threads._body_footer')

</div> {{-- end of main thread title and body --}}




	{{-- EDITING MAIN THREAD --}}
{{-- <div class="has-border" v-if="editing" v-cloak> 	
	<div class="p-10">
		<div class="field">
			<label for="title" class="label">Title: </label>
			<div class="control">
				<input type="text" name="title" class="input" v-model="form.title">
			</div>
		</div>

		<div class="field">
			<label for="body" class="label">Body: </label>
			<div class="control">

				<div id="updatethread-textarea">
					<trumbowyg v-model="form.body" 
						:config="config" class="form-control" 
						required></trumbowyg>
				</div>
			</div>
		</div>

		<div class="field">
			<p class="is-underline"><strong>change Image/s</strong></p>
			<p class="is-size-7">
				<span class="is-red">NB: </span>Only jpg, jpeg, png, gif, svg images with max-size:2MB are allowed.
			</p>
			<div class="control">
				<label for="image1">Change Image 1.</label>
				<input type="file" name="image1" accept="image/*">
			</div>
			<div class="control">
				<label for="image2">Change Image 2.</label>
				<input type="file" name="image2" accept="image/*">
			</div>
		</div>

		<div v-if="errors" v-for="error in errors">
            <div v-for="err in error">
                <p class="help is-danger" v-text="err"></p>
            </div>
        </div>

	</div>

	<div class="level is-marginless has-border-top-bottom p-5 is-size-7">
	@can('update', $thread)
		<div class="level-right">
			<button class="button is-primary is-small m-l-10" @click="update">Update</button>
			<button class="button is-danger is-small m-l-10" @click="cancel">Cancel</button>
		</div>
	@endcan
	
</div>

</div> --}} {{-- end of editing --}}



