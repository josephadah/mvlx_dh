<div class="mobile-nav-container">
	<div class="submenu is-size-7-mobile">
         <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/threads/trending">Trending</a>
         <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/threads">Recent</a>
         @if(Auth()->check())
            <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="{{ route('user.threadsubscriptions', auth()->user()) }}">
             	Subscribed
         	</a>
            <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/threads?by={{ auth()->user()->username }}">My Threads</a>
         @endif
   </div>
</div>
