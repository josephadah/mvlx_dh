@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Forum')

{{-- @section('sub_nav')
	@include('threads.sub_nav')
@endsection --}}

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.forum_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">{{ $filter_type }} Threads</h1>
			@forelse($threads as $thread)
				@include('threads.section_thread')
			@empty
				<p>No threads</p>
			@endforelse

			{{ $threads->links('vendor.pagination.bulma-simple') }}
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection