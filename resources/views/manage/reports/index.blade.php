@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<h3 class="title is-size-4 has-text-centered">All Recent Reports</h3>			
		<hr>
		@forelse($reports as $report)
			<div class="has-border p-5">
				<p><strong>Reported Content Link:</strong> 
					<a href="{{ url($report->link) }}">Content Link</a>
				</p>
				<p><strong>Other: </strong> {{ $report->description }}</p>
				<p class="is-underline">Reports</p>
				<ul>
					<li>{{ $report->report1 }}</li>
					<li>{{ $report->report2 }}</li>
					<li>{{ $report->report3 }}</li>
					<li>{{ $report->report4 }}</li>
					<li>{{ $report->report5 }}</li>
					<li>{{ $report->report6 }}</li>
					<li>{{ $report->report7 }}</li>
					<li>{{ $report->report8 }}</li>
					<li>{{ $report->report9 }}</li>
					<li>{{ $report->report10 }}</li>
					<li>{{ $report->report11 }}</li>
					<li>{{ $report->report12 }}</li>
					<li>{{ $report->report13 }}</li>
					<li>{{ $report->report14 }}</li>
					<li>{{ $report->report15 }}</li>
					<li>{{ $report->report16 }}</li>
					<li>{{ $report->report17 }}</li>
					<li>{{ $report->report18 }}</li>
					<li>{{ $report->report19 }}</li>
					<li>{{ $report->report20 }}</li>
				</ul>
				<p><strong>Other: </strong> {{ $report->description }}</p>

				<p><strong>Status: </strong> Blocked = {{ $report->blocked ? 'YES' : 'NO' }}</p>
				<p>
					<form method="POST" action="{{ route('reports.blocked', $report) }}" class="m-l-10 m-b-5">
						@csrf
						<button type="submit" class="button is-danger is-small is-outlined">Blocked Content/Owner?</button>
					</form>

					<form method="POST" action="{{ route('reports.destroy', $report) }}" class="m-l-10">
						@csrf
						{{ method_field('DELETE') }}
						<button type="submit" class="button is-danger is-small is-outlined">Delete</button>
					</form>
				</p>
			</div>
		@empty
			<p class="has-text-centered">No report</p>		
		@endforelse
			{{ $reports->links('vendor.pagination.bulma') }}
	</div>
@endsection