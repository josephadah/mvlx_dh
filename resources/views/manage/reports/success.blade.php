@extends('layouts.app')

@section('content')

	<div class="columns">
		<div class="column">
			<div class="box has-text-centered">
				<p class="has-text-centered has-text-success is-size-5 m-10">Report sent successfully.</p>

				<p class="is-size-5 has-color-info p-10">Thank you for your report.</p>

		        <p><a href="/" class="button is-size-7-mobile is-link is-outlined">Click here to go to the home page</a></p>
			</div>
		</div>
	</div>

@endsection