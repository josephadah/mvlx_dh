@extends('layouts.app')

@section('content')
	<div class="box">
		<form method="POST" action="{{ route('reports.store') }}">
	        @csrf

	        <p class="is-size-5 has-text-centered is-underline has-text-link">Report Content</p>

			<div class="has-border p-10">
		        <p class="has-border-bottom">You want to report the content below:</p>
		        <p class="is-underline m-5 p-5">{{ $content->title ? $content->title : '' }}</p>
		        <div class="has-border p-5 m-5">{!! $content->body !!}</div>
		        <p class="p-5 m-5"><strong>By User:</strong> 
		        	{{ $content->creator ? $content->creator->username : $content->owner->username }}
		        </p>
		    </div>

	        <p class="is-underline m-10 p-5 has-text-weight-bold has-text-link">Select what you want to report for below:</p>

	        <input type="hidden" name="link" value="{{ $content->creator ? $content->showPath() : $content->path() }}">

	        <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report1" 
			            value="Insult">
			            Insult
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report2" 
			            value="Fraudulent Activity">
			            Fraudulent Activity
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report3" 
			            value="Inciting/Violent Post">
			            Inciting/Violent Post
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report4" 
			            value="Sexually implicit content">
			            Sexually implicit content
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report5" 
			            value="Sexual Harrassment">
			            Sexual Harrassment
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report6" 
			            value="Illegal Advertisement">
			            Illegal Advertisement
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report7" 
			            value="Spam Content">
			            Spam Content
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report8" 
			            value="False Information">
			            False Information
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report9" 
			            value="Privacy Infringement">
			            Privacy Infringement
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label class="checkbox">
			            <input class="checkbox" type="checkbox" name="report10" 
			            value="Racism">
			            Racism
			        </label>
		        </div>
		    </div>

		    <div class="field">
		        <div class="control">
		        	<label for="description">Other (describe)</label>
		        	<input type="text" name="description" class="input">
		        </div>
		    </div>

		    <p class="is-size-7 has-text-danger p-5">NB: You may be banned if you send a false report.</p>

	        <div class="field">
	            <div class="control">
	                <button type="submit" class="button is-small is-danger is-outlined">
	                    Send Report
	                </button>
	            </div>
	        </div>

	    </form>
	</div>
@endsection