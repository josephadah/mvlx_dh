@extends('layouts.app')

@section('title', ' You have been banned')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>A Rewarding Social Media Experience</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box has-text-centered">
        <form method="POST" action="{{ route('feedbacks.store') }}">
            @csrf
            
            <h2 class="is-size-5 is-underline m-10">Send Feedback</h2>
            <ol class="m-10 has-text-left">
                <span class="is-underline has-text-link">You can send feedback related to the following:</span>
                <li>Your User Experience on this platform.</li>
                <li>Report any bug (error or malfunction) on any page.</li>
                <li>Suggest features you want to be added</li>
                <li>Suggest features you think should be improved, modified or removed</li>
                <li>Suggest a better User Interface you want implemented.</li>
            </ol>
            <div class="field">
                <div class="control">
                    <textarea class="textarea" name="feedback" rows="3" placeholder="Enter your feedback here..."></textarea>
                </div>
            </div>
            <button type="submit" class="button is-outlined is-link">Send Feedback</button>
        </form>
    </div>
    
@endsection
