@extends('layouts.app')

@section('title', ' You have been banned')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>A Rewarding Social Media Experience</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box has-text-centered">
        <p class="has-text-success is-size-5 p-10">
            Your feedback has been sent successfully.<br>
        </p>
        <p class="is-size-4 has-color-info p-10">Thank you for your feedback.</p>
        <p><a href="/" class="button is-size-7-mobile is-link is-outlined">Click here to go to the home page</a></p>
    </div>
    
@endsection
