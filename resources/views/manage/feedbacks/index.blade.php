@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<h3 class="title is-size-4 has-text-centered">All Recent Feedbacks</h3>			
		<hr>
		@forelse($feedbacks as $feedback)
			<div class="has-border p-5">
				<p><strong>Feedback from: <a href="{{ route('profile', $feedback->sender->username) }}">{{ $feedback->sender->username }}</a></strong> 
				</p>
				<p><strong>feedback: </strong> {{ $feedback->feedback }}</p>
				
				<p>
					<form method="POST" action="{{ route('feedbacks.destroy', $feedback) }}" class="m-l-10">
						@csrf
						{{ method_field('DELETE') }}
						<button type="submit" class="button is-danger is-small is-outlined">Delete</button>
					</form>
				</p>
			</div>
		@empty
			<p class="has-text-centered">No Feedback</p>		
		@endforelse
			{{ $feedbacks->links('vendor.pagination.bulma') }}
	</div>
@endsection