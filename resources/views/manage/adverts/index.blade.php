@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<h3 class="title is-size-4 has-text-centered">All Adverts</h3>			
		<hr>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Link</th>
					<th>Image Path</th>
					<th>Created_at</th>
					<th>Updated_at</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($allAdverts as $advert)
					<tr>
						<td>{{ $advert->id }}</td>
						<td><a href="{{ route('adverts.show', $advert) }}">{{ $advert->name }}</a></td>
						<td>{{ str_limit($advert->description, 10) }}</td>
						<td>{{ str_limit($advert->link, 10) }}</td>
						<td>{{ str_limit($advert->image_path, 10) }}</td>
						<td>{{ $advert->created_at }}</td>
						<td>{{ $advert->updated_at }}</td>
						<td>
							<a href="{{ route('adverts.edit', $advert->id) }}" class="button is-small is-info">Edit</a>
						</td>
						<td>
							<form method="POST" action="{{ route('adverts.destroy', $advert) }}" 
								class="m-l-10">
								@csrf
								{{ method_field('DELETE') }}
								<button type="submit" class="button is-danger is-small">Delete</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<hr>
			@include('manage.adverts.add_advert_form')
	</div>
@endsection