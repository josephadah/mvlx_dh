@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<div class="columns is-centered">
			<div class="column is-8">
				<h1 class="title">Advert</h1>
				<p>Name: {{ $advert->name }}</p>
				<p>Description: {{ $advert->description }}</p>
				<p>Link: {{ $advert->link }}</p>
				<p>
					<figure class="image">
		    			<img src="{{ asset('storage/' . $advert->image_path) }}" alt="">
		    		</figure>
				</p>
				<hr>
				<p>
					<a href="{{ route('adverts.edit', $advert->id) }}" class="button is-small is-info m-r-20 m-b-10">Edit</a> 

					<form method="POST" action="{{ route('adverts.destroy', $advert) }}">
						@csrf
						{{ method_field('DELETE') }}
						<button type="submit" class="button is-danger is-small">Delete</button>
					</form>
				</p>
			</div>
		</div>
		
	</div>
@endsection