<div class="columns is-centered">
	<div class="column is-8">
		<form method="POST" action="{{ route('adverts.store') }}" enctype="multipart/form-data">
			@csrf
			<p class="has-text-centered is-size-5 is-underline">Add Advert</p>
			<div class="field">
				<div class="control">
					<label for="name">Name: </label>
					<input type="text" name="name" 
					class="input {{ $errors->has('name') ? ' is-danger' : '' }}"
					value="{{ old('name') }}">

					@if ($errors->has('name'))
	                    <p class="help is-danger">
	                        {{ $errors->first('name') }}
	                    </p>
	                @endif
				</div>
			</div>
			<div class="field">
				<div class="control">
					<label for="description">Description: </label>
					<input type="text" name="description" 
					class="input {{ $errors->has('description') ? ' is-danger' : '' }}"
					value="{{ old('description') }}">

					@if ($errors->has('description'))
	                    <p class="help is-danger">
	                        {{ $errors->first('description') }}
	                    </p>
	                @endif
				</div>
			</div>

			<div class="field">
	            <label for="link" class="label">Link</label>
	            <div class="control">
	                <textarea
	                    class="textarea {{ $errors->has('link') ? ' is-danger' : '' }}" 
	                    id="link" name="link" rows="1" required>
	                    {{ old('link') }}
	                </textarea>
	                    
	                @if ($errors->has('link'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link') }}
	                    </p>
	                @endif
	            </div>
	        </div>

			<div class="field">
	            <label for="image_path" class="label">Image path</label>
	            <div class="control">
	                <textarea
	                    class="textarea {{ $errors->has('image_path') ? ' is-danger' : '' }}" 
	                    id="image_path" name="image_path" rows="1">
	                    {{ old('image_path') }}
	                </textarea>
	                    
	                @if ($errors->has('image_path'))
	                    <p class="help is-danger">
	                        {{ $errors->first('image_path') }}
	                    </p>
	                @endif
	            </div>
	        </div>

			<div class="field">
	            <p class="is-underline"><strong>Upload Advert Image</strong></p>
	            <p class="is-size-7">
	                <span class="is-red">NB: </span>Only jpg, jpeg, png, gif, svg images with max-size:2MB are allowed. Please use appropriate sizes.
	            </p>
	            <div class="control is-size-7">
	                <label for="image1">Select Image.</label>
	                <input type="file" name="image1" accept="image/*">
	            </div>

	             @if ($errors->has('image1'))
                    <p class="help is-danger">
                        {{ $errors->first('image1') }}
                    </p>
                @endif
	        </div>

			<div class="field">
				<div class="control">
					<button class="button is-primary is-small">Add Advert</button>
				</div>
			</div>
		</form>
	</div>
</div>