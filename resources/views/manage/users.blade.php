@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<h3 class="title is-size-4 has-text-centered">All Users</h3>
		<div class="columns">
			<div class="column is-6">
				<form method="GET" action="{{ route('manage.users.search') }}">
					@csrf
					<input class="input" type="text" name="user" placeholder="Search for user">
					<button type="submit" class="button">Search</button>
				</form>

				@if($users->toArray() == null)
					<div class="is-danger">
						<p class="is-size-5 is-danger">No Result found!</p>
					</div>
				@endif
			</div>
		</div>
			
		<hr>
		<p><strong>Total Registered Users: </strong> {{ $totalUsers }}</p>
		<p><strong>Total Confirmed Registered Users: </strong> {{ $confirmedUsers }}</p>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Username</th>
					<th>Email Address</th>
					<th>Xp</th>
					<th>Ucoin</th>
					<th>Award Ucoin</th>
					<th>Reduce Ucoin</th>
					<th>Created_at</th>
					<th>Confirmed</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						<td><a href="{{ route('profile', $user->username) }}">{{ $user->username }}</a></td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->reputation }}</td>
						<td>{{ $user->reward }}</td>
						<td>
							<form method="POST" action="{{ route('award-coin', $user->username) }}">
								@csrf
								<button type="submit" class="button is-small is-outlined is-primary">Award Ucoin</button>
							</form>
						</td>
						<td>
							<form method="POST" action="{{ route('award-coin', $user->username) }}" 
								class="m-l-10">
								@csrf
								{{ method_field('DELETE') }}
								<button type="submit" class="button is-danger is-small is-outlined">Reduce Ucoin</button>
							</form>
						</td>
						<td>{{ $user->created_at }}</td>
						<td>{{ $user->confirmed ? 'YES' : 'NO' }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{{ $users->links('vendor.pagination.bulma') }}
	</div>
@endsection