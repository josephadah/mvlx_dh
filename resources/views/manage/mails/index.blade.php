@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<div class="box m-20">
			<a href="{{ route('mails.update.list') }}" class="button is-link is-small">Update Mailing List</a>
		</div>
		<p class="is-size-4 has-text-centered">Send Broadcast mails</p>
		<form method="POST" action="{{ route('mails.store') }}">
			@csrf
			
			{{-- FIRST LINK --}}
			<div class="field">
	            <label for="link1_title" class="label">Link 1 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link1_title') ? ' is-danger' : '' }}" 
	                    id="link1_title" name="link1_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link1_title') }}" required autofocus>

	                @if ($errors->has('link1_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link1_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link1" class="label">Link 1 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link1') ? ' is-danger' : '' }}" 
	                    id="link1" name="link1" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link1') }}" required autofocus>

	                @if ($errors->has('link1'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link1') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 2 --}}
	        <div class="field">
	            <label for="link2_title" class="label">Link 2 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link2_title') ? ' is-danger' : '' }}" 
	                    id="link2_title" name="link2_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link2_title') }}" required autofocus>

	                @if ($errors->has('link2_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link2_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link2" class="label">Link 2 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link2') ? ' is-danger' : '' }}" 
	                    id="link2" name="link2" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link2') }}" required autofocus>

	                @if ($errors->has('link2'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link2') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 3 --}}
	        <div class="field">
	            <label for="link3_title" class="label">Link 3 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link3_title') ? ' is-danger' : '' }}" 
	                    id="link3_title" name="link3_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link3_title') }}" required autofocus>

	                @if ($errors->has('link3_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link3_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link3" class="label">Link 3 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link3') ? ' is-danger' : '' }}" 
	                    id="link3" name="link3" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link3') }}" required autofocus>

	                @if ($errors->has('link3'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link3') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 4 --}}
	        <div class="field">
	            <label for="link4_title" class="label">Link 4 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link4_title') ? ' is-danger' : '' }}" 
	                    id="link4_title" name="link4_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link4_title') }}" required autofocus>

	                @if ($errors->has('link4_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link4_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link4" class="label">Link 4 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link4') ? ' is-danger' : '' }}" 
	                    id="link4" name="link4" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link4') }}" required autofocus>

	                @if ($errors->has('link4'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link4') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 5  --}}
	        <div class="field">
	            <label for="link5_title" class="label">Link 5 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link5_title') ? ' is-danger' : '' }}" 
	                    id="link5_title" name="link5_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link5_title') }}" required autofocus>

	                @if ($errors->has('link5_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link5_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link5" class="label">Link 5 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link5') ? ' is-danger' : '' }}" 
	                    id="link5" name="link5" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link5') }}" required autofocus>

	                @if ($errors->has('link5'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link5') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 6 --}}
	        <div class="field">
	            <label for="link6_title" class="label">Link 6 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link6_title') ? ' is-danger' : '' }}" 
	                    id="link6_title" name="link6_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link6_title') }}" required autofocus>

	                @if ($errors->has('link6_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link6_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link6" class="label">Link 6 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link6') ? ' is-danger' : '' }}" 
	                    id="link6" name="link6" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link6') }}" required autofocus>

	                @if ($errors->has('link6'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link6') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 7 --}}
	        <div class="field">
	            <label for="link7_title" class="label">Link 7 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link7_title') ? ' is-danger' : '' }}" 
	                    id="link7_title" name="link7_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link7_title') }}" required autofocus>

	                @if ($errors->has('link7_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link7_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link7" class="label">Link 7 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link7') ? ' is-danger' : '' }}" 
	                    id="link7" name="link7" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link7') }}" required autofocus>

	                @if ($errors->has('link7'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link7') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 8 --}}
	        <div class="field">
	            <label for="link8_title" class="label">Link 8 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link8_title') ? ' is-danger' : '' }}" 
	                    id="link8_title" name="link8_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link8_title') }}" required autofocus>

	                @if ($errors->has('link8_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link8_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link8" class="label">Link 8 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link8') ? ' is-danger' : '' }}" 
	                    id="link8" name="link8" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link8') }}" required autofocus>

	                @if ($errors->has('link8'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link8') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 9 --}}
	        <div class="field">
	            <label for="link9_title" class="label">Link 9 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link9_title') ? ' is-danger' : '' }}" 
	                    id="link9_title" name="link9_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link9_title') }}" required autofocus>

	                @if ($errors->has('link9_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link9_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link9" class="label">Link 9 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link9') ? ' is-danger' : '' }}" 
	                    id="link9" name="link9" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link9') }}" required autofocus>

	                @if ($errors->has('link9'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link9') }}
	                    </p>
	                @endif
	            </div>
	        </div>


	        {{-- LINK 10 --}}
	        <div class="field">
	            <label for="link10_title" class="label">Link 10 title</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link10_title') ? ' is-danger' : '' }}" 
	                    id="link10_title" name="link10_title" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link10_title') }}" required autofocus>

	                @if ($errors->has('link10_title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link10_title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="link10" class="label">Link 10 url</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('link10') ? ' is-danger' : '' }}" 
	                    id="link10" name="link10" maxlength="200"
	                    placeholder="Enter a good concise headline for the news"
	                    value="{{ old('link10') }}" required autofocus>

	                @if ($errors->has('link10'))
	                    <p class="help is-danger">
	                        {{ $errors->first('link10') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <div class="control">
	                <button type="submit" class="button is-link">
	                    Send Mail
	                </button>
	            </div>
	        </div>
		</form>
	</div>
@endsection