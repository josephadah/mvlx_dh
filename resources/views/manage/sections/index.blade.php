@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<h3 class="title is-size-4 has-text-centered">All Sections</h3>			
		<hr>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Model Type</th>
					<th>Name</th>
					<th>Slug</th>
					<th>Description</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				@foreach($sections as $section)
					<tr>
						<td>{{ $section->id }}</td>
						<td>{{ $section->model_type }}</td>
						<td>{{ $section->name }}</td>
						<td>{{ $section->slug }}</td>
						<td>{{ str_limit($section->description, 10) }}</td>
						<td>
							<a href="{{ route('sections.edit', $section) }}" class="button is-small is-info">Edit</a>
						</td>
						<td>
							<form method="POST" action="{{ route('sections.destroy', $section) }}" 
								class="m-l-10">
								@csrf
								{{ method_field('DELETE') }}
								<button type="submit" class="button is-danger is-small">Delete</button>
							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<hr>
			@include('manage.sections.add_section_form')
	</div>
@endsection