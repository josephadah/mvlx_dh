@extends('layouts.manage')

@section('content')

<div class="columns is-centered">
	<div class="column is-8">
		<form method="POST" action="{{ route('sections.update', $section) }}">
			@csrf
			{{ method_field('patch') }}

			<p class="has-text-centered is-size-5 is-underline">Edit Section</p>
			<div class="field">
				<div class="control">
					<label for="model_type">Model Type: </label>
					<input type="text" name="model_type" 
					class="input {{ $errors->has('model_type') ? ' is-danger' : '' }}"
					value="{{ old('model_type', $section->model_type) }}">

					@if ($errors->has('model_type'))
	                    <p class="help is-danger">
	                        {{ $errors->first('model_type') }}
	                    </p>
	                @endif
				</div>
			</div>

			<div class="field">
				<div class="control">
					<label for="name">Name: </label>
					<input type="text" name="name" 
					class="input {{ $errors->has('name') ? ' is-danger' : '' }}"
					value="{{ old('name', $section->name) }}">

					@if ($errors->has('name'))
	                    <p class="help is-danger">
	                        {{ $errors->first('name') }}
	                    </p>
	                @endif
				</div>
			</div>

			<div class="field">
				<div class="control">
					<label for="slug">Slug: </label>
					<input type="text" name="slug" 
					class="input {{ $errors->has('slug') ? ' is-danger' : '' }}"
					value="{{ old('slug', $section->slug) }}">

					<span class="is-size-7">
	                	<span class="has-text-danger">NB: </span>
	                	Slug must not contain any space
		            </span>

					@if ($errors->has('slug'))
	                    <p class="help is-danger">
	                        {{ $errors->first('slug') }}
	                    </p>
	                @endif
				</div>
			</div>

			<div class="field">
				<div class="control">
					<label for="description">Description: </label>
					<input type="text" name="description" 
					class="input {{ $errors->has('description') ? ' is-danger' : '' }}"
					value="{{ old('description', $section->description) }}">

					@if ($errors->has('description'))
	                    <p class="help is-danger">
	                        {{ $errors->first('description') }}
	                    </p>
	                @endif
				</div>
			</div>

			<div class="field">
				<div class="control">
					<button class="button is-primary is-small">Update Section</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection