@extends('layouts.manage')

@section('content')
	<div class="box m-t-10">
		<h3 class="title is-size-4 has-text-centered">All Recent Appeals</h3>			
		<hr>
		@forelse($appeals as $appeal)
			<div class="has-border p-5">
				<p><strong>Appeal from: <a href="{{ route('profile', $appeal->sender->username) }}">{{ $appeal->sender->username }}</a></strong> 
				</p>
				<p><strong>Appeal: </strong> {{ $appeal->appeal }}</p>
				
				<p>
					<form method="POST" action="{{ route('appeals.destroy', $appeal) }}" class="m-l-10">
						@csrf
						{{ method_field('DELETE') }}
						<button type="submit" class="button is-danger is-small is-outlined">Delete</button>
					</form>
				</p>
			</div>
		@empty
			<p class="has-text-centered">No Appeal</p>		
		@endforelse
			{{ $appeals->links('vendor.pagination.bulma') }}
	</div>
@endsection