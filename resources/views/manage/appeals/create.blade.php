@extends('layouts.app')

@section('title', ' You have been banned')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>A Rewarding Social Media Experience</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box has-text-centered">
        <form method="POST" action="{{ route('appeals.store') }}">
            @csrf
            
            <h2 class="is-size-5 is-underline m-10">Appeal you ban</h2>
            <div class="field">
                <div class="control">
                    <label for="appeal">Enter your appeal below. <span class="is-size-7 has-text-danger">(Not more than 300 characters).</span></label> <br>
                    <textarea class="textarea" name="appeal" rows="3" placeholder="Enter your appeal here..."></textarea>
                </div>
            </div>
            <button type="submit" class="button is-outlined is-link">Send Appeal</button>
        </form>
    </div>
    
@endsection
