@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Privacy page')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <p class="has-border-bottom is-size-4 has-text-centered has-text-link m-b-10">
           <strong>Privacy</strong>
        </p>

        <p class="has-text-left">
            We at NaijaNewsForum.com treat the privacy of our visitors with the highest importance. 
            We will not disclose users personal information or contact details with any third party agent, except when the user violate any government law and it's required for investigation purpose.<br><br>

            We provide areas on our site where you can post your personal information and communicate with others. In addition, such posts may appear on other websites or when searches are executed on the subject of your post. Also, whenever you voluntarily disclose personal information on public web pages, that information will be publicly available and can be collected and used by others. For example, if you post your email address, you may receive unsolicited messages. We cannot control who reads your posts or what other users may do with the information you voluntarily post, so we encourage you to exercise discretion and caution with respect to your personal information.<br><br>

            With regards to the email notifications and updates we send to you when necessary, you can unsubscribe from our email list by clicking on the unsubscribe link on the email. 
            <br><br>
            Last updated – 7th August, 2018.
        </p>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
