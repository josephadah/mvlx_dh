@extends('layouts.app')

@section('title', 'NaijaNewsForum | Successfully Changed Email')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="is-size-4 has-text-link m-10">Congratulation!</p>
                    <p>Your email has been changed to <strong>{{ $userNewEmail }}</strong> and a new confirmation email has been sent to you.</p>
                    <p class="m-t-20 m-b-20">Please check your inbox or spam in your email to confirm.</p>
                    <p>If you didn't recieve the email immediately, please check after few minutes.</p>

                    <p class="is-size-4 has-text-link m-10">Thank You</p>
                </div>
            </div>
        </div>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
