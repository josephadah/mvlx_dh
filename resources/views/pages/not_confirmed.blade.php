@extends('layouts.app')

@section('title', 'NaijaNewsForum | You are not confirmed')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="has-text-danger">Sorry, You can't perform this action because your email is not yet confirmed</p>
                    <p class="is-size-5 has-text-link m-20">Please check your inbox (or spam box) in your email to confirm.</p>
                    <p>A confirmation email was sent to {{ auth()->user()->email }}, if the email address is not correctly spelt, <br>
                        <a class="is-primary is-underline" href="{{ route('change-email') }}">Click here to change the Email Address</a>
                    </p>
                    <p class="m-t-20">If you didn't recieve any confirmation email from us for over 15 minutes after Registration and your email address was correctly spelt,  
                        <br>
                        <a class="is-underline is-link" href="{{ route('resend.email') }}">Click here to recieve new confirmation email.</a>
                    </p>
                </div>
            </div>
        </div>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
