@extends('layouts.app')

@section('title', 'NaijaNewsForum | Registration success')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="has-text-success is-size-4 m-10 p-5 has-border-bottom"><strong>Congratulation!!!</strong></p>
                    <p class="is-size-5 has-text-link m-20">Your Registration was successful.</p>
                    <p class="is-size-4 has-text-link">One Last Step Remaining</p>
                    <p class="is-size-5">
                        Please check your email to confirm your email address <br>
                        <span class="is-size-6">(Check the spam box if it's not in the inbox)</span><br>
                        <span class="is-size-6">
                            <span class="has-text-danger">NB:</span>
                            If you didn't receive the confirmation email immediately, please check after few minutes.
                        </span>
                    </p>
                    <p>
                        <a class="button is-size-7-mobile is-link m-20" href="{{ route('home') }}">Go to Home Page</a>
                    </p>
                </div>
            </div>
        </div>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
