@extends('layouts.app')

@section('title', 'NaijaNewsForum | Confirmation success')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="has-text-success is-size-4"><strong>Congratulation!!!</strong></p>
                    <p class="is-size-5 has-text-link m-20">Your email is now confirmed.</p>
                    <p>You now have full access to all features on this platform.</p>
                    <p>
                        <a class="button is-size-7-mobile is-link m-10" 
                        href="{{ route('profile', auth()->user()->username) }}">Update Your Profile
                        </a>
                    </p>
                    <p>
                        <a class="button is-size-7-mobile is-link m-10" href="{{ route('home') }}">Go to Home Page</a>
                    </p>
                </div>
            </div>
        </div>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
