@extends('layouts.app')

@section('title', 'NaijaNewsForum | Change Email Address')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p>Change your email address</p>
                    <form method="POST" action="{{ route('store-change-email', auth()->user()) }}">
                        @csrf
                        <div class="field">
                        <div class="control has-icons-left has-icons-right">
                            <input type="email" 
                                class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
                                id="email" name="email" 
                                placeholder="Enter your correct email address"
                                value="{{ old('email') }}" required autofocus>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-envelope"></i>
                                </span>
                            @if ($errors->has('email'))
                                <p class="help is-danger">
                                    {{ $errors->first('email') }}
                                </p>
                            @endif
                        </div>
                        <p class="is-size-7">
                            <span class="has-text-danger">NB:</span>Use an email address that you can login to confirm.
                        </p>
                    </div>

                        <div class="field">
                            <div class="control">
                                <button class="button is-small is-link">Change Email and Resend Confirmaton Link</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
