@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Terms and Conditions')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>A Rewarding Social Media Experience</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <p class="has-border-bottom is-size-4 has-text-centered has-text-link m-b-10">
            Terms and Conditions for using NaijaNewsForum
        </p>
        <p class="has-text-left">
            Listed below are the terms and conditions that binds everyone that uses this platform:
            <ol class="p-20">
                <li>You are solely responsible for everything you post here.</li>
                <li>If you break any government law/regulation on this platform, your contact detail may be given out for security investigation</li>
                <li>Piracy and use of intellectual property without appropriate permission or reference is not allowed.</li>
                <li>You must reference the source of any information that is not yours.</li>
                <li>Insult or use of violent, vulgar and hurting words on any member of the platform is not allowed.</li>
                <li>Posting of Sexually implicit content is not allowed on this platform.</li>
                <li>Promotion of illegal and fraudulent business is not allowed.</li>
                <li>Insult on any Tribe, Race or Religion is strongly prohibited.</li>
            </ol>

            <p class="has-text-danger m-b-10 has-text-centered">We have the right to banned anyone who violate any of the above terms and condition.</p>

            <p class="has-text-centered m-20">
                <span class="has-text-danger">Remember: </span>
                <span>We are all Humans irrespective of our colour, tribe or religion. Let's respect each other, for you never may know who you are interacting with.</span>
            </p>
        </p>
        
        <p class="has-text-centered m-20"><a class="is-underline is-link is-size-7-mobile m-20" href="{{ route('feedbacks.create') }}">You can send your suggestion/s for future feature upgrade here.</a></p>
    </div>
    
@endsection
