@extends('layouts.app')

@section('title', 'NaijaNewsForum | You have been banned')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box has-text-centered">
        <p class="has-text-danger is-size-5">
            Sorry You have been Banned from making any Post!
        </p>
        <p class="m-t-10"><a href="{{ route('appeals.create') }}" class="button is-size-6 is-size-7-mobile is-link is-outlined">Click here if you wish to send an appeal</a></p>
    </div>
    
@endsection
