@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | About Page')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <p class="has-border-bottom is-size-4 has-text-centered has-text-link m-b-10">
            About NaijaNews Forum
        </p>
        <p class="has-text-left">
            NaijaNews Forum is a social platform that provides user with a modern and rewarding social media features which the following main features;
            <ol class="p-20">
                <li>Creating of Forum Threads to start discussion on any topic and any registered user can participate in the discussion.</li>
                <li>Asking questions and answering of questions. Any registered user can answer a question and the owner of the question can mark the best answer that worked for him/her.</li>
                <li>Posting of news. A news is posted according to it's location and section. A user can therefore search for news base on their location.</li>
                <li>Posting of Status Update by a user. All the followers of that user will see the status update and can comment and like it.</li>
                <li>Subscribing to any post - Thread, Question and News. This will enable the user to recieve notifications related to that post.</li>
                <li>Ability to follow other users and be followed</li>
                <li>And lots of other modern interactive features like liking, disliking, sharing, etc.</li>
            </ol>
        </p>
        
        <p class="has-text-centered"><a class="is-underline is-link is-size-7-mobile is-outlined m-20" href="{{ route('feedbacks.create') }}">You can send your suggestion/s for future feature upgrade here.</a></p>
    </div>
    
@endsection
