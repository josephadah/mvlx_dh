@extends('layouts.app')

@section('title', 'NaijaNewsForum | Resend confirmation')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="has-text-link is-size-5">A new confirmation email has been sent to you.</p>
                    <p class="is-size-5 m-20">Please check your inbox (or spam box) in your email to confirm.</p>
                    <p>
                        <span class="is-size-6">
                            <span class="has-text-danger">NB:</span>
                            If you didn't receive the email immediately, please check after few minutes.
                        </span>
                    </p>
                </div>
            </div>
        </div>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
