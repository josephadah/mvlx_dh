@component('mail::message')
# Welcome to NaijaNewsForum.
# Confirm NaijaNewsForum Registration

You are Almost done. Please click on the button below to confirm your email address. 

@component('mail::button', ['url' => url('/register/confirm?token=' . $user->confirmation_token)])
Confirm Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
