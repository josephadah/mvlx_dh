@component('mail::message')
# New Posts on NaijaNews Forum

Hello dear, here are lists of recent posts on NaijaNews Forum that you may find interesting.

<ul>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link1'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link1_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link2'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link2_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link3'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link3_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link4'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link4_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link5'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link5_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link6'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link6_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link7'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link7_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link8'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link8_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link9'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link9_title'] }}</a>
</li>

<li style="font-size: 1rem; margin-bottom: 10px;">
<a href="{{ $all_requests['link10'] }}" style="padding: 5px; margin-bottom: 10px; text-decoration: underline;">{{ $all_requests['link10_title'] }}</a>
</li>

</ul>

@component('mail::button', ['url' => 'https://www.naijanewsforum.com'])
View more on NaijaNewsForum.com
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team.

<p style="font-size:0.8rem;">Don't want to receive this email any more? Click the link below to unsubscribe<br> %mailing_list_unsubscribe_url%</p>
@endcomponent
