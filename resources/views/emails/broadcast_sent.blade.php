@extends('layouts.manage')

@section('content')

	<div class="columns">
		<div class="column">
			<div class="box has-text-centered">
				<p class="has-text-centered has-text-success is-size-5 m-10">Broadcast emails sent</p>

		        <p><a href="/" class="button is-size-7-mobile is-link is-outlined">Click here to go to the home page</a></p>
			</div>
		</div>
	</div>

@endsection