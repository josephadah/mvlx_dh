@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Home')

@section('content')
    
@section('welcome')

    <div class="box has-text-centered is-marginless p-5" id="home-top">
        {{-- SHOW ONLY ON DESKTOP --}}
        <div class="is-hidden-mobile">
            @include('partials.home_top_section')
        </div> {{-- END OF SHOWING ON DESKTOP --}}

        {{-- SHOW ONLY ON MOBILE --}}
        <div class="is-hidden-tablet">
            @include('partials.home_mobile_top')
        </div> {{-- END OF SHOWING MOBILE SUMMARY --}}
    </div>
@endsection

@include('partials.nnf_ad')
{{-- DISPLAY FEATURED NEWS --}}
<div class="box m-t-10">
    <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Featured Posts</h3>
    @forelse($featuredNews as $news)
        <a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen has-border-bottom" href="{{ $news->showPath() }}">
            @if($news->thumbnail)
                <span class="m-5">
                    <figure class="image is-96x96">
                        <img class="img-responsive" src="{{ $news->thumbnail }}" alt="">
                    </figure>
                </span>
            @endif
            <span class="p-t-10 p-b-10">
                @if(!$news->thumbnail)
                    <strong> >> </strong>
                @endif
                    {{ $news->title }} <br/>
            </span>
        </a>
    @empty
        <p>No featured news</p>
    @endforelse
</div>

    @include('partials.top_center_ad')

    {{-- DISPLAY SPONSORED NEWS --}}
<div class="box m-t-20">
    @if(!$sponsoredNews->isEmpty())
        <div>
            <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Sponsored</h3>
            @foreach($sponsoredNews as $news)
                <a style="display: flex;" class="is-darkgreen has-text-weight-semibold is-underline is-darkgreen" href="{{ $news->showPath() }}">
                    @if($news->thumbnail)
                        <span class="m-5">
                            <figure class="image is-96x96">
                                <img class="img-responsive" src="{{ $news->thumbnail }}" alt="">
                            </figure>
                        </span>
                    @endif
                    <span class="p-t-10 p-b-10">
                        @if(!$news->thumbnail)
                            <strong> >> </strong>
                        @endif
                            {{ $news->title }} <br/>
                    </span>
                </a>
            @endforeach
        </div>
    @endif

</div>

{{-- RECENT POSTS --}}
<div class="box p-10 has-text-centered">
    <h3 class="has-border-bottom-2 m-b-10 has-text-weight-semibold">Recent Post</h3>
    @foreach($recentNews as $news)
        @include('news.section_news')
    @endforeach
</div>




{{-- DISPLAY HOME FEED --}}
@auth
    <div class="box p-10">
        <h3 class="is-size-6 has-text-centered has-border-bottom-2 m-10 has-text-weight-semibold" id="feed">
            Your Home Feed
        </h3>

        @forelse($feedActivities as $activity)
            @if(view()->exists("activities.{$activity->type}"))
                @include("activities.{$activity->type}")
            @endif
        @empty
            <p class="has-text-centered">No activities in Home Feed. <br>
                Follow other users to have their activities appear on your Home feed.
            </p>
        @endforelse

        {{ $feedActivities->links('vendor.pagination.bulma-simple') }}
    </div>
@endauth

@guest
    <div class="box p-10">
        <h3 class="is-size-5 has-text-centered has-border-bottom-2 m-10" id="feed">Your Home Feed</h3>
        <p class="has-text-centered has-text-weight-semibold">No activities in Home Feed. <br>
            <a href="/login">Sign In</a> and Follow other users to have their activities appear on your Home feed.
        </p>
    </div>
@endguest

@include('partials.bottom_center_ad')
    
@endsection
