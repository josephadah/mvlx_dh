@extends('layouts.app')

@section('title', ' Page not found.')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="m-20 is-size-4 has-text-danger has-border-bottom">Sorry Page Not Found!!!</p>
                    <p class="is-size-5">It seems like the page you are looking for does not exist.</p>
                    <p class="m-20"><a href="{{ route('home') }}" class="button is-link is-outlined">Return to Home Page</a></p>
                </div>
            </div>
        </div>

        <p class="is-underline has-text-link p-20 m-10 has-text-centered">(c)2018 NaijaNewsForum. All Rights Reserved</p>
    </div>
    
@endsection
