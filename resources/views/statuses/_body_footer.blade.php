<div class="level is-marginless has-border-top-bottom p-5 is-size-7">
	<div class="level-left">
		<span class="p-l-20" v-text="likesCount">
			{{-- {{ $status->likes_count }} {{ str_plural('Like', $status->likes_count) }} --}}
		</span>
		<span class="p-l-20" v-text="dislikesCount"></span>
		<span class="p-l-20">
			<span v-text="responsesCount"></span>
		</span>
	</div>	
</div>
@if(Auth()->check())
	<div class="level is-marginless p-5 is-size-6 has-border-bottom">
		<div class="level-left">
			<like :model="{{ $status }}" @liked="likeCounter"
				:model-route="'/statuses/'"
				:like-type="'Like'"
				:dislike-type="'Dislike'">
			</like>
		</div>
		<div class="level-right m-0">
			<span v-if="isConfirmed" v-cloak>
				<a href="#newresponse-textarea" class="m-l-10 m-r-5 button is-small is-outlined is-link"><i class="fa fa-reply is-size-6 m-r-5"></i>Comment</a>
			</span>

			@include('partials.share_buttons', ['title' => $status->creator->username . ' status update on NaijaNewsForum.com'])
			
			<a href="{{ route('statuses.report', $status) }}" class="button is-small is-danger is-outlined m-l-5">Report</a>
		</div>
	</div>

	@can('update', $status)
		<div class="level is-mobile is-marginless p-5 is-size-6">
			<div class="level-left m-0 p-b-5">
				<span v-if="isAdmin" v-cloak>
					<button class="button is-danger is-small is-outlined" @click="block" v-if="! blocked">Block</button>
					<button class="button is-danger is-small is-outlined" @click="unblock" v-else>Unblock</button>
				</span>
			</div>
			<div class="level-right m-0">
				<form method="POST" action="{{ route('statuses.destroy', $status) }}" class="m-l-10">
					@csrf
					{{ method_field('DELETE') }}
					<a class="button is-small is-link is-outlined" href="{{ route('statuses.edit', $status) }}">Edit</a>
					
					@include('partials.confirm_delete')
					
				</form>
			</div>
		</div>
	@endcan
@endif