@extends('layouts.app')

@section('title', 'NaijaNewsForum | ' . $profileOwner->username . 'Statuses')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria</h3>
        </div>
    </div>
@endsection

@section('content')
    <div class="box is-marginless p-10">
        
        @include('partials.top_center_ad')

        <p class="is-size-5 has-text-centered has-border-bottom-2 m-10">{{ $profileOwner->username }} Statuses</p>

        @forelse($statuses as $status)

            <div class="has-border m-b-20"> 
                <div class="media has-border-top-bottom p-5">
                    <figure class="media-left">
                        <p class="image is-32x32">
                            <a href="{{ route('profile', $status->creator) }}">
                                <img src="{{ $status->creator->thumbnail }}" alt="avatar">
                            </a>
                        </p>
                    </figure>
                    <div class="media-content">
                        <div class="level is-mobile is-marginless">
                            <div class="level-left">
                                <span class="is-size-7">
                                    <strong>
                                        <a href="{{ route('profile', $status->creator) }}">
                                            {{ $status->creator->username }}
                                        </a><span>-{{ $status->creator->reputation }}xp</span>
                                    </strong><br>
                                </span>
                            </div>
                            <div class="level-right">
                                <span class="is-size-7">
                                    <strong>{{ $status->created_at->diffForHumans() }}</strong>
                                </span>
                            </div>
                        </div>
                        <p>
                            <span class="is-size-7">
                                <strong>
                                    {{ str_limit($status->creator->title, 50) }}
                                </strong>
                            </span>
                        </p>
                    </div>
                </div> {{-- end of media --}}

                <div class="p-10 has-text-justified">
                    <p>
                        {!! str_limit($status->body, 100, '') !!}
                        @if(strlen($status->body) > 100)
                            <a href="{{ $status->showPath() }}" class="btn btn-primary is-size-7">
                                ...Continue Reading
                            </a>
                        @endif
                    </p>
                </div>

                <div class="has-border-top">
                    <span class="is-size-7">
                        <span class="p-l-10">
                            {{ $likesCount = $status->likes_count }} {{ str_plural('Like', $likesCount) }}
                        </span>
                        <span class="p-l-10">
                            {{ $status->reponses_count }} {{ str_plural('comment', $status->reponses_count) }}
                        </span>
                        <span class="p-l-20">
                            <a href="{{ route('statuses.show', $status) }}">..comment</a>
                        </span>
                    </span>
                </div>
            </div>

        @empty
            <p class="has-text-centered">{{ $profileOwner->username }} has no status</p>

        @endforelse

        @include('partials.bottom_center_ad')

    </div>
@endsection