<div class="media has-border-top-bottom p-5">
	<figure class="media-left">
		<p class="image is-32x32">
			<a href="{{ route('profile', $status->creator) }}">
				<img src="{{ $status->creator->thumbnail }}" class="profile-image-thumbnail-50x50" alt="">
			</a>
		</p>
	</figure>
	<div class="media-content">
		<div class="level is-marginless is-mobile">
			<div class="level-left">
				<span class="is-size-7">
					<strong>
						<a href="{{ route('profile', $status->creator) }}">
							{{ $status->creator->username }}
						</a> <span>({{ $status->creator->reputation }} xp)</span>
					</strong><br>
				</span>
			</div>
			<div class="level-right">
				<span class="is-size-7">
					<strong>{{ $status->created_at->diffForHumans() }}</strong>
				</span>
			</div>
		</div>
		<div class="level is-marginless is-mobile">
			<span class="is-size-7 m-r-5">
				<strong>
					{{ str_limit($status->creator->title, 50) }}
				</strong>
			</span>
		</div>
	</div>
</div> {{-- end of media --}}