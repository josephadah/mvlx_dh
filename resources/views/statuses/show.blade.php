@extends('layouts.app')

@section('title', 'NaijaNewsForum | Status')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria</h3>
        </div>
    </div>
@endsection

@section('content')
<status :status="{{ json_encode($status) }}"
            inline-template>
    <div class="box is-marginless p-10">
        
        @include('partials.top_center_ad')

        <p class="has-text-centered is-size-4">{{ $status->creator->username }} Status</p>

        @include('statuses.body')

        {{-- Replies start here --}}
        <responses :statuspath="{{ json_encode($status->showPath()) }}" :status="{{ $status }}" 
                @added="responses++" @removed="responses--"></responses>

        @guest
            <p class="has-text-centered"><a href="{{ route('login') }}">Sign In</a> to add your Comment</p>
        @endguest

        @include('partials.bottom_center_ad')

    </div>
</status>
@endsection