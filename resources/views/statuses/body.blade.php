{{-- SHOWING MAIN STATUS--}}
<div class="has-border"> 
	{{-- main status heading --}}
	@include('statuses._body_heading')

	<div class="p-10">
		<p class="is-size-6 m-b-20" v-html="body"></p>
		<p class="has-text-centered" v-if="image1">
			<figure class="image">
				<img :src="image1" alt="">
			</figure>
		</p>
		<p class="has-text-centered" v-if="image2">
			<figure class="image">
				<img :src="image2" alt="">
			</figure>
		</p>
	</div>
	{{-- main status footer --}}
	@include('statuses._body_footer')

</div> {{-- end of main status title and body --}}