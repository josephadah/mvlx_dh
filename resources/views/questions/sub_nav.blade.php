<div class="mobile-nav-container">
	<div class="submenu is-size-7-mobile">
         <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/questions/trending">Trending</a>
         <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/questions">Recent</a>
         @if(Auth()->check())
            <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="{{ route('user.questionsubscriptions', auth()->user()) }}">
             	Subscribed
         	</a>
            <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/questions?by={{ auth()->user()->username }}">My Questions</a>
         @endif
   </div>
</div>
