@extends('layouts.app')

@section('meta_image')
	<meta property="og:url" content="{{ $question->showPath() }}">
    <meta property="og:image" content="{{ $question->image1 ? $question->image1 : $question->image2 }}">
    <meta property="og:description" content="{{ str_limit(strip_tags($question->body), 300) }}">
    <meta name="twitter:title" content="{{ $question->title }}">
    <meta name="twitter:description" content="{{ str_limit(strip_tags($question->body), 300) }}">
	<meta name="twitter:image" content="{{ $question->image1 ? $question->image1 : $question->image2 }}">
@endsection

@section('title', $question->title)

@section('styles')
	<link rel="stylesheet" href="../css/vendor/jquery.atwho.min.css">
@endsection

@section('content')
<question :question="{{ json_encode($question) }}"
 			inline-template>
	<div class="box is-marginless p-10">
		
		@include('questions.body')

		{{-- Answers start here --}}
		<answers :questionpath="{{ json_encode($question->path()) }}" :question="{{ $question }}" 
				@added="answers++" @removed="answers--"></answers>

		@guest
			<p class="has-text-centered has-text-weight-semibold">
				<a href="{{ route('questions.login', $question->id) }}">Sign In</a> to add your Answer
			</p>
		@endguest

		@include('partials.top_center_ad')

		@include('partials.featured_home_questions')

		@include('partials.question_top_section')

		@include('partials.bottom_center_ad')

	</div>
</question>
@endsection