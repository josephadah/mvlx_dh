@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Questions')

@section('sub_nav')
	@include('questions.sub_nav')
@endsection

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.question_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">{{ $filter_type }} Questions</h1>
			@forelse($questions as $question)
				@include('questions.section_question')
			@empty
				<p>No Questions</p>
			@endforelse

			{{ $questions->links('vendor.pagination.bulma-simple') }}
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection