<h2 class="is-size-5 p-5 m-l-10 has-text-weight-bold has-text-centered" v-text="title"></h2>
<div class="media has-border-top-bottom p-5">
	<figure class="media-left">
		<p class="image is-32x32">
			<a href="{{ route('profile', $question->creator) }}">
				<img src="{{ $question->creator->thumbnail }}" class="profile-image-thumbnail-50x50" alt="">
			</a>
		</p>
	</figure>
	<div class="media-content">
		<div class="level is-marginless is-mobile">
			<div class="level-left">
				<span class="is-size-7">
					<strong>
						<a href="{{ route('profile', $question->creator) }}">
							{{ $question->creator->username }}
						</a> <span>({{ $question->creator->reputation }} xp)</span>
					</strong><br>
				</span>
			</div>
			<div class="level-right">
				<time datetime="{{ $question->created_at }}" class="is-size-7">
					<strong>{{ $question->created_at->diffForHumans() }}</strong>
				</time>
			</div>
		</div>
		<div class="level is-marginless is-mobile">
			<span class="is-size-7 m-r-5">
				<strong>
					{{ str_limit($question->creator->title, 50) }}
				</strong>
			</span>
			<div class="level-right">
				<span class="is-size-7">
					<strong>In: <a href="{{ route('questions.section', $question->section->slug) }}">{{ $question->section->name }}</a></strong>
				</span>
			</div>
		</div>
	</div>
</div> {{-- end of media --}}