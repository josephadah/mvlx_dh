@extends('layouts.app')

@section('title', 'NaijaNewsForum | Edit Question')

@section('styles')
	<link href="{{ asset('css/vendor/trumbowyg.min.css') }}" rel="stylesheet">
	{{-- <link href="{{ asset('vendor/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.css') }}" rel="stylesheet"> --}}
@endsection

@section('content')
{{-- <create-thread inline-template> --}}
	<div class="box">
		
		<form method="POST" action="{{ route('questions.update', $question) }}" enctype="multipart/form-data">
	        @csrf
	        {{ method_field('patch') }}

	        <p class="is-size-4 has-border-bottom-2 m-b-20 has-text-centered">Edit Question</p>
	        
	        <div class="field">
	            <label for="title" class="label">Question Title:</label>
	            <div class="control">
	                <input type="text" 
	                    class="input {{ $errors->has('title') ? ' is-danger' : '' }}" 
	                    id="title" name="title" maxlength="100"
	                    value="{{ old('title', $question->title) }}" required autofocus>

	                @if ($errors->has('title'))
	                    <p class="help is-danger">
	                        {{ $errors->first('title') }}
	                    </p>
	                @endif
	            </div>
	        </div>

	        <div class="field">
	            <label for="body" class="label">Body of Question:</label>
	            <div class="control">
					<div id="create-textarea">
		                <textarea
		                    class="textarea {{ $errors->has('body') ? ' is-danger' : '' }}" 
		                    id="body" name="body" rows="1" 
		                    placeholder="Enter Body of your Thread here" required>
		                    {{ old('body', $question->body) }}
		                </textarea>
		            </div>
	                    
	                @if ($errors->has('body'))
	                    <p class="help is-danger">
	                        {{ $errors->first('body') }}
	                    </p>
	                @endif
	            </div>
	        </div>

			<div class="field">
				<p class="is-underline"><strong>Add/Change Image/s</strong></p>
				<p class="is-size-7">
					<span class="is-red">NB: </span>Only jpg, jpeg, png, gif, svg images with max-size:4MB are allowed.
				</p>
				<div class="control">
					<label for="image1">Add/Change Image 1.</label>
					<input type="file" name="image1" accept="image/*">
				</div>
				<div class="control">
					<label for="image2">Add/Change Image 2.</label>
					<input type="file" name="image2" accept="image/*">
				</div>

				<div class="control" v-if="isAdmin" v-cloak>
					<label class="checkbox has-text-danger">
                        <input type="checkbox" name="delete_image1"> 
                        Delete Image 1
                    </label>
                    <label class="checkbox has-text-danger">
                        <input type="checkbox" name="delete_image2"> 
                        Delete Image 2
                    </label>
				</div>

				@if ($errors->has('image1'))
                    <p class="help is-danger">
                        {{ $errors->first('image1') }}
                    </p>
                @endif

                @if ($errors->has('image2'))
                    <p class="help is-danger">
                        {{ $errors->first('image2') }}
                    </p>
                @endif
			</div>

	        <div class="field">
	            <div class="control">
	                <button type="submit" class="button is-link">
	                    Update Question
	                </button>
	            </div>
	        </div>

	    </form>

	</div>
{{-- </create-thread> --}}
@endsection

@section('scripts')
	<script src="{{ asset('js/vendor/trumbowyg.min.js') }}"></script>
	
	{{-- Import Trumbowyg colors JS at the end of.. --}}
	{{-- <script src="{{ asset('vendor/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js') }}"></script> --}}

	<script>
		$('textarea').trumbowyg({
			autogrow: true,
			autogrowOnEnter: true,
			btns: [
				['strong', 'em', 'del'],
				['superscript', 'subscript'],
				// ['formatting'],
				// ['foreColor'],
				['link'],
				['insertImage'],
				['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
				// ['horizontalRule']
			]
		});
	</script>
@endsection