@extends('layouts.app')

@section('title', 'NaijaNewsForum | Questions')

@section('sub_nav')
	@include('questions.sub_nav')
@endsection

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.question_top_section')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Questions in Last 24 hours</h1>
			@forelse($trendingQuestionsToday as $question)
				@include('questions.section_question')
			@empty
				<p>No Trending Questions in Last 24 hours</p>
			@endforelse
		</div>

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Questions in Last 7 days</h1>
			@forelse($trendingQuestionsThisWeek as $question)
				@include('questions.section_question')
			@empty
				<p>No Trending Questions in Last 7 days</p>
			@endforelse
		</div>

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Trending Questions within a Month</h1>
			@forelse($trendingQuestionsThisMonth as $question)
				@include('questions.section_question')
			@empty
				<p>No Trending Questions within a month</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection