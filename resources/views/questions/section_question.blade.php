	{{-- Main question title and body --}}
<div class="has-border m-b-20"> 
	<h2 class="is-size-5 p-5 m-l-10 is-darkgreen has-text-centered">
		<a class="is-darkgreen" href="{{ $question->showPath() }}">{{ $question->title }}</a>
	</h2>

	<div class="media has-border-top-bottom p-5">
		<figure class="media-left">
			<p class="image is-32x32">
				<a href="{{ route('profile', $question->creator) }}">
					<img src="{{ $question->creator->thumbnail }}" alt="">
				</a>
			</p>
		</figure>
		<div class="media-content">
			<div class="level is-mobile is-marginless">
				<div class="level-left">
					<span class="is-size-7">
						<strong>
							<a href="{{ route('profile', $question->creator) }}">
								{{ $question->creator->username }}
							</a> <span>({{ $question->creator->reputation }} xp)</span>
						</strong><br>
					</span>
				</div>
				<div class="level-right">
					<span class="is-size-7">
						<strong>{{ $question->created_at->diffForHumans() }}</strong>
					</span>
				</div>
			</div>
			<p>
				<span class="is-size-7">
					<strong>
						{{ str_limit($question->creator->title, 50) }}
					</strong>
				</span>
			</p>
		</div>
	</div> {{-- end of media --}}

	<div class="has-border-top">
		<span class="is-size-7">
			<span class="p-l-10">
				{{ $likesCount = $question->likes_count }} {{ str_plural('Like', $likesCount) }}
			</span>
			<span class="p-l-10">
				{{ $question->answers_count }} {{ str_plural('answer', $question->answers_count) }}
			</span>
		</span>
	</div>
</div> {{-- end of main question title and body --}}