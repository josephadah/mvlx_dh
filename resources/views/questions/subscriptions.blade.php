@extends('layouts.app')

@section('title', 'NaijaNewsForum | Subscribed Questions')

@section('sub_nav')
	@include('questions.sub_nav')
@endsection

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.question_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10 has-text-centered">Questions you subscribed to</h1>
			@forelse($questions as $question)
				@include('questions.subscription_question')
			@empty
				<p class="has-text-centered m-10">No questions</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

	</div>
@endsection