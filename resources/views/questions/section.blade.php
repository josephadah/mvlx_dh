@extends('layouts.app')

@section('title', 'NaijaNewsForum | Question >> ' . $section->name)

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.question_top_section')

		@include('partials.top_center_ad')

		<div class="box p-10">
			<p class="has-text-centered has-border-bottom"><strong>Question >> {{ $section->name }}</strong></p>
			<p class="has-text-centered has-border-bottom">{{ $section->description }}</p>
		</div>

		{{-- Load 3 or 5 featured first then the recent follows --}}
		<div class="box p-10">
			@foreach($questions as $question)
				@include('questions.section_question')
			@endforeach

			<div class="m-10">
				{{ $questions->links('vendor.pagination.bulma-simple') }}
			</div>

			@include('partials.bottom_center_ad')
			
		</div>


	</div>
@endsection