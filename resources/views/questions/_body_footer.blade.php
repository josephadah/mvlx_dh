<div class="level is-marginless has-border-top-bottom p-5 is-size-7">
	<div class="level-left">
		<span class="p-l-20" v-text="likesCount">
			{{-- {{ $question->likes_count }} {{ str_plural('Like', $question->likes_count) }} --}}
		</span>
		<span class="p-l-20" v-text="dislikesCount"></span>
		<span class="p-l-20">
			<span v-text="answersCount"></span>
		</span>
	</div>	

	@include('partials.share_buttons', ['title' => $question->title])
</div>

@if(Auth()->check())
	<div class="level is-marginless p-5 is-size-6 has-border-bottom">
		<div class="level-left">
			<like :model="{{ $question }}" @liked="likeCounter"
				:model-route="'/questions/'"
				:like-type="'Like'"
				:dislike-type="'Dislike'">
			</like>
			<span v-if="isConfirmed" v-cloak>
				<a href="#newanswer-textarea" class="m-l-10 m-b-5 button is-small is-outlined is-link"><i class="fa fa-reply is-size-6 m-r-5"></i>Answer</a>
			</span>
		</div>
		<div class="level-right m-0">
			<subscribe-button 
				:data="{{ json_encode($question->isSubscribedTo) }}" 
				:modelpath="{{ json_encode($question->path()) }}"
				v-if="! authorize('owns', question)">
			</subscribe-button>
			<a href="{{ route('questions.report', $question) }}" class="button is-small is-danger is-outlined m-l-5">Report</a>
		</div>
	</div>

	@can('update', $question)
	<div class="level is-marginless p-5 is-size-6">
		<div class="level-left m-0 p-b-5">
			<admin-actions routename="questions/" 
				:model="{{ $question }}" 
				:featured="{{ json_encode($question->featured_question) }}">
			</admin-actions>
		</div>
		<div class="level-right m-0">
			<form method="POST" action="{{ route('questions.destroy', $question) }}" class="m-l-10">
				@csrf
				{{ method_field('DELETE') }}
				<a class="button is-small is-link is-outlined" href="{{ route('questions.edit', $question) }}">Edit</a>
				
				@include('partials.confirm_delete')
				
			</form>
		</div>
	</div>
	@endcan
@endif