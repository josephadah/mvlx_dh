	{{-- SHOWING MAIN QUESTION--}}
<div class="has-border"> 
	{{-- main question heading --}}
	@include('questions._body_heading')

	<div class="p-10">
		<p class="has-text-centered" v-if="image1">
			<figure class="image">
				<img :src="image1" alt="">
			</figure>
		</p>

		<p class="is-size-6 m-b-15 m-t-15" v-html="body"></p>
		
		<p class="has-text-centered" v-if="image2">
			<figure class="image">
				<img :src="image2" alt="">
			</figure>
		</p>
	</div>
	{{-- main question footer --}}
	@include('questions._body_footer')

</div> {{-- end of main question title and body --}}