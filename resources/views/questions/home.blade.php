@extends('layouts.app')

@section('meta_image')
    <meta property="og:image" content="{{ asset('storage/images/meta_logo.png') }}">
@endsection

@section('title', 'NaijaNewsForum | Question Home')

@section('sub_nav')
	@include('questions.sub_nav')
@endsection

@section('content')
	<div class="box is-marginless p-10">
		
		@include('partials.question_top_section')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Featured Questions</h1>
			@forelse($featuredQuestions as $featuredQuestion)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $featuredQuestion->showPath() }}">
						{{ $featuredQuestion->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No featured questions</p>
			@endforelse
		</div>

		@include('partials.top_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Trending Questions</h1>
			@forelse($trendingQuestions as $trendingQuestion)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $trendingQuestion->showPath() }}">
						{{ $trendingQuestion->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No trending questions</p>
			@endforelse
		</div>

		@include('partials.bottom_center_ad')

		<div class="box p-10">
			<h1 class="is-size-4 m-l-5 has-border-bottom-2 m-b-10">Recent Questions</h1>
			@forelse($recentQuestions as $recentQuestion)
				<h1 class="m-b-5 m-l-5"> <strong> > </strong>
					<a class="has-text-justified is-size-6 is-underline is-darkgreen has-text-weight-semibold" 
						href="{{ $recentQuestion->showPath() }}">
						{{ $recentQuestion->title }}
					</a>
				</h1>
			@empty
				<p class="has-text-centered">No recent questions</p>
			@endforelse
		</div>


	</div>
@endsection