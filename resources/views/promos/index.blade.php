@extends('layouts.app')

@section('title', 'NaijaNewsForum | Promo')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>A Rewarding Social Media Experience</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <h1 class="has-text-centered is-darkgreen title has-border-bottom-2">Welcome to NaijaNewsForum.com</h1>
        <p class="has-text-centered is-size-4"><strong class="has-text-danger">YES, IT'S TRUE.</strong></p>
        <p class="has-text-centered is-size-5 m-b-20">
            You can win <strong class="is-size-4">N1,000</strong> airtime every month <br>
            just by posting on NaijaNewsForum.com
        </p>
        <p class="is-size-4 has-border-bottom-2 has-text-centered"><strong class="has-text-danger">How???  ....Very Simple;</strong></p>
        <p><ul class="m-10">
                <li class="m-b-5 is-size-5"><strong>></strong> <a href="/login">Login</a> to NaijaNewsForum.com or <a href="/register">Create a New Account.</a></li>
                <li class="m-b-5 is-size-5"><strong>></strong> Post News or Ask Question or Create a Forum Topic.</li>
                <li class="m-b-5 is-size-5"><strong>></strong> Share your Post with all your friends on social media and ask them to comment on your post.</li>
                <li class="m-b-5 is-size-5"><strong>></strong> If your post falls among the top 3 most Trending posts of the month, You automatically win N1,000 airtime.</li>
                <li class="m-b-5 is-size-5"><strong>></strong> And Lastly, ....... That's All. Very Simple!</li>
            </ul>
        </p>
        <p class="is-size-5 m-10 p-t-20">Don't Forget It's <span class="has-text-danger">EVERY MONTH</span>, so Keep posting and sharing.</p>
        <p class="is-size-5 m-10 p-t-20"><span class="has-text-danger">Tip:</span> Create Relevant and Interesting Post that people will like to comment on.</p>

        <p class="has-border-bottom-2 is-size-5 has-border-bottom-2 has-text-link m-t-20">Terms and Conditions:</p>
        <p><ol class="m-10">
            <li>The most trending Post is with respect to number of comments by unique users excluding the owner of the post.</li>
            <li>Most trending Post must have a minimum of 10 comments by unique users.</li>
            <li>The post must not violate the <a href="{{ route('terms') }}">Terms and Conditions</a> of NaijaNewsForum.</li>
            <li>The owner of the post must not be banned or under suspension from this platform as at the period under review.</li>
        </ol></p>
        <p class="has-text-centered is-size-4 m-10"><strong class="has-text-danger has-border-bottom-2">Give it a Try. Good Luck!</strong></p>
        

        <div class="m-10 has-text-centered">
            <p class="is-size-5"><strong>What do you want to do now?</strong></p>
            <a href="/register" class="button is-link is-outlined m-10">Register</a><br>
            <a href="/login" class="button is-link is-outlined m-10">Login</a><br>
            <a href="{{ route('news.create') }}" class="button is-link is-outlined m-10">Post News</a><br>
            <a href="{{ route('questions.create') }}" class="button is-link is-outlined m-10">Ask Question</a><br>
            <a href="{{ route('threads.create') }}" class="button is-link is-outlined m-10">Create Forum Topic</a><br>
        </div>

        <p class="has-text-centered is-size-5 m-20">For any further information, please <a class="is-underline" href="{{ route('contact-us') }}">Contact Us</a></p>
    </div>
    
@endsection
