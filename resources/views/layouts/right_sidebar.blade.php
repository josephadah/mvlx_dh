@foreach($ads as $ad)
	@if($ad->name == 'top_right')
		<div class="m-b-30">
		    <p>
		    	<a href="{{ $ad->link }}">
		    		<p class="image">
		    			<img src="{{ asset('storage/' . $ad->image_path) }}" alt="">
		    		</p>
		    	</a>
		    </p>
		</div>
	@endif
@endforeach

@foreach($ads as $ad)
	@if($ad->name == 'bottom_right')
		<div>
		    <p>
		    	<a href="{{ $ad->link }}">
		    		<p class="image">
		    			<img src="{{ asset('storage/' . $ad->image_path) }}" alt="">
		    		</p>
		    	</a>
		    </p>
		</div>
	@endif
@endforeach
