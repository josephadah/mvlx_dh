<!-- Navigation bar for mobile -->
<div class="has-text-centered is-nav has-border-bottom-2">
    <a class="p-0 m-0" href="{{ url('/') }}">
         <img class="m-l-10 m-t-5" src="{{ asset('logo.png') }}" alt="" style="max-width: 200px;">
     </a>
</div>

<div class="is-nav is-white has-border-bottom-2">
    <div class="mobile-nav-container is-size-6">
        <div class="d-flex">
            <a class="mobile-menu-item has-text-weight-semibold m-l-10" href="{{ route('home') }}">Home</a>
        </div>
        <div class="d-flex">
                @guest
                    <a class="mobile-menu-item has-text-weight-semibold" href="{{ route('login') }}">Login</a>
                    <a class="mobile-menu-item has-text-weight-semibold m-r-10" href="{{ route('register') }}">Register</a>
                @else
                    <span class="m-r-5 m-l-5 m-t-5">
                        <user-notification></user-notification>
                    </span>

                    <div class="dropdown is-clickable is-right">
                        <div class="dropdown-trigger">
                            <img class="profile-image-thumbnail-32x32 has-border-circle" 
                                    src="{{ auth()->user()->thumbnail }}" 
                                    alt="">

                            <span class="is-size-3 m-l-5 m-r-20">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="dropdown-menu" id="dropdown-menu" role="menu">
                            <div class="dropdown-content">
                                <a href="{{ route('profile', auth()->user()->username) }}" 
                                    class="dropdown-item">
                                    My Profile
                                </a>
                                <a href="{{ route('manage.index') }}" 
                                    class="dropdown-item" v-if="isAdmin" v-cloak>
                                    Manage
                                </a>
                                <hr class="dropdown-divider">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                @endguest
        </div>
    </div>
</div> 
<!-- End of Navigation bar for mobile -->

<div class="is-size-7 has-text-centered">
     <a class="is-darkgreen is-underline has-text-weight-semibold white-nowrap m-r-5" href="/news/trending">Trending Posts</a>
     <a class="is-darkgreen is-underline has-text-weight-semibold white-nowrap m-r-5" href="/news">Latest Posts</a>
     @auth
         <a class="is-darkgreen is-underline has-text-weight-semibold white-nowrap m-r-5" href="/news?by={{ auth()->user()->username }}">My Posts</a>
         <a class="is-darkgreen is-underline has-text-weight-semibold white-nowrap m-r-5" href="/{{ auth()->user()->username }}/subscriptions">My Subscriptions</a>
     @endauth
</div>
