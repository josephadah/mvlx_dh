<div class="box has-text-centered">
    <p>
    	<a class="button is-link is-small is-outlined has-text-weight-semibold" href="/#home-top">Home Top</a>
    </p>
</div>

<p>Follow on: 
	<a href="https://fb.me/naijanewsforum" target="_blank" class="button has-border-circle is-primary"><i class="fa fa-facebook"></i></a>
	<a href="https://twitter.com/@naijanewsforum" target="_blank" class="button has-border-circle is-info"><i class="fa fa-twitter"></i></a>
</p>
<p class="p-10 has-text-centered">
	<span class="tag has-text-weight-semibold">(c)2018 NaijaNewsForum. All Rights Reserved</span>
	<a class="tag has-text-weight-semibold" href="{{ route('contact-us') }}">Contact Us</a>
	<a class="tag has-text-weight-semibold" href="{{ route('feedbacks.create') }}">Give us a Feedback</a>
	<a class="tag has-text-weight-semibold" href="{{ route('about-us') }}">About</a>
	<a class="tag has-text-weight-semibold" href="{{ route('terms') }}">Terms</a>
	<a class="tag has-text-weight-semibold" href="{{ route('privacy') }}">Privacy</a>
</p>