<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    @include('layouts.head')

<body>
    <div id="app">

        @include('layouts.desktop_nav')

        <!-- main content -->
        <main class="container">
            <div class="columns">
                <div class="column is-3">
                    @include('layouts.manage_side_nav')
                </div>
                <div class="column is-9">
                    @yield('content')
                </div>
            </div>
        </main>

        <flash message="{{ session('flash') }}"></flash>

    </div> <!-- end of app -->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    @yield('scripts')
    
</body>
</html>
