<!-- Navigation bar for desktop -->
<div class="is-white-background m-t-5 has-border-bottom-2">
    <div class="nav-container">
        <div class="menu-left m-l-30 is-size-5">
             <a class="p-0 m-0" href="{{ url('/') }}">
                 <img class="p-0 m-0" src="{{ asset('logo.png') }}" alt="" style="max-width: 250px;">
             </a>
        </div>
        <div class="menu-left m-r-50">
                @guest
                    <a class="menu-item has-text-weight-semibold" href="{{ route('login') }}">Login</a>
                    <a class="menu-item has-text-weight-semibold" href="{{ route('register') }}">Register</a>
                @else
                    <span class="m-r-5">
                        <user-notification></user-notification>
                    </span>

                    <div class="dropdown is-hoverable is-right">
                        <div class="dropdown-trigger">
                            <a href="#" aria-haspopup="true" aria-controls="dropdown-menu">
                                <img class="profile-image-thumbnail-32x32 has-border-circle" 
                                src="{{ auth()->user()->thumbnail }}" 
                                alt="">
                                <span class="is-size-4">
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </span>
                            </a>
                        </div>
                        <div class="dropdown-menu" id="dropdown-menu" role="menu">
                            <div class="dropdown-content">
                                <a href="{{ route('profile', auth()->user()->username) }}" 
                                    class="dropdown-item">
                                    My Profile
                                </a>
                                <a href="{{ route('manage.index') }}" 
                                    class="dropdown-item" v-if="isAdmin" v-cloak>
                                    Manage
                                </a>
                                <hr class="dropdown-divider">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" 
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                @endguest
        </div>
    </div>
</div> 

<div class="mobile-nav-container">
    <div class="submenu is-size-7">
         <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/news/trending">Trending Posts</a>
         <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/news">Latest Posts</a>
         @auth
             <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/news?by={{ auth()->user()->username }}">My Posts</a>
             <a class="submenu-item is-darkgreen has-border has-text-weight-semibold" href="/{{ auth()->user()->username }}/subscriptions">My Subscriptions</a>
         @endauth
   </div>
</div>
<!-- End of Navigation bar for desktop -->