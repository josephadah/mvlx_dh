<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    @include('layouts.head')

<body>
    <div id="app">
        
        <span class="is-hidden-tablet">
            @include('layouts.mobile_nav')
        </span>
        <span class="is-hidden-mobile">
            @include('layouts.desktop_nav')
        </span>

        <!-- main content -->
        <main class="container">
            @yield('slogan')

            @yield('sub_nav')

            <div class="columns is-marginless">

                <aside class="column is-3 is-hidden-mobile" style="background-color: #fff;">
                        @include('layouts.left_sidebar')
                </aside>

                <section class="column is-6">
                    @yield('welcome')
                    @yield('content')
                </section>
                
                <aside class="column is-3 is-hidden-mobile" style="background-color: #fff;">
                    @include('layouts.right_sidebar')
                </aside>

            </div>

            <div class="box m-20 p-20 has-text-centered">
                @include('layouts.footer')
            </div>
        </main>

        <flash message="{{ session('flash') }}"></flash>

    </div> <!-- end of app -->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/share.js') }}"></script>

    @yield('scripts')
    
</body>
</html>
