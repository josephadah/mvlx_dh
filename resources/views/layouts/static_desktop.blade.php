<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    @include('layouts.head')

<body>
    <div id="app">

        <span class="is-hidden-tablet">
            @include('layouts.mobile_nav')
        </span>
        <span class="is-hidden-mobile">
            @include('layouts.desktop_nav')
        </span>

        <!-- main content -->
        <main class="container">
            @yield('content')
        </main>

    </div> <!-- end of app -->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
