<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128176284-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-128176284-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="#1 News Discussion Forum in Nigeria.">
    <meta name="Keywords" content="NaijaNewsForum, forum, news, discussion, online, nigeria, africa, gossips, entertainment, politics, sports, jokes, business, technology, health, education, jobs, vacancies">
    <meta name="robots" content="index, follow" />
    <meta name="copyright" content="NaijaNewsForum.ng" />
    <meta name="author" content="NaijaNewsForum.com" />
    <meta name="language" content="English" />
    {{-- og meta tags --}}
    @yield('meta_image')
    <meta property="og:type" content="website" />
    <meta property="og:image:type" content="image/*">
    <meta property="og:image:width" content="500">
    <meta property="og:image:height" content="500">
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:creator" content="@youlyn_tweet" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vendor/jquery.atwho.min.css') }}" rel="stylesheet">
    @yield('styles')

	{{-- global script --}}
    <script>
    	window.App = {!! json_encode([
    			'signedIn' => Auth::check(),
                'isConfirmed' => Auth::check() ? Auth::user()->confirmed : '',
                'banned' => Auth::check() ? Auth::user()->banned : '',
                'userId' => auth()->check() ? auth()->id() : '', 
                'isAdmin' => auth()->check() ? auth()->user()->isAdmin : '',
                'isSuperAdmin' => auth()->check() ? auth()->user()->isSuperAdmin : ''
    		]) !!};
    </script>
</head>