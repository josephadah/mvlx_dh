<div class="box is-marginless p-10">
	<figure class="image is-96x96 has-margin-auto">
		@auth
			<a href="{{ route('profile', auth()->user()) }}">
				<img class="has-border-circle profile-image-thumbnail-100x100" 
				src="{{ auth()->user()->avatar }}" alt="">
			</a>
		@else
			<img class="has-border-circle profile-image-thumbnail-100x100" 
			src="{{ asset('storage/images/avatars/default.png') }}" alt="">
		@endauth
	</figure>
	<div>
		<p class="has-text-centered">
			@auth
				<strong>Welcome {{ auth()->user()->username }}.</strong>
			@else 
				<strong>Welcome Guest.</strong>
			@endauth
		</p>
	</div>

	<div class="columns">
		<div class="column is-6 has-text-centered">
			<p>Experience <br>
				@auth
					<strong>{{ auth()->user()->reputation }}xp</strong>
				@else
					<strong>0xp</strong>
				@endauth
			</p>
			<p>
				@auth
					<a href="{{ route('followers.show', auth()->user()->username) }}">Followers</a> <br>
					<strong>{{ auth()->user()->followers()->count() }}</strong>
				@else
					Followers <br>
					<strong>0</strong>
				@endauth
			</p>
		</div>
		<div class="column is-6 has-text-centered">
			<p>Reward <br>
				@auth
					<strong>{{ $rewardCount = auth()->user()->reward }} {{ str_plural('Ucoin', $rewardCount) }}</strong>
				@else
					<strong>0 Ucoin</strong>
				@endauth
			</p>
			<p>
				@auth
					<a href="{{ route('followings.show', auth()->user()->username) }}">Following </a><br>
					<strong>{{ auth()->user()->follows()->count() }}</strong>
				@else
					Following <br>
					<strong>0</strong>
				@endauth
			</p>
		</div>
	</div>

	@auth
		@if($requestsCount = auth()->user()->requestsCount)
			<div class="columns p-0 m-0">
				<div class="column p-0 m-0">
					<p class="has-border-bottom p-0 m-0 has-text-centered">
			            <a href="{{ route('contact-requests') }}" class="is-darkgreen">
			                Contact Requests
			            </a>
			            <span class="notifications-count">{{ $requestsCount }}</span>
			        </p>
				</div>
			</div>
		@endif
	@endauth

	<div class="box is-marginless p-10 p-l-20">
		<p class="is-size-5 has-border-bottom m-5">Quick Links</p>
		<p><a href="/news/trending" class="has-text-weight-semibold">Trending Posts</a></p>
		<p><a href="/news" class="has-text-weight-semibold">Latest Posts</a></p>
		@auth
			<p><a href="/news?by={{ auth()->user()->username }}" class="has-text-weight-semibold">My Posts</a></p>
			<p><a href="/{{ auth()->user()->username }}/subscriptions" class="has-text-weight-semibold">My Subscriptions</a></p>
			<p><a href="/{{ auth()->user()->username }}/statuses" class="has-text-weight-semibold">My Statuses</a></p>
		@endauth

		<div class="m-10">
			@include('layouts.search_form') 
		</div>
	</div>

	<div class="p-10 has-text-centered">
		<p>Follow on: 
			<a href="https://fb.me/naijanewsforum" target="_blank" class="button has-border-circle is-primary"><i class="fa fa-facebook"></i></a>
			<a href="https://twitter.com/@naijanewsforum" target="_blank" class="button has-border-circle is-info"><i class="fa fa-twitter"></i></a>
		</p>
		<p class="p-10 has-text-centered">
			<span class="tag">
				(c)2018 NaijaNewsForum.<br>
				All Rights Reserved
			</span>
			<a class="tag" href="{{ route('contact-us') }}">Contact Us</a>
			<a class="tag" href="{{ route('feedbacks.create') }}">Give us a Feedback</a>
			<a class="tag" href="{{ route('about-us') }}">About</a>
			<a class="tag" href="{{ route('terms') }}">Terms</a>
			<a class="tag" href="{{ route('privacy') }}">Privacy</a>
		</p>
	</div>


</div>