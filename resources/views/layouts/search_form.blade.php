<form method="GET" action="{{ route('search.user') }}">
    @csrf
    <div class="field has-addons">
        <div class="control">
            <input type="text" name="query" placeholder="Search users ...." class="input">
        </div>
        <div class="control">
            <button type="submit" class="button is-link">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
</form>