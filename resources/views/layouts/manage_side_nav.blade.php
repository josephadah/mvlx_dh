<aside class="menu manage-side-nav">
	<a class="is-size-6" href="{{ route('manage.dashboard') }}">
		Dashboard
	</a>
	<p class="menu-label">
		Manage Tasks
	</p>
	<ul class="menu-list">
		<li v-if="isSuperAdmin" v-cloak><a href="{{ route('manage.users') }}">Users</a></li>
		<li v-if="isSuperAdmin" v-cloak><a href="{{ route('sections.index') }}">Sections</a></li>
		<li v-if="isSuperAdmin" v-cloak><a href="{{ route('mails.index') }}">Mail</a></li>
		<li><a href="{{ route('feedbacks.index') }}">Feedbacks</a></li>
		<li><a href="{{ route('reports.index') }}">Reports</a></li>
		<li><a href="{{ route('appeals.index') }}">Appeals</a></li>
	</ul>
	<p class="menu-label">Manage Contents</p>
	<ul class="menu-list">
		<li><a href="#">Threads</a></li>
		<li><a href="#">Questions</a></li>
		<li><a href="#">News</a></li>

	</ul>
	<p class="menu-label">Manage Adverts</p>
	<ul class="menu-list" v-if="isSuperAdmin" v-cloak>
		<li><a href="{{ route('adverts.index') }}">Adverts</a></li>
	</ul>
	<p class="menu-label">
		My Settings
	</p>
	<ul class="menu-list">
		<li><a href="#">Change Password</a></li>
	</ul>
	<a href="#" class="is-link">Logout</a>
</aside>