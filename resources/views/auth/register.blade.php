@extends('layouts.static_desktop')

@section('title', 'NaijaNewsForum | Register')

@section('content')
    <div class="columns is-centered m-10">
        <div class="column is-6">
            <div class="box">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <p class="is-size-4 has-text-centered has-border-bottom-2 m-b-5">Join NaijaNews Forum</p>
                    
                    <div class="field">
                        <label for="username" class="label">Username: </label> 
                        <div class="control has-icons-left has-icons-right">
                            <input type="text" 
                                class="input {{ $errors->has('username') ? ' is-danger' : '' }}" 
                                id="username" name="username" 
                                placeholder="Enter a Username"
                                value="{{ old('username') }}" 
                                pattern="^[a-zA-Z0-9_]+$" required autofocus>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-user"></i>
                                </span>
                            @if ($errors->has('username'))
                                <p class="help is-danger">
                                    {{ $errors->first('username') }}
                                </p>
                            @endif
                        </div>
                        <p class="is-size-7">
                            <span class="has-text-danger">NB:</span>(only alphabets, numbers and underscores without spaces are allowed.
                        </p>
                    </div>

                    <div class="field">
                        <label for="email" class="label">E-Mail Address</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="email" 
                                class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
                                id="email" name="email" 
                                placeholder="Enter A Valid E-Mail Address"
                                value="{{ old('email') }}" required autofocus>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-envelope"></i>
                                </span>
                            @if ($errors->has('email'))
                                <p class="help is-danger">
                                    {{ $errors->first('email') }}
                                </p>
                            @endif
                        </div>
                        <p class="is-size-7">
                            <span class="has-text-danger">NB:</span>Use an email address that you can login to confirm.
                        </p>
                    </div>

                    <div class="field">
                        <label for="password" class="label">Password</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="password" 
                                class="input {{ $errors->has('password') ? ' is-danger' : '' }}" 
                                id="password" name="password" 
                                placeholder="Enter Your Password"
                                value="{{ old('password') }}" required>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-lock"></i>
                                </span>
                            @if ($errors->has('password'))
                                <p class="help is-danger">
                                    {{ $errors->first('password') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="field">
                        <label for="password-confirm" class="label">Confirm Password</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="password" 
                                class="input" 
                                id="password-confirm" name="password_confirmation" required>
                            <span class="icon is-small is-left">
                              <i class="fa fa-lock"></i>
                            </span>
                        </div>
                    </div>

                    <p><a class="is-size-7" href="{{ route('terms') }}">Read Terms and Conditions Here.</a></p>

                    <div class="field">
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="terms" required>
                                    I accept the Terms and Condition binding the use of this platform.
                            </label>
                        </div>

                        @if ($errors->has('terms'))
                            <p class="help is-danger">
                                {{ $errors->first('terms') }}
                            </p>
                        @endif
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-link">
                                Submit
                            </button>
                        </div>
                    </div>

                </form>
            </div>        
        </div>
    </div>

@endsection
