@extends('layouts.static_desktop')

@section('title', 'NaijaNewsForum | Login')

@section('content')
    <div class="columns is-centered m-10">
        <div class="column is-6">
            <div class="box">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <p class="is-size-4 has-text-centered has-border-bottom-2 m-b-5">Login</p>
                    <p class="m-b-20 has-text-centered">Please Login to Participate on NaijaNews Forum</p>
                    <div class="field">
                        <label for="email" class="label">E-Mail Address</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="email" 
                                class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
                                id="email" name="email" 
                                placeholder="Enter A Valid E-Mail Address"
                                value="{{ old('email') }}" required autofocus>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-envelope"></i>
                                </span>
                            @if ($errors->has('email'))
                                <p class="help is-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="field">
                        <label for="password" class="label">Password</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="password" 
                                class="input {{ $errors->has('password') ? ' is-danger' : '' }}" 
                                id="password" name="password" 
                                placeholder="Enter Your Password"
                                value="{{ old('password') }}" required>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-lock"></i>
                                </span>
                            @if ($errors->has('password'))
                                <p class="help is-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-link">
                                Login
                            </button> <br/>
                        </div>
                    </div>

                </form>

                <p class="m-t-10 m-b-10">
                    <a class="is-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                </p>

                <p>Don't have an Account? <a href="{{ route('register') }}">Register Now</a></p>
            </div>        
        </div>
    </div>
@endsection
