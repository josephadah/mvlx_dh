@extends('layouts.app')

@section('title', ' Reset request sent')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="has-text-success is-size-3 has-border-bottom">Success!</p>
                    <p class="is-size-5">A reset password email has been sent to you</p>
                    <p class="is-size-5 has-text-link m-20">Please check your inbox (or spam box) in your email to reset password.</p>
                    <p>
                        <span class="is-size-6">
                            <span class="has-text-danger">NB:</span>
                            If you didn't receive the email immediately, please check after few minutes.
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
@endsection
