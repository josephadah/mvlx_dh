@extends('layouts.static_desktop')

@section('title', ' Reset Password')

@section('content')
    <div class="columns is-centered">
        <div class="column is-6">
            <div class="box">
                <form method="POST" action="/password/reset">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">

                    <p class="title has-text-centered">Reset Password</p>

                    <div class="field">
                        <label for="email" class="label">E-Mail Address</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="email" 
                                class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
                                id="email" name="email" 
                                placeholder="Enter A Valid E-Mail Address"
                                value="{{ old('email') }}" required autofocus>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-envelope"></i>
                                </span>
                            @if ($errors->has('email'))
                                <p class="help is-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="field">
                        <label for="password" class="label">Password</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="password" 
                                class="input {{ $errors->has('password') ? ' is-danger' : '' }}" 
                                id="password" name="password" 
                                placeholder="Enter Your Password"
                                value="{{ old('password') }}" required>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-lock"></i>
                                </span>
                            @if ($errors->has('password'))
                                <p class="help is-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="field">
                        <label for="password-confirm" class="label">Confirm Password</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="password" 
                                class="input" 
                                id="password-confirm" name="password_confirmation" required>
                            <span class="icon is-small is-left">
                              <i class="fa fa-lock"></i>
                            </span>
                            @if ($errors->has('password_confirmation'))
                                <p class="help is-danger">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-link">
                                Reset Password
                            </button>
                        </div>
                    </div>

                </form>
            </div>        
        </div>
    </div>

@endsection