@extends('layouts.static_desktop')

@section('title', ' Reset Password')

@section('content')
    <div class="columns is-centered">
        <div class="column is-6">
            <div class="box">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <p class="title has-text-centered">Reset Password</p>
                    <div class="field">
                        <label for="email" class="label">E-Mail Address</label>
                        <div class="control has-icons-left has-icons-right">
                            <input type="email" 
                                class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
                                id="email" name="email" 
                                placeholder="Enter Your Email Address"
                                value="{{ old('email') }}" required autofocus>
                                <span class="icon is-small is-left">
                                  <i class="fa fa-envelope"></i>
                                </span>
                            @if ($errors->has('email'))
                                <p class="help is-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-link">
                                Send Password Reset Link
                            </button> <br/>
                        </div>
                    </div>

                </form>
            </div>        
        </div>
    </div>
@endsection