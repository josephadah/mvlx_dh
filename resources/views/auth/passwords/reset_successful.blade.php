@extends('layouts.app')

@section('title', ' Resent successful')

@section('slogan')
    <div class="columns is-marginless">
        <div class="column has-text-centered" style="background-color: #fff;">
            <h3>#1 News Discussion Forum in Nigeria.</h3>
        </div>
    </div>
@endsection

@section('content')

    <div class="box m-10">
        <div class="columns is-centered">
            <div class="column-10">
                <div class="box has-text-centered">
                    <p class="has-text-success is-size-4 has-border-bottom">Success!</p>
                    <p class="is-size-5">Your password reset was successfully.</p>
                    <p class="is-size-5 has-text-link m-20">
                        <a href="{{ route('home') }}" class="button is-small is-link">Go to Home</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
@endsection
