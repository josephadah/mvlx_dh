	{{-- Main activity title and body --}}
<div class="has-border m-b-20"> 
	<h2 class="is-size-5 has-text-centered p-5 m-l-10">
		<a class="is-darkgreen has-text-weight-semibold" href="{{ $activity->subject->showPath() }}">{{ $activity->subject->title }}</a>
	</h2>

	<div class="media has-border-top-bottom p-5">
		<figure class="media-left">
			<p class="image is-32x32">
				<a href="{{ route('profile', $activity->subject->creator) }}">
					<img src="{{ $activity->subject->creator->thumbnail }}" alt="">
				</a>
			</p>
		</figure>
		<div class="media-content">
			<div class="level is-mobile is-marginless">
				<div class="level-left">
					<span class="is-size-7">
						<strong>
							<a href="{{ route('profile', $activity->subject->creator) }}">
								{{ $activity->subject->creator->username }}
							</a> <span>({{ $activity->subject->creator->reputation }} xp)</span>
						</strong><br>
					</span>
				</div>
				<div class="level-right">
					<span class="is-size-7">
						<strong>{{ $activity->subject->created_at->diffForHumans() }}</strong>
					</span>
				</div>
			</div>
			<p>
				<span class="is-size-7">
					<strong>
						{{ str_limit($activity->subject->creator->title, 50) }}
					</strong>
				</span>
			</p>
		</div>
	</div> {{-- end of media --}}

	{{-- <div class="p-10 has-text-justified">
		<p>
			{!! str_limit(strip_tags($activity->subject->body, '<br/><br><p>'), 100, '') !!}
			@if(strlen(strip_tags($activity->subject->body, '<br/><br><p>')) > 100)
				<a href="{{ $activity->subject->showPath() }}" class="btn btn-primary is-size-7">
					...Continue Reading
				</a>
			@endif
		</p>
	</div> --}}

	<div class="has-border-top">
		<span class="is-size-7">
			<span class="p-l-10">
				{{ $likesCount = $activity->subject->likes_count }} {{ str_plural('Like', $likesCount) }}
			</span>
			<span class="p-l-10">
				{{ $likesCount = $activity->subject->dislikes_count }} {{ str_plural('Dislike', $likesCount) }}
			</span>
			<span class="p-l-10">
				{{ $activity->subject->replies_count }} {{ str_plural('reply', $activity->subject->replies_count) }}
			</span>
		</span>
	</div>
</div> {{-- end of main activity title and body --}}