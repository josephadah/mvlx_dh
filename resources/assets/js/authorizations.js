let userId = window.App.userId;

module.exports = {
    owns (model, prop = 'user_id') {
        return parseInt(model[prop]) === userId;
    }
};
