
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import 'jquery.caret';
import 'at.js';

window.Vue = require('vue');

window.events = new Vue();

window.flash = function (message, level = 'success') {
	window.events.$emit('flash', {message, level});
}

let authorizations = require('./authorizations');

Vue.prototype.authorize = function (...params) {
    if (! window.App.signedIn) return false;

    if (typeof params[0] === 'string') {
        return authorizations[params[0]](params[1], params[2]);
    }

    return params[0](window.App.user);
};

Vue.prototype.signedIn = window.App.signedIn;
Vue.prototype.isConfirmed = window.App.isConfirmed;
Vue.prototype.isAdmin = window.App.isAdmin;
Vue.prototype.isSuperAdmin = window.App.isSuperAdmin;
Vue.prototype.banned = window.App.banned;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('flash', require('./components/Flash.vue'));
Vue.component('admin-actions', require('./components/AdminActions.vue'));
Vue.component('paginator', require('./components/Paginator.vue'));
Vue.component('user-notification', require('./components/UserNotification.vue'));
Vue.component('subscribe-button', require('./components/SubscribeButton.vue'));
Vue.component('thread', require('./components/Thread.vue'));
Vue.component('question', require('./components/Question.vue'));
Vue.component('news', require('./components/News.vue'));
Vue.component('status', require('./components/Status.vue'));
Vue.component('profile', require('./components/Profile.vue'));

const app = new Vue({
    el: '#app', 
    data() {
    	return {
            showStatusHidden: false,
            showStatusAddImage: false
    	}
    }, 

    methods: {
        markAsRead(notification) {
            axios.delete('/notifications/' + notification);
        },
    }

    // computed: {
    // 	checkDevice() {
    // 		if(window.innerWidth < 760) {
    // 			return true;
    // 		} else {
    //             return false;
    //         }
    // 	}
    // }
});

// toggle nav menu
$('.is-clickable').click(function() {
    $(this).toggleClass('is-active');
});


// script for at.js for searching news region
$('#searchRegion').atwho({
    'at': "", 
    delay: 1000, 
    callbacks: {
        remoteFilter: function(query, callback) {
            console.log('called');
            $.getJSON("/api/regions", {region: query}, function(regions) {
                callback(regions)
            });
        }
    }
});