<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_has_a_profile_page()
    {
    	$user = factory('App\User')->create();

    	$this->get('/' . $user->username)
    		->assertSee($user->username);
    }

    /** @test */
    public function a_user_profile_page_contains_their_threads()
    {
        $this->signIn();

    	$thread = factory('App\Thread')->create(['user_id' => auth()->id()]);

    	$this->get('/' . auth()->user()->username)
    		->assertSee($thread->title);
    }
}
