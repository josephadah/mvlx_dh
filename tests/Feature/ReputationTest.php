<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Reputation;

class ReputationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_earns_point_when_they_create_thread()
    {
    	$this->signIn();

    	$thread = factory('App\Thread')->create(['user_id' => auth()->id()]);

    	$this->assertEquals(Reputation::THREAD_WAS_CREATED, $thread->creator->reputation);
    }

    /** @test */
    function a_user_looses_point_when_they_delete_thread()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create(['user_id' => auth()->id()]);

        $this->assertEquals(Reputation::THREAD_WAS_CREATED, $thread->creator->reputation);

        $this->delete($thread->path());

        $this->assertEquals(0, $thread->creator->fresh()->reputation);
    }

    /** @test */
    function a_user_earns_point_when_they_reply_to_thread()
    {
    	$this->signIn();

    	$reply = factory('App\Reply')->create();

    	$this->assertEquals(Reputation::REPLY_WAS_CREATED, $reply->owner->reputation);
    }

    /** @test */
    function a_user_looses_point_when_they_delete_their_reply()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create(['user_id' => auth()->id()]);

        $this->assertEquals(Reputation::REPLY_WAS_CREATED, $reply->owner->reputation);

        $this->delete('/replies/' . $reply->id);

        $this->assertEquals(0, $reply->owner->fresh()->reputation);
    }
}
