<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PinnedThreadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function regular_users_cannot_pin_threads()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = factory(\App\Thread::class)->create(['user_id' => auth()->id()]);

        $this->post(route('pinned-threads.store', $thread))->assertStatus(403);

        $this->assertFalse($thread->fresh()->pinned);
    }

    /** @test */
    public function regular_users_cannot_unpin_threads()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = factory(\App\Thread::class)->create(['user_id' => auth()->id(), 'pinned' => true]);

        $this->delete(route('pinned-threads.destroy', $thread))->assertStatus(403);

        $this->assertTrue($thread->fresh()->pinned);
    }

    /** @test */
    function an_administrator_can_pin_threads()
    {
    	$this->signInAdmin();

    	$thread = factory('App\Thread')->create();

    	$this->post(route('pinned-threads.store', $thread));

    	$this->assertTrue($thread->fresh()->pinned);
    }

    /** @test */
    function an_administrator_can_unpin_threads()
    {
    	$this->signInAdmin();

    	$thread = factory('App\Thread')->create(['pinned' => true]);

    	$this->delete(route('pinned-threads.destroy', $thread));

    	$this->assertFalse($thread->fresh()->pinned);
    }
}
