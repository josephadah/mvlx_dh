<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class ThreadFeaturesTest extends TestCase
{
	use RefreshDatabase;

	protected $thread;

	protected function setUp()
	{
		parent::setUp();

		$this->thread = factory('App\Thread')->create();
	}

    /** @test */
    function any_user_can_view_all_threads()
    {
    	$this->get('/forum')
    		->assertSee($this->thread->title);
    }

    /** @test */
    function any_user_can_view_a_single_thread()
    {
    	$this->get($this->thread->showPath())
            ->assertSee($this->thread->title)
            ->assertSee($this->thread->body);
    }

    /** @test */
    function a_user_can_filter_threads_according_to_section()
    {
        $section = factory('App\Section')->create();
        $thread = factory('App\Thread')->create(['section_id' => $section->id]);
        $threadNotInSection = factory('App\Thread')->create();
        $this->get('/forum/' . $section->slug)
            ->assertSee($thread->fresh()->title) 
            ->assertDontSee($threadNotInSection->fresh()->title);
    }

    /** @test */
    function a_user_can_filter_threads_by_trending()
    {
        $threadWithTwoReplies = factory('App\Thread')->create();
        factory('App\Reply', 2)->create(['thread_id' => $threadWithTwoReplies->id]);

        $threadWithThreeReplies = factory('App\Thread')->create();
        factory('App\Reply', 3)->create(['thread_id' => $threadWithThreeReplies->id]);

        $threadWithNoReplies = $this->thread;

        $response = $this->getJson('/threads?trending=1')->json();
        
        $this->assertEquals([3, 2, 0], array_column($response, 'replies_count'));
    }

    /** @test */
    function an_unauthenticated_user_cannot_create_a_thread()
    {
    	$this->withExceptionHandling();

        $this->get('/threads/create')
            ->assertRedirect('login');
            
    	$this->post('/threads')
    		->assertRedirect('/login');
    }

    /** @test */
    function authenticated_users_must_confirm_their_email_before_creating_threads()
    {
        $user = factory('App\User')->create(['confirmed' => false]);
        $this->signIn($user);

        $thread = factory('App\Thread')->make(['user_id' => auth()->id()]);

        $this->post(route('threads.store'), $thread->toArray())
            ->assertRedirect('email-not-confirmed');
    }

    /** @test */
    function an_authenticated_and_confirmed_user_can_create_a_thread()
    {
		$this->signIn();

        $section_id = factory('App\Section')->create()->id;

		$thread = factory('App\Thread')->make([
			'title' => 'A title', 
			'body' => 'A body',
            'section' => $section_id, 
			'user_id' => auth()->id(), 
            'terms' => true
		]);

		$this->post(route('threads.store'), $thread->toArray());

    	$this->assertDatabaseHas('threads', ['title' => 'A title']);
    }

    /** @test */
    public function a_thread_must_have_a_title()
    {
        $this->withExceptionHandling()->signIn();

        $thread = factory('App\Thread')->make(['title' => null]);

        $this->post(route('threads.store'), $thread->toArray())
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_thread_must_have_a_body()
    {
        $this->withExceptionHandling()->signIn();

        $thread = factory('App\Thread')->make(['body' => null]);

        $this->post(route('threads.store'), $thread->toArray())
            ->assertSessionHasErrors('body');
    }

    /** @test */
    public function a_thread_must_belong_to_a_section()
    {
        $this->withExceptionHandling()->signIn();

        factory('App\Section', 2)->create();

        $thread = factory('App\Thread')->make(['section' => null]);

        $this->post(route('threads.store'), $thread->toArray())
            ->assertSessionHasErrors('section');

        $thread2 = factory('App\Thread')->make(['section' => 567]);

        $this->post(route('threads.store'), $thread2->toArray())
            ->assertSessionHasErrors('section');
    }

    /** @test */
    function an_unauthorized_user_cannot_edit_a_thread()
    {
        $this->withExceptionHandling()->signIn();

        $thread = factory('App\Thread')->create(['user_id' => factory('App\User')->create()]);

        $this->patch($thread->path())
            ->assertStatus(403);
    }

    /** @test */
    function an_authorized_user_can_edit_their_thread()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create(['user_id' => auth()->id()]);

        $this->patch($thread->path(), ['body' => 'Updated Body', 'title' => 'Updated Title']);

        $this->assertDatabaseHas('threads', [
            'id' => $thread->id, 'body' => 'Updated Body', 'title' => 'Updated Title', 
            ]);
    }

    /** @test */
    function an_unauthorized_user_cannot_delete_a_thread()
    {
        $this->withExceptionHandling();
        
        $this->delete($this->thread->path())
            ->assertRedirect('login');

        $this->signIn();

        $this->delete($this->thread->path())
            ->assertStatus(403);
    }

    /** @test */
    function an_authorized_user_can_delete_their_thread()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create(['user_id' => auth()->id()]);

        $reply = factory('App\Reply')->create(['thread_id' => $thread->id]);

        $this->delete($thread->path());

        $this->assertDatabaseMissing('threads', [
            'id' => $thread->id, 'title' => $thread->title, 'body' => $thread->body
        ]);

        $this->assertDatabaseMissing('replies', [
            'id' => $reply->id
        ]);

        $this->assertDatabaseMissing('activities', [
            'subject_id' => $thread->id,
            'subject_type' => get_class($thread)
        ]);

        $this->assertDatabaseMissing('activities', [
            'subject_id' => $reply->id,
            'subject_type' => get_class($reply)
        ]);
    }

}
