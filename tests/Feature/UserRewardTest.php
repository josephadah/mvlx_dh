<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserRewardTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_earn_one_ucoin_for_every_100xp_reputation()
    {
    	$user = factory('App\User')->create();

    	$user->reputation = 100;

    	$user->earnReward();

    	$this->assertEquals(1, $user->reward);
    }

    /** @test */
    function a_user_earn_one_ucoin_after_successful_registration()
    {
    	$this->post(route('register'), [
    		'username' => 'Joe',
    		'email' => 'joe@mail.com', 
    		'password' => 'password', 
    		'password_confirmation' => 'password', 
            'terms' => true
    	]);

    	$user = User::where('username', 'Joe')->first();

    	$this->assertEquals(1, $user->reward);
    }

    /** @test */
    function an_admin_can_increment_a_user_reward()
    {
        $admin = factory('App\User')->create(['email' => 'jonathanadahjoe@gmail.com']);

        $this->signIn($admin);

        $user = factory('App\User')->create();

        $this->assertEquals(0, $user->reward);

        $this->post('award-coin/'.$user->username);

        $this->assertEquals(1, $user->fresh()->reward);
    }

    /** @test */
    function an_admin_can_decrement_a_user_reward()
    {
        $admin = factory('App\User')->create(['email' => 'jonathanadahjoe@gmail.com']);

        $this->signIn($admin);

        $user = factory('App\User')->create();

        $this->assertEquals(0, $user->reward);

        $this->post('award-coin/'.$user->username);

        $this->assertEquals(1, $user->fresh()->reward);

        $this->delete('award-coin/'.$user->username);

        $this->assertEquals(0, $user->fresh()->reward);
    }
}
