<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParticipateInThreadTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function an_unauthenticated_user_cannot_reply_to_a_thread()
    {   
        $this->withExceptionHandling();
        
    	$this->post('/threads/1/replies', [])
            ->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_reply_to_a_thread()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $reply = factory('App\Reply')->make();

        $this->post($thread->path() . '/replies', $reply->toArray());
        
        $this->assertDatabaseHas('replies', ['body' => $reply->body]);
    }

    /** @test */
    public function a_reply_must_contain_a_body()
    {
        $this->withExceptionHandling()->signIn();

        $thread = factory('App\Thread')->create();

        $reply = factory('App\Reply')->make(['body' => null]);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertSessionHasErrors('body');
    }

    /** @test */
    public function an_unauthorized_user_cannot_delete_a_reply()
    {
        $this->withExceptionHandling();

        $reply = factory('App\Reply')->create();

        $this->delete('/replies/' . $reply->id)
            ->assertRedirect('login');

        $this->signIn();

        $this->delete('/replies/' . $reply->id)
            ->assertStatus(403);
    }

    /** @test */
    public function an_authorized_user_can_delete_their_reply()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create(['user_id' => auth()->id()]);

        $this->delete('/replies/' . $reply->id)
            ->assertStatus(302);

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
    }

    /** @test */
    public function an_unauthorized_user_cannot_edit_a_reply()
    {
        $this->withExceptionHandling();

        $reply = factory('App\Reply')->create();

        $this->post('/update-replies/' . $reply->id)
            ->assertRedirect('login');

        $this->signIn();

        $this->post('/update-replies/' . $reply->id)
            ->assertStatus(403);
    }

    /** @test */
    public function an_authorized_user_can_edit_their_reply()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create(['user_id' => auth()->id()]);

        $updatedBody = "This is an updated body";

        $this->post('/update-replies/' . $reply->id, ['body' => $updatedBody]);

        $this->assertDatabaseHas('replies', ['id' => $reply->id, 'body' => $updatedBody]);
    }

    /** @test */
    public function a_user_can_fetch_replies_for_a_given_thread()
    {
        $thread = factory('App\Thread')->create();

        $reply = factory('App\Reply', 2)->create(['thread_id' => $thread->id]);

        $response = $this->getJson($thread->path() . '/replies')->json();

        $this->assertCount(2, $response['data']);
        
        $this->assertEquals(2, $response['total']);
    }

    /** @test */
    function replies_that_contain_spam_may_not_be_created()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = factory('App\Thread')->create();
        $reply = factory('App\Reply')->make(['body' => 'fuck you']);

        $this->json('post', $thread->path() . '/replies', $reply->toArray())
            ->assertStatus(422);
    }

    /** @test */
    function a_user_may_only_reply_once_in_few_seconds()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = factory('App\Thread')->create();

        $reply = factory('App\Reply')->make();

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(200);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(422);
    }

}
