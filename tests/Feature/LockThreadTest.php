<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LockThreadTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function a_locked_thread_cannot_recieve_any_reply() 
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $thread->lock();

        $this->post($thread->path() . '/replies', [
            'body' => 'A reply', 
            'user_id' => auth()->id()
        ])->assertStatus(422);
    }

    /** @test */
    function a_non_administrator_cannot_lock_a_thread()
    {
    	$this->withExceptionHandling()->signIn();

    	$thread = factory('App\Thread')->create();

    	$this->post(route('locked-threads.store', $thread))
    		->assertStatus(403);

    	$this->assertFalse(!! $thread->fresh()->locked);
    }

    /** @test */
    function an_administrator_can_lock_a_thread()
    {
    	$this->signInAdmin();

    	$thread = factory('App\Thread')->create();

    	$this->post(route('locked-threads.store', $thread))
    		->assertStatus(200);

    	$this->assertTrue(!! $thread->fresh()->locked);
    }

    /** @test */
    function an_administrator_can_unlock_a_thread()
    {
        $this->signInAdmin();

        $thread = factory('App\Thread')->create();

        $this->post(route('locked-threads.store', $thread))
            ->assertStatus(200);

        $this->assertTrue(!! $thread->fresh()->locked);

        $this->delete(route('locked-threads.store', $thread))
            ->assertStatus(200);

        $this->assertFalse(!! $thread->fresh()->locked);
    }
}
