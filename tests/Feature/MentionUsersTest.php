<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MentionUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    //
    // WORKING IN APP BUT NOT IN TEST
    // 
    // public function mentioned_users_in_a_reply_are_notified()
    // {
    // 	$john = factory('App\User')->create(['username' => 'John']);

    // 	$this->signIn($john);

    // 	$jane = factory('App\User')->create(['username' => 'Jane_Joe']);

    // 	$thread = factory('App\Thread')->create();

    // 	$reply = factory('App\Reply')->make(['body' => 'Hello @Jane_Joe* how are you doing.']);

    // 	$this->json('post', $thread->path() . '/replies', $reply->toArray());

    // 	// notify jane
    // 	$this->assertCount(1, $jane->notifications);
    // }

    /** @test */
    function it_can_fetch_all_mentioned_users_starting_with_the_given_characters()
    {
        factory('App\User')->create(['username' => 'adah']);
        factory('App\User')->create(['username' => 'adahrose']);
        factory('App\User')->create(['username' => 'Mary']);
        factory('App\User')->create(['username' => 'joe']);

        $results = $this->json('GET', '/api/users', ['username' => 'ada']);

        $this->assertCount(2, $results->json());
    }
}
