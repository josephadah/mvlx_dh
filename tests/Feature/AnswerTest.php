<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AnswerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_knows_if_its_the_best_answer()
    {
    	$answer = factory('App\Answer')->create();

    	$this->assertFalse($answer->isBest());

    	$answer->question->update(['best_answer_id' => $answer->id]);

    	$this->assertTrue($answer->fresh()->isBest());
    }
}
