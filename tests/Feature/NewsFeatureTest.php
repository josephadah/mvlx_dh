<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NewsFeatureTest extends TestCase
{
    use RefreshDatabase;

	protected $news;

	protected function setUp()
	{
		parent::setUp();

		$this->news = factory('App\News')->create();
	}

    /** @test */
    function any_user_can_view_all_news()
    {
    	$this->get('/news-home')
    		->assertSee($this->news->title);
    }

    /** @test */
    function any_user_can_view_a_single_news()
    {
    	$this->get($this->news->showPath())
            ->assertSee($this->news->title)
            ->assertSee($this->news->body);
    }

    /** @test */
    function a_user_can_filter_news_according_to_section()
    {
        $section = factory('App\Section')->create();
        $news = factory('App\News')->create(['section_id' => $section->id]);
        $newsNotInSection = factory('App\News')->create();
        $this->get('/news-section/' . $section->slug)
            ->assertSee($news->fresh()->title) 
            ->assertDontSee($newsNotInSection->fresh()->title);
    }

    /** @test */
    function a_user_can_filter_news_according_to_region()
    {
        $region = factory('App\Region')->create();
        $news = factory('App\News')->create(['region_id' => $region->id]);
        $newsNotInRegion = factory('App\News')->create();
        $this->get('search-news?region=' . $region->name)
            ->assertSee($news->fresh()->title) 
            ->assertDontSee($newsNotInRegion->fresh()->title);
    }

    /** @test */
    function a_user_can_filter_news_by_trending()
    {
        $newsWithTwoComments = factory('App\News')->create();
        factory('App\Comment', 2)->create(['news_id' => $newsWithTwoComments->id]);

        $newsWithThreeComments = factory('App\News')->create();
        factory('App\Comment', 3)->create(['news_id' => $newsWithThreeComments->id]);

        $newsWithNoComment = $this->news;

        $response = $this->getJson('/news?trending=1')->json();
        
        $this->assertEquals([3, 2, 0], array_column($response, 'comments_count'));
    }

    /** @test */
    function an_unauthenticated_user_cannot_create_a_news()
    {
    	$this->withExceptionHandling();

        $this->get('/news/create')
            ->assertRedirect('login');
            
    	$this->post('/news')
    		->assertRedirect('/login');
    }

    /** @test */
    function authenticated_users_must_confirm_their_email_before_creating_news()
    {
        $user = factory('App\User')->create(['confirmed' => false]);
        $this->signIn($user);

        $news = factory('App\News')->make(['user_id' => auth()->id()]);

        $this->post(route('news.store'), $news->toArray())
            ->assertRedirect('email-not-confirmed');
    }

    /** @test */
    function an_authenticated_and_confirmed_user_can_create_a_news()
    {
		$this->signIn();

        $section_id = factory('App\Section')->create()->id;
        $region_id = factory('App\Region')->create()->id;

		$news = factory('App\News')->make([
			'title' => 'A title', 
			'body' => 'A body',
            'section' => $section_id, 
            'region_id' => $region_id,
			'user_id' => auth()->id(), 
            'terms' => true
		]);

		$this->post(route('news.store'), $news->toArray());

    	$this->assertDatabaseHas('news', ['title' => 'A title']);
    }

    /** @test */
    public function a_news_must_have_a_title()
    {
        $this->withExceptionHandling()->signIn();

        $news = factory('App\News')->make(['title' => null]);

        $this->post(route('news.store'), $news->toArray())
            ->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_news_must_have_a_body()
    {
        $this->withExceptionHandling()->signIn();

        $news = factory('App\News')->make(['body' => null]);

        $this->post(route('news.store'), $news->toArray())
            ->assertSessionHasErrors('body');
    }

    /** @test */
    public function a_news_must_belong_to_a_section()
    {
        $this->withExceptionHandling()->signIn();

        factory('App\Section', 2)->create();

        $news = factory('App\News')->make(['section' => null]);

        $this->post(route('news.store'), $news->toArray())
            ->assertSessionHasErrors('section');

        $news2 = factory('App\News')->make(['section' => 567]);

        $this->post(route('news.store'), $news2->toArray())
            ->assertSessionHasErrors('section');
    }

    /** @test */
    function an_unauthorized_user_cannot_edit_a_news()
    {
        $this->withExceptionHandling()->signIn();

        $news = factory('App\News')->create(['user_id' => factory('App\User')->create()]);

        $this->patch($news->path())
            ->assertStatus(403);
    }

    /** @test */
    function an_authorized_user_can_edit_their_news()
    {
        $this->signIn();

        $news = factory('App\News')->create(['user_id' => auth()->id()]);

        $this->patch($news->path(), ['body' => 'Updated Body', 'title' => 'Updated Title']);

        $this->assertDatabaseHas('news', [
            'id' => $news->id, 'body' => 'Updated Body', 'title' => 'Updated Title', 
            ]);
    }

    /** @test */
    function an_unauthorized_user_cannot_delete_a_news()
    {
        $this->withExceptionHandling();
        
        $this->delete($this->news->path())
            ->assertRedirect('login');

        $this->signIn();

        $this->delete($this->news->path())
            ->assertStatus(403);
    }

    /** @test */
    function an_authorized_user_can_delete_their_news()
    {
        $this->signIn();

        $news = factory('App\News')->create(['user_id' => auth()->id()]);

        $comment = factory('App\Comment')->create(['news_id' => $news->id]);

        $this->delete($news->path());

        $this->assertDatabaseMissing('news', [
            'id' => $news->id, 'title' => $news->title, 'body' => $news->body
        ]);

        $this->assertDatabaseMissing('comments', [
            'id' => $comment->id
        ]);

        $this->assertDatabaseMissing('activities', [
            'subject_id' => $news->id,
            'subject_type' => get_class($news)
        ]);

        $this->assertDatabaseMissing('activities', [
            'subject_id' => $comment->id,
            'subject_type' => get_class($comment)
        ]);
    }
}
