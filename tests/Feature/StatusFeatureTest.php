<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusFeatureTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	function any_user_can_view_a_status()
	{
		$status = factory('App\Status')->create();

		$this->get(route('statuses.show', $status))
			->assertSee($status->body);
	}

    /** @test */
    function an_authenticated_and_confirm_user_can_create_a_status()
    {
    	$this->signIn($user = factory('App\User')->create());

    	$user->confirmed = true;

    	$status = factory('App\Status')->make(['user_id' => auth()->id()]);

    	$response = $this->post(route('statuses.store', $user->username), $status->toArray());

    	$this->assertDatabaseHas('statuses', ['user_id' => auth()->id(), 'body' => $status->body]);
    }
}
