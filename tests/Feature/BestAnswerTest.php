<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BestAnswerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_question_creator_may_mark_an_answer_as_the_best_answer()
    {
    	$this->signIn();

    	$question = factory('App\Question')->create(['user_id' => auth()->id()]);

    	$answers = factory('App\Answer', 3)->create(['question_id' => $question->id]);

    	$this->assertFalse($answers[1]->fresh()->isBest());

    	$this->postJson(route('best-answer.store', [$answers[1]->id]));

    	$this->assertTrue($answers[1]->fresh()->isBest());
    }

    /** @test */
    function only_the_question_creator_can_mark_best_reply()
    {
    	$this->withExceptionHandling()->signIn();

    	$question = factory('App\Question')->create(['user_id' => auth()->id()]);

    	$answers = factory('App\Answer', 3)->create(['question_id' => $question->id]);

    	$this->assertFalse($answers[1]->fresh()->isBest());

    	$this->signIn(factory('App\User')->create());

    	$this->postJson(route('best-answer.store', [$answers[1]->id]))
    		->assertStatus(403);

    	$this->assertFalse($answers[1]->fresh()->isBest());
    }
}
