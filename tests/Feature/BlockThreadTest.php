<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BlockThreadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_blocked_thread_body_is_not_visible() 
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $thread->block();

        $this->assertEquals(
            '<p style="color:red;">This thread has been blocked.</p>', 
            $thread->fresh()->body
        );

        $this->get($thread->showPath())
        	->assertSee('This thread has been blocked.');
    }

    /** @test */
    function a_blocked_thread_cannot_receive_a_new_reply()
    {
    	$this->signIn();

        $thread = factory('App\Thread')->create();

        $thread->block();

    	$this->post($thread->path() . '/replies', [
            'body' => 'A reply', 
            'user_id' => auth()->id()
        ])->assertStatus(422);
    }

    /** @test */
    function a_non_administrator_cannot_block_a_thread()
    {
    	$this->withExceptionHandling()->signIn();

    	$thread = factory('App\Thread')->create();

    	$this->post(route('blocked-threads.store', $thread))
    		->assertStatus(403);

    	$this->assertFalse(!! $thread->fresh()->blocked);
    }

    /** @test */
    function an_administrator_can_block_a_thread()
    {
    	$this->signInAdmin();

    	$thread = factory('App\Thread')->create();

    	$this->post(route('blocked-threads.store', $thread))
    		->assertStatus(200);

    	$this->assertTrue($thread->fresh()->blocked);
    }

    /** @test */
    function an_administrator_can_unlock_a_thread()
    {
        $this->signInAdmin();

        $thread = factory('App\Thread')->create();

        $this->post(route('blocked-threads.store', $thread))
            ->assertStatus(200);

        $this->assertTrue($thread->fresh()->blocked);

        $this->delete(route('blocked-threads.destroy', $thread))
            ->assertStatus(200);

        $this->assertFalse($thread->fresh()->blocked);
    }
}
