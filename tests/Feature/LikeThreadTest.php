<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LikeThreadTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function an_unauthenticated_user_cannot_like_anything()
    {
    	$this->withExceptionHandling()
    		->post('/threads/1/likes')
    		->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_like_a_thread()
    {
    	$this->signIn();

    	$thread = factory('App\Thread')->create();

    	$this->post($thread->path() . '/likes');

    	$this->assertCount(1, $thread->likes);
    }

    /** @test */
    public function a_user_can_unlike_a_thread_by_second_click()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $this->post($thread->path() . '/likes');
        $this->post($thread->path() . '/likes');

        $this->assertCount(0, $thread->likes);
    }

    /** @test */
    public function a_user_cannot_like_and_dislike_a_thread()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $this->post($thread->path() . '/likes');
        $this->assertCount(1, $thread->likes);
        $this->post($thread->path() . '/dislikes');
        $this->assertCount(1, $thread->fresh()->dislikes);
        $this->assertCount(0, $thread->fresh()->likes);
    }

    /** @test */
    public function an_unauthenticated_user_cannot_dislike_anything()
    {
        $this->withExceptionHandling()
            ->post('/threads/1/dislikes')
            ->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_dislike_a_thread()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $this->post($thread->path() . '/dislikes');

        $this->assertCount(1, $thread->dislikes);
    }

    /** @test */
    public function a_user_can_undislike_a_thread_by_second_click()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $this->post($thread->path() . '/dislikes');
        $this->post($thread->path() . '/dislikes');

        $this->assertCount(0, $thread->dislikes);
    }

    /** @test */
    public function a_user_cannot_dislike_and_like_a_thread()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $this->post($thread->path() . '/dislikes');
        $this->assertCount(1, $thread->dislikes);
        $this->post($thread->path() . '/likes');
        $this->assertCount(1, $thread->fresh()->likes);
        $this->assertCount(0, $thread->fresh()->dislikes);
    }

}
