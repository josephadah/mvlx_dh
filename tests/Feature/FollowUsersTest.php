<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FollowUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function an_unauthenticated_user_cannot_follow_a_user()
    {
    	$this->withExceptionHandling();

    	$this->post(route('follow.user', 'joe'))
    		->assertRedirect('/login');
    }

    /** @test */
    function a_user_can_follow_another_user()
    {
    	// given we have a signedIn user
    	$this->signIn($user1 = factory('App\User')->create());

    	// given we have another user2
    	$user2 = factory('App\User')->create();

    	// the signedIn user click on follow user2
    	$this->post(route('follow.user', $user2));

    	$this->assertEquals($user2->username, $user1->follows()->first()->username);
    }

    /** @test */
    function a_user_can_unfollow_another_user()
    {
    	$this->signIn($user1 = factory('App\User')->create());

    	$user2 = factory('App\User')->create();

    	$this->post(route('follow.user', $user2));

    	$this->assertEquals($user2->username, $user1->follows()->first()->username);

    	$this->delete(route('unfollow.user', $user2));

    	$this->assertNull($user1->follows()->first());
    }
}
