<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserSubscriptionsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_view_their_subscribed_threads()
    {
    	$this->signIn();

    	$thread = factory('App\Thread')->create();
    	$thread2 = factory('App\Thread')->create(['user_id' => factory('App\User')->create(), 'body' => 'hello world']);

    	$thread->subscribe();
    	$thread2->subscribe();

    	$this->get('/' . auth()->user()->username . '/subscriptions')
    		->assertSee($thread->title);
    }

    /** @test */
    public function a_subscribed_thread_knows_if_a_user_has_read_all_its_replies()
    {
        $this->signIn();

        $user = auth()->user();

        $thread = factory('App\Thread')->create();

        factory('App\Reply')->create(['thread_id' => $thread->id]);

        $thread->subscribe();

        $this->assertTrue($thread->hasUpdateFor());

        // simulate user read the thread
        $this->get('/threads/'.$thread->id.'/'.$thread->slug);

        $this->assertFalse($thread->hasUpdateFor());
    }
}
