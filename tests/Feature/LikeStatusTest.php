<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LikeStatusTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_unauthenticated_user_cannot_like_any_status()
    {
    	$this->withExceptionHandling()
    		->post('/statuses/1/likes')
    		->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_like_a_status()
    {
    	$this->signIn();

    	$status = factory('App\Status')->create();

    	$this->post('statuses/'.$status->id.'/likes');

    	$this->assertCount(1, $status->likes);
    }

    /** @test */
    public function a_user_can_unlike_a_status_by_second_click()
    {
        $this->signIn();

        $status = factory('App\Status')->create();

        $this->post('statuses/'.$status->id.'/likes');
        $this->post('statuses/'.$status->id.'/likes');

        $this->assertCount(0, $status->likes);
    }

    /** @test */
    public function an_unauthenticated_user_cannot_dislike_any_status()
    {
        $this->withExceptionHandling()
            ->post('/statuses/1/dislikes')
            ->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_dislike_a_status()
    {
        $this->signIn();

        $status = factory('App\Status')->create();

        $this->post('statuses/'.$status->id.'/dislikes');

        $this->assertCount(1, $status->dislikes);
    }

    /** @test */
    public function a_user_can_undislike_a_status_by_second_click()
    {
        $this->signIn();

        $status = factory('App\Status')->create();

        $this->post('statuses/'.$status->id.'/dislikes');
        $this->post('statuses/'.$status->id.'/dislikes');

        $this->assertCount(0, $status->dislikes);
    }

    /** @test */
    public function a_user_cannot_like_and_dislike_a_status()
    {
        $this->signIn();

        $status = factory('App\Status')->create();

        $this->post('statuses/'.$status->id.'/likes');
        $this->assertCount(1, $status->likes);
        $this->post('statuses/'.$status->id.'/dislikes');
        $this->assertCount(1, $status->fresh()->dislikes);
        $this->assertCount(0, $status->fresh()->likes);
    }

    /** @test */
    public function a_user_cannot_dislike_and_like_a_status()
    {
        $this->signIn();

        $status = factory('App\Status')->create();

        $this->post('statuses/'.$status->id.'/dislikes');
        $this->assertCount(1, $status->dislikes);
        $this->post('statuses/'.$status->id.'/likes');
        $this->assertCount(1, $status->fresh()->likes);
        $this->assertCount(0, $status->fresh()->dislikes);
    }
}
