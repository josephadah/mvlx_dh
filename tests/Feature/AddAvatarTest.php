<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AddAvatarTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function only_authenticated_users_can_add_avatar()
    {
    	$this->withExceptionHandling();

    	$this->json('POST', 'api/users/1/avatar')
    		->assertStatus(401);
    }

    /** @test */
    function only_valid_image_is_allowed()
    {
    	$this->withExceptionHandling()->signIn();

    	$this->json('POST', 'api/users/' . auth()->id() . '/avatar', [
    		'avatar' => 'not-an-image-file'
    	])->assertStatus(422);
    }

    /** @test */
    // function a_user_can_add_an_avatar_to_their_profile()
    // {
    // 	$this->signIn();

    // 	Storage::fake('public');

    // 	$this->json('POST', 'api/users/' . auth()->id() . '/avatar', [
    // 		'avatar' => $file = UploadedFile::fake()->image('avatar.jpg')
    // 	]);

    // 	$this->assertEquals('/images/avatars/' . $file->hashName(), auth()->user()->avatar_path);

        

    // 	// Storage::disk('public')->assertExists(auth()->user()->avatar_path);
    // }
}
