<?php

namespace Tests\Feature;

use App\Mail\PleaseConfirmYourEmail;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    function a_confirmation_email_is_sent_upon_registration()
    {
    	Mail::fake();

    	$this->post(route('register'), [
    		'username' => 'Joe',
    		'email' => 'joe@mail.com', 
    		'password' => 'password', 
    		'password_confirmation' => 'password', 
            'terms' => true
    	]);

    	Mail::assertQueued(PleaseConfirmYourEmail::class);
    }

    /** @test */
    function user_can_fully_confirm_their_email_addresses()
    {
        Mail::fake();

    	$this->post(route('register'), [
    		'username' => 'Joe',
    		'email' => 'joe@mail.com', 
    		'password' => 'password', 
    		'password_confirmation' => 'password', 
            'terms' => true
    	]);

    	$user = User::where('username', 'Joe')->first();

    	$this->assertFalse($user->confirmed);
    	$this->assertNotNull($user->confirmation_token);

    	// let the user confirm their account
    	$this->get('/register/confirm?token=' . $user->confirmation_token);
        
    	$this->assertTrue($user->fresh()->confirmed);
        $this->assertNull($user->fresh()->confirmation_token);
    }

    /** @test */
    function confirming_an_invalid_token()
    {
        $this->get(route('register.confirm', ['token' => 'invalid']))
            ->assertRedirect(route('home'))
            ->assertSessionHas('flash', 'Invalid confirmation token.');
    }
}
