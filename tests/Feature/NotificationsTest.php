<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotificationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_notification_is_sent_when_a_subscribed_thread_recieves_a_new_reply_that_is_not_by_the_current_user()
    {
        $this->signIn();

        $thread = factory('App\Thread')->create();

        $this->assertCount(0, auth()->user()->notifications);

        $this->post($thread->path() . '/subscriptions');

        $thread->addReply([
        	'user_id' => auth()->id(), 
        	'body' => 'some reply body'
        ]);

        $this->assertCount(0, auth()->user()->fresh()->notifications);

        $thread->addReply([
        	'user_id' => factory('App\User')->create()->id, 
        	'body' => 'some reply body'
        ]);

        $this->assertCount(1, auth()->user()->fresh()->notifications);
    }

    /** @test */ 
    public function a_user_can_mark_their_notification_as_read()
    {
    	$this->signIn();

    	$thread = factory('App\Thread')->create();

    	$thread->subscribe();

    	$thread->addReply([
        	'user_id' => factory('App\User')->create()->id, 
        	'body' => 'some reply body'
        ]);

        $this->assertCount(1, auth()->user()->fresh()->unreadNotifications);

        $notificationId = auth()->user()->unreadNotifications->first()->id;

        $this->delete('/notifications/' . $notificationId);

        $this->assertCount(0, auth()->user()->fresh()->unreadNotifications);
    }

    /** @test */
    // public function a_user_can_fetch_their_unread_notification()
    // {
    // 	$this->signIn();

    // 	$thread = factory('App\Thread')->create();

    // 	$thread->subscribe();

    // 	$thread->addReply([
    //     	'user_id' => factory('App\User')->create()->id, 
    //     	'body' => 'some reply body'
    //     ]);

    // 	$user = auth()->user();

    //     $response = $this->get('/notifications');

    //     $this->assertCount(1, $response[1]);
    // }
}
