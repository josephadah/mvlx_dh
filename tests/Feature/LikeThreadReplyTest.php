<?php

namespace Tests\Feature;

use App\Like;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LikeThreadReplyTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function an_unauthenticated_user_cannot_like_a_reply()
    {
    	$this->withExceptionHandling()
    		->post('/replies/1/likes')
    		->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_like_a_reply()
    {
    	$this->signIn();

    	$reply = factory('App\Reply')->create();

    	$this->post('/replies/' . $reply->id . '/likes');

    	$this->assertCount(1, $reply->likes);

        $this->assertEquals(1, Like::count());
    }

    /** @test */
    public function a_user_can_unlike_a_reply_by_second_click()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create();

        $this->post('/replies/' . $reply->id . '/likes');
        $this->post('/replies/' . $reply->id . '/likes');

        $this->assertCount(0, $reply->likes);
    }

    /** @test */
    public function a_user_cannot_like_and_dislike_a_reply()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create();

        $this->post('/replies/' . $reply->id . '/likes');
        $this->assertCount(1, $reply->likes);
        $this->post('/replies/' . $reply->id . '/dislikes');
        $this->assertCount(1, $reply->fresh()->dislikes);
        $this->assertCount(0, $reply->fresh()->likes);
    }

    /** @test */
    public function an_unauthenticated_user_cannot_dislike_a_reply()
    {
        $this->withExceptionHandling()
            ->post('/replies/1/dislikes')
            ->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_dislike_a_reply()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create();

        $this->post('/replies/' . $reply->id . '/dislikes');

        $this->assertCount(1, $reply->dislikes);
    }

    /** @test */
    public function a_user_can_undislike_a_reply_by_second_click()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create();

        $this->post('/replies/' . $reply->id . '/dislikes');
        $this->post('/replies/' . $reply->id . '/dislikes');

        $this->assertCount(0, $reply->dislikes);
    }

    /** @test */
    public function a_user_cannot_dislike_and_like_a_reply()
    {
        $this->signIn();

        $reply = factory('App\Reply')->create();

        $this->post('/replies/' . $reply->id . '/dislikes');
        $this->assertCount(1, $reply->dislikes);
        $this->post('/replies/' . $reply->id . '/likes');
        $this->assertCount(1, $reply->fresh()->likes);
        $this->assertCount(0, $reply->fresh()->dislikes);
    }

}
