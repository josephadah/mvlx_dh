<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BlockedReplyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_blocked_reply_body_is_changed() 
    {
        $this->signIn();

        $reply = factory('App\Reply')->create();

        $reply->block();

        $this->assertEquals(
            '<p style="color:red;">This reply has been blocked.</p>', 
            $reply->fresh()->body
        );
    }

    /** @test */
    function a_non_administrator_cannot_block_a_reply()
    {
    	$this->withExceptionHandling()->signIn();

    	$reply = factory('App\Reply')->create();

    	$this->post(route('blocked-replies.store', $reply))
    		->assertStatus(403);

    	$this->assertFalse(!! $reply->fresh()->blocked);
    }

    /** @test */
    function an_administrator_can_block_a_reply()
    {
    	$this->signInAdmin();

    	$reply = factory('App\Reply')->create();

    	$this->post(route('blocked-replies.store', $reply))
    		->assertStatus(200);

    	$this->assertTrue($reply->fresh()->blocked);
    }

    /** @test */
    function an_administrator_can_unlock_a_reply()
    {
        $this->signInAdmin();

        $reply = factory('App\Reply')->create();

        $this->post(route('blocked-replies.store', $reply))
            ->assertStatus(200);

        $this->assertTrue($reply->fresh()->blocked);

        $this->delete(route('blocked-replies.destroy', $reply))
            ->assertStatus(200);

        $this->assertFalse($reply->fresh()->blocked);
    }
}
