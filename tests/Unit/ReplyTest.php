<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class ReplyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_generates_the_correct_path_for_a_paginated_thread()
    {
        $thread = factory('App\Thread')->create();

        $replies = factory('App\Reply', 3)->create(['thread_id' => $thread->id]);

        config(['admin.pagination.repliesPerPage' => 1]);

        $this->assertEquals(
            $thread->showPath() . '?page=1#reply-1', 
            $replies->first()->path()
        );

        $this->assertEquals(
            $thread->showPath() . '?page=2#reply-2', 
            $replies[1]->path()
        );

        $this->assertEquals(
            $thread->showPath() . '?page=3#reply-3', 
            $replies->last()->path()
        );
    }

    /** @test */
    public function it_knows_the_user_that_created_the_reply()
    {
    	$reply = factory('App\Reply')->create();

    	$this->assertInstanceOf('App\User', $reply->owner);
    }

    /** @test */
    public function it_knows_if_it_was_just_published()
    {
    	$reply = factory('App\Reply')->create();

    	$this->assertTrue($reply->wasJustPublished());

		$reply->created_at = Carbon::now()->subWeek();
		
		$this->assertFalse($reply->wasJustPublished());
    }

    /** @test */
    public function it_can_detect_all_mentioned_users_in_the_body()
    {
        $reply = factory('App\Reply')->create(['body' => '@Musa is the greeting you @Joseph']);

        $this->assertEquals(['Musa', 'Joseph'], $reply->mentionedUsers());
    }

    /** @test */
    function it_wraps_username_within_an_anchor_tag()
    {
        $reply = factory('App\Reply')->create(['body' => 'Hello @Musa, how are you.']);

        $this->assertEquals(
            'Hello <a href="/Musa">@Musa</a>, how are you.', 
            $reply->body
        );
    }

    /** @test */
    function a_reply_can_automatically_sanitize_its_body()
    {
        $reply = factory('App\Reply')->make([
            'body' => '<script>alert("hi");</script><p>This is good</p>'
        ]);

        $this->assertEquals('<p>This is good</p>', $reply->body);
    }

    /** @test */
    function a_reply_can_be_blocked()
    {
        $reply = factory('App\Reply')->create();

        $this->assertFalse($reply->fresh()->blocked);

        $reply->block();

        $this->assertTrue($reply->fresh()->blocked);
    }

    /** @test */
    function a_reply_can_be_unblocked()
    {
        $reply = factory('App\Reply')->create();

        $this->assertFalse($reply->fresh()->blocked);

        $reply->block();

        $this->assertTrue($reply->fresh()->blocked);

        $reply->unblock();

        $this->assertFalse($reply->fresh()->blocked);
    }
}
