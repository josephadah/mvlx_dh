<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function a_user_can_fetch_their_most_recent_reply()
    {
        $user = factory('App\User')->create();

        $reply = factory('App\Reply')->create(['user_id' => $user->id]);

        $this->assertEquals($reply->id, $user->lastReply->id);
    }

    /** @test */
    // function a_user_can_determine_its_avatar_path()
    // {
    // 	$user = factory('App\User')->create();

    // 	$this->assertEquals(asset('storage/images/avatars/default.png'), $user->avatar_path);

    // 	$user->avatar_path = 'images/avatars/myavatar.jpg';

    // 	$this->assertEquals(asset('storage/images/avatars/myavatar.jpg'), $user->avatar_path);
    // }

    /** @test */
    function a_user_can_follow_other_users()
    {
        $john = factory('App\User')->create(['username' => 'John']);
        $rose = factory('App\User')->create(['username' => 'Rose']);

        $john->follow($rose->id);

        $this->assertEquals('Rose', $john->follows()->first()->username);
    }

    /** @test */
    function a_user_can_be_followed_by_other_users()
    {
        $john = factory('App\User')->create(['username' => 'John']);
        $rose = factory('App\User')->create(['username' => 'Rose']);

        $rose->follow($john->id);

        $this->assertEquals('John', $rose->follows()->first()->username);
    }

    /** @test */
    function a_user_knows_if_its_following_another_user()
    {
        $this->signIn($user1 = factory('App\User')->create());

        $user2 = factory('App\User')->create();

        $this->post(route('follow.user', $user2));

        $this->assertTrue($user1->isFollowing($user2->id));

        $this->delete(route('unfollow.user', $user2));

        $this->assertFalse($user1->isFollowing($user2->id));

    }

    /** @test */
    function a_user_can_earn_reward()
    {
        $user = factory('App\User')->create();

        $user->earnReward();

        $this->assertEquals(1, $user->reward);
    }

    /** @test */
    function a_user_can_loose_reward()
    {
        $user = factory('App\User')->create();

        $user->reward = 1;

        $user->looseReward();

        $this->assertEquals(0, $user->reward);
    }
}
