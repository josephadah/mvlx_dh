<?php

namespace Tests\Unit;

use App\Notifications\ThreadWasUpdated;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ThreadTest extends TestCase
{
    use RefreshDatabase;

    protected $thread;

    protected function setUp()
    {
    	parent::setUp();

    	$this->thread = factory('App\Thread')->create();
    }

    /** @test */
    function a_thread_has_a_path()
    {
        $thread = factory('App\Thread')->create();

        $this->assertEquals(
            "/threads/{$thread->id}", $thread->path()
        );
    }

    /** @test */
    public function it_knows_the_creator_of_the_thread()
    {
    	$this->assertInstanceOf('App\User', $this->thread->creator);
    }

    /** @test */
    public function it_can_add_reply()
    {
    	$this->signIn();

    	$this->thread->addReply([
    		'body' => 'reply body', 
    		'user_id' => auth()->id()
    	]);

    	$this->assertCount(1, $this->thread->replies);
    }

    /** @test */
    public function it_send_subribers_notification_when_a_new_reply_is_added()
    {
        Notification::fake();

        $this->signIn();

        $this->thread->subscribe();

        $this->thread->addReply([
            'body' => 'reply body', 
            'user_id' => factory('App\User')->create()->id
        ]);

        Notification::assertSentTo(auth()->user(), ThreadWasUpdated::class);

        
    }

    /** @test */
    public function a_thread_belongs_to_a_section()
    {
    	$this->assertInstanceOf('App\Section', $this->thread->section);
    }

    /** @test */
    public function an_authenticated_user_can_subscribe_to_a_thread()
    {
        $this->signIn();

        $this->thread->subscribe();

        $this->assertEquals(
            1, 
            $this->thread->subscriptions()->where('user_id', auth()->id())->count()
        );
    }

        /** @test */
    public function an_authenticated_user_can_unsubscribe_from_a_thread()
    {
        $this->signIn();

        $this->thread->unsubscribe();

        $this->assertEquals(
            0, 
            $this->thread->subscriptions()->where('user_id', auth()->id())->count()
        );
    }

    /** @test */
    public function it_knows_if_the_authenticated_user_is_subscribed_to_it()
    {
        $this->signIn();

        $this->assertFalse($this->thread->isSubscribedTo());

        $this->thread->subscribe();

        $this->assertTrue($this->thread->isSubscribedTo());
    }

    /** @test */
    function it_knows_the_numbers_of_visits_per_thread()
    {
    	$thread = factory('App\Thread')->create();

    	$thread->resetVisits();

    	$this->assertSame(0, $thread->visits());

    	$thread->recordVisit();

    	$this->assertEquals(1, $thread->visits());

    	$thread->recordVisit();

    	$this->assertEquals(2, $thread->visits());
    }

    /** @test */
    function a_thread_may_be_locked()
    {
        $this->assertFalse($this->thread->locked);

        $this->thread->lock();

        $this->assertTrue($this->thread->locked);
    }

    /** @test */
    function a_thread_may_be_unlocked()
    {
        $this->assertFalse($this->thread->locked);

        $this->thread->lock();

        $this->assertTrue($this->thread->locked);

        $this->thread->fresh()->unlock();

        $this->assertFalse($this->thread->fresh()->locked);
    }

    /** @test */
    function a_thread_can_be_blocked()
    {
        $this->assertFalse($this->thread->fresh()->blocked);

        $this->thread->block();

        $this->assertTrue($this->thread->fresh()->blocked);
    }

    /** @test */
    function a_thread_can_be_unblocked()
    {
        $this->assertFalse($this->thread->fresh()->blocked);

        $this->thread->block();

        $this->assertTrue($this->thread->fresh()->blocked);

        $this->thread->unblock();

        $this->assertFalse($this->thread->fresh()->blocked);
    }

    /** @test */
    function a_thread_can_automatically_sanitize_its_body()
    {
        $thread = factory('App\Thread')->make([
            'body' => '<script>alert("hi");</script><p>This is good</p>'
        ]);

        $this->assertEquals('<p>This is good</p>', $thread->body);
    }
}
